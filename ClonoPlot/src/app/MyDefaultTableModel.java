package app;

import javax.swing.table.DefaultTableModel;

import util.Reorderable;

public class MyDefaultTableModel extends DefaultTableModel implements Reorderable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7621784673602893420L;

	@Override
	public void reorder(int fromIndex, int toIndex) {
		// System.out.println(fromIndex + " toIndex: " + (toIndex));
		if (fromIndex < toIndex)
			moveRow(fromIndex, fromIndex, toIndex - 1);
		else
			moveRow(fromIndex, fromIndex, toIndex);
	}
}
