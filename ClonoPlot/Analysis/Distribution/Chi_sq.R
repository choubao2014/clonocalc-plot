# Bsp. Aufruf:
# chisq.plot(l, legend.y.names = c("T1","T2"), title = "Distribution between 2 TCZ of mouse m1 naive using Chi-square Test", 
#                range = 1:50, max.value = 0, ignore.own.range = F, color.numbers = "goldenrod3")

# in datei speichern:
# save.chisq.plot(l, legend.y.names = c("T1","T2"), file.name = "chisq_m1_naive_TCZs.pdf",
#                title = "Distribution between 2 TCZ of mouse m1 naive using Chi-square Test", range = 1:50)

#' @description
#' Plot a chi sqaure diagram.
#' 
#' @param title string() Set the title of the plot
#' @param width float() Set the width of the diagram.
#' @param height float() Set the height of the diagram.
#' @param range regex(^[0-9]*:[0-9]*$) Set Range of used Clonotypes. The Clonotypes are ordered descending by their abundance. e.g. for the first 20 "1:20" without quotes
#' @param max.value integer() Set the maximum value for the bar of heat (0 for auto).
#' @param ignore.own.range bool() True = Let levelplot choose the range for the bar of head, False = Let the function choose the range for the bar of heat.
#' @param percent.delim float([0-1]) Plot a vertical line in the diagram. Left there are the Clonotypes of the first sample in the list with a percentage > percent.delim and right the ones with a percentage < percent.delim 
#' @param color.numbers string() Set the color for the numbers inside the plot
#' @param scale.factor.numbers float() Set the size for the numbers inside the plot


Chi_sq <- function(dataFrameList, height = 5, width = 15, legend.y.names = NULL, title = "Distribution using Chi-squared Test", range = 1:20, max.value = 0, ignore.own.range = F, percent.delim = 0.05, color.numbers = "goldenrod3", scale.factor.numbers = 1 , preview=F){
  library(lattice)
  l <- dataFrameList

  if(!is.null(legend.y.names)&&length(l) != length(legend.y.names))
    stop("length(legend.y.names) != length(l)")
  else
    legend.y.names <- names(dataFrameList)
  
  percent.delim.line.point <- sum(l[[1]][,2] > percent.delim)
  if(percent.delim.line.point > range[length(range)])
    percent.delim.line.point <- 0
  
  l <- lapply(l, toMoeller)
  m1.r  <- lapply( l, function(x) data.frame(AAseq=rownames(x),count=x$Read.count) )
  m <- Reduce( function(x,y) merge(x,y,"AAseq",all.x=TRUE,all.y=TRUE), m1.r )
  m[is.na(m)] <- 0
  rownames(m) <- m$AAseq
  m <- m[,-1]
  m.sort <- m[order(rowSums(m),decreasing=TRUE),]

  m2 <- rbind(m.sort[range,],colSums(m.sort[-(range),]))
  N <- 100
  colpal <- colorRampPalette(c("black","blue","orange","red"))(N+1)
  
  number.of.samples <- length(range)
  enrichments <- t(sapply( 1:(number.of.samples+1), function(i) chisq.test(m2[i,], p=colSums(m), rescale.p=TRUE)$stdres ))
  
  if(max.value == 0)
    max.value <- max(apply(enrichments,1,max))
  sc <- seq(-max.value, max.value, length.out=N)
  
  colpal[abs(sc)<20] <- NA
  
  if(!is.null(legend.y.names))
    colnames(enrichments) <- legend.y.names
  rownames(enrichments) <- rownames(m2)
  rownames(enrichments)[nrow(enrichments)] <- "Rest"
  
  xlab <- paste0(number.of.samples," most abundant clonotypes")
  if(percent.delim.line.point > 0)
    xlab <- paste0(xlab,", line by ", percent.delim, "%")
  ylab <- "compartments"
  
  cex <- scale.factor.numbers * (0.7 - ncol(m)*0.12)
  
  myPanel <- function(x, y, z, ...) {
    panel.levelplot(x,y,z,...)
    panel.abline(v = percent.delim.line.point-0.5)
    panel.text(x, y, as.matrix(m2)[cbind(x,y)], srt=90, cex=cex, col=color.numbers)
  }

  dev.control(displaylist="enable")

  if(ignore.own.range){
    print(levelplot( enrichments, col.regions=colpal, scales=list(rot=45), #at=sc , 
               main=title, 
               xlab=xlab, ylab=ylab,
               panel = myPanel))
  }
  else {
    print(levelplot( enrichments, col.regions=colpal, scales=list(rot=45), at=sc , 
                     main=title, 
                     xlab=xlab, ylab=ylab,
                     panel = myPanel))
  }
  return(recordPlot())
}
