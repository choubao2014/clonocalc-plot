#'IMPORTANT INFORMATION
#'This are only wrapper scripts for functions from the R tcR-package.
#'To get more details about the tcR-package please visit
#'https://cran.r-project.org/web/packages/tcR/index.html
#'or
#'http://imminfo.github.io/tcr/
