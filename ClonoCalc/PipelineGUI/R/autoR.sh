#!/bin/bash

i=0
length="$#"
while [ "$length" -ne 0 ] && [ -n "$1" ]
  do
	#double square bracket to supress errors
    	[[ $1 == *"-cd "* ]] && echo cd $(echo $1|cut -c 5-) && cd $(echo $1|cut -c 5-)
	((i++))
	if ((i < length))
	then
		shift
	else
		break
	fi
done

pwd=$(pwd)
echo $pwd
cd R

#auto genrate r script
filenamesfile="../filenames.txt"
cryoNrfile="../cyroNr.txt"
barcode="../barcodes.txt"
rscript="autoGenScript.R"
rscripteqpart_non_tcr="template_non_tcr.R"
rscripteqpart_tcr="template_tcr.R"
wsName_non_tcr="workspace__non_tcr_post_processed.RData"
dfName_non_tcr="dataframes_non_tcr.RData"
wsName_tcr="workspace_tcr_post_processed.RData"
dfName_tcr="dataframes_tcr.RData"
prefix="id_"
config="../config.txt"
outputfoldertables="tables"

#get outputpath from config file
if [ -f $config ]
then
	while read line; do
		if [[ $line == *"#"* ]] || [[ -z "$line" ]]
		then
			continue
		fi
		#split line by "=" in name and value
		#tr -d '\040\011\012\015' will remove spaces, tabs, carriage returns and newlines
		name=$(echo $line |  tr -d '\040\011\012\015' | cut -d\= -f1) 
		if [[ $line != *"\""* ]]
		then	#example: count = 10000
			value=$(echo $line |  tr -d '\040\011\012\015' | cut -d\= -f2)
		else	#example: string = "hello world"
			#get the substring between the quotes
 			value=$(echo $line | cut -d'"' -f2)
		fi
		if [[ $name == "OutputPath" ]]
		then
			echo "OutputPath = $value";
			outputpath=$value;
		fi
	done < $config
else
	echo "no $config file found";
fi

#get last run
lastTimestamp=$(head -n 1 "../lastTime.txt")
echo $lastTimestamp

outputdir="${outputpath}/${lastTimestamp}"

while IFS=\= read var; do
    filenamesfilearr+=($var)
done < $filenamesfile
#readarray -t filenamesfilearr < $filenamesfile

while IFS=\= read var; do
    cryoNrarr+=($var)
done < $cryoNrfile
#readarray -t cryoNrarr < $cryoNrfile

while IFS=\= read var; do
    barcodearr+=($var)
done < $barcode
#readarray -t barcodearr < $barcode

rm $rscript 2> /dev/null

#install data.tables
#echo "install.packages(\"data.table\")" >> $rscript

#copy tcr stuff to nontcr
#cp -r ${outputdir}/tcr ${outputdir}/nontcr

#setwd("~/RTeasy")
#workingdir="${outputdir}/nontcr"
#echo "setwd(\"${workingdir}\")" >> $rscript
#echo >> $rscript

#ATGATA_1800_t_i9<-read.table(file="ATGATA.xls", sep="\t", header=T, strip.white=T, stringsAsFactors=F)

#for i in "${!filenamesfilearr[@]}"; do
#	echo "${prefix}${filenamesfilearr[i]} <- read.table(file=\"${filenamesfilearr[i]}.xls\", sep=\"\\t\", header=T, strip.white=T, stringsAsFactors=F)" >> $rscript
#done

#echo >> $rscript

#cuttedlist<-list(i9_1800_t=ATGATA_1800_t_i9, i11_1855_t=TACGTA_1855_t_i11, 
#i10_1855_k=CGTGAT_1855_k_i10, i12_1855_b=TGAGCG_1855_b_i12, 
#i4_1856_t=CGCTCT_1856_t_i4, i3_1856_k=TGACTA_1856_k_i3, i5_1856_b=AGATGA_1856_b_i5)

#echo "tmpList <- list(" >> $rscript
#filenamesfilearrlen=${#filenamesfilearr[@]}
#for i in "${!filenamesfilearr[@]}"; do
#	if ((i+1 != filenamesfilearrlen))
#	then
#		echo "${prefix}${filenamesfilearr[i]} = ${prefix}${filenamesfilearr[i]}, " >> $rscript
#	else
#		echo "${prefix}${filenamesfilearr[i]} = ${prefix}${filenamesfilearr[i]})" >> $rscript
#	fi
#done

#append the equal script part to the dynamic generated part
#cat $rscripteqpart_non_tcr >> $rscript

#save.image("~/RTeasy/as.RData")
#echo "save.image(\"${workingdir}/${wsName_non_tcr}\")" >> $rscript

#save dataframes in RData
#echo >> $rscript

#for i in "${!cryoNrarr[@]}"; do
#	echo "${prefix}${cryoNrarr[i]} <- wihout1count_merge_norm_AAcut_sort_list[[\"${prefix}${filenamesfilearr[i]}\"]]" \
#	>> $rscript

	#echo "for(n in names(wihout1count_merge_norm_AAcut_sort_list)) {" >> $rscript
	#echo "   assign(n, wihout1count_merge_norm_AAcut_sort_list[[n]])" >> $rscript
	#echo "}" >> $rscript
#done

#echo >> $rscript

#echo "save(" >> $rscript

#for i in "${!cryoNrarr[@]}"; do
#	echo "${prefix}${cryoNrarr[i]}," >> $rscript
#done

#echo "file = \"${workingdir}/${dfName_non_tcr}\")" >> $rscript

#---------------------------------------------------
#create skript part to handle files for the tcr package

echo >> $rscript
echo "#clear workspace" >> $rscript
echo "rm(list=ls())" >> $rscript
echo >> $rscript

#setwd("~/RTeasy")
workingdir="${outputdir}/${outputfoldertables}"
echo "setwd(\"${workingdir}\")" >> $rscript

#ATGATA_1800_t_i9<-read.table(file="ATGATA.xls", sep="\t", header=T, strip.white=T, stringsAsFactors=F)

for i in "${!filenamesfilearr[@]}"; do
	echo "${prefix}${filenamesfilearr[i]} <- read.table(file=\"${filenamesfilearr[i]}.xls\", sep=\"\\t\", header=T, strip.white=T, stringsAsFactors=F)" >> $rscript
done

echo >> $rscript

#cuttedlist<-list(i9_1800_t=ATGATA_1800_t_i9, i11_1855_t=TACGTA_1855_t_i11, 
#i10_1855_k=CGTGAT_1855_k_i10, i12_1855_b=TGAGCG_1855_b_i12, 
#i4_1856_t=CGCTCT_1856_t_i4, i3_1856_k=TGACTA_1856_k_i3, i5_1856_b=AGATGA_1856_b_i5)

echo "tmpList <- list(" >> $rscript
filenamesfilearrlen=${#filenamesfilearr[@]}
for i in "${!filenamesfilearr[@]}"; do
	if ((i+1 != filenamesfilearrlen))
	then
		echo "${prefix}${filenamesfilearr[i]} = ${prefix}${filenamesfilearr[i]}, " >> $rscript
	else
		echo "${prefix}${filenamesfilearr[i]} = ${prefix}${filenamesfilearr[i]})" >> $rscript
	fi
done

#update the log file
logfilePath="${outputdir}/log/overview.txt"
logfilePathHP="${outputdir}/log/overview_high_precision.txt"

#read in "old" log file
echo "t <- scan(\"${logfilePath}\", what=character(), sep=\"\n\")" >> $rscript
echo "tHP <- scan(\"${logfilePathHP}\", what=character(), sep=\"\n\")" >> $rscript

#append the equal script part to the dynamic generated part
cat $rscripteqpart_tcr >> $rscript

#update the log file
logfilePathNew="${outputdir}/log/overview_post_processing.txt"
logfilePathNewHighPrecision="${outputdir}/log/overview_post_processing_high_precision.txt"
#additional log file part
echo "write(tmpfinalstr, file=\"${logfilePathNew}\")" >> $rscript
echo "write(tmpfinalstr_high_precision, file=\"${logfilePathNewHighPrecision}\")" >> $rscript

#save.image("~/RTeasy/as.RData")
echo "save.image(\"${workingdir}/${wsName_tcr}\")" >> $rscript

#excute R script
Rscript $rscript

echo "R script finished"
