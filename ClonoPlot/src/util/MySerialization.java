package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTree;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultTreeModel;

import app.GlobalVars;

public class MySerialization {
	// private static final Logger log =
	// Logger.getLogger(MySerialization.class);

	private final String serFileTableJTreeExpansion = "serTableJTreeExpansion.txt";
	private final String serFileTableTreeModel = "serTableTreeModel.txt";
	private final String serFileTableJTree = "serTableJTree.txt";
	private final String serFileScriptJTreeExpansion = "serScriptJTreeExpansion.txt";
	private final String serFileScriptTreeModel = "serScriptTreeModel.txt";
	private final String serFileScriptJTree = "serScriptJTree.txt";
	private final String serAdvancedOptTableModel = "serAdvancedOptTableModel.txt";
	private final String serSepParaValue = "###";

	// private SerializeGeneric<DefaultTreeModel> serTableTreeModel;

	private String serFolder;

	public MySerialization(String serFolder) {
		this.serFolder = serFolder + File.separator;
	}

	public void saveTableTreeModel(DefaultTreeModel treeModel) {
		new SerializeGeneric<DefaultTreeModel>(serFolder + serFileTableTreeModel, treeModel).serializeObject();
	}

	public DefaultTreeModel loadTableTreeModel() {
		return new SerializeGeneric<DefaultTreeModel>(serFolder + serFileTableTreeModel).deserializeObject();
	}

	public void saveScriptTreeModel(DefaultTreeModel treeModel) {
		new SerializeGeneric<DefaultTreeModel>(serFolder + serFileScriptTreeModel, treeModel).serializeObject();
	}

	public DefaultTreeModel loadScriptTreeModel() {
		return new SerializeGeneric<DefaultTreeModel>(serFolder + serFileScriptTreeModel).deserializeObject();
	}

	public void saveTableJTree(JTree JTree) {
		new SerializeGeneric<JTree>(serFolder + serFileTableJTree, JTree).serializeObject();
	}

	public JTree loadTableJTree() {
		JTree jTree = new SerializeGeneric<JTree>(serFolder + serFileTableJTree).deserializeObject();
		if (jTree != null)
			return jTree;
		else
			return new JTree();
	}

	public void saveScriptJTree(JTree JTree) {
		new SerializeGeneric<JTree>(serFolder + serFileScriptJTree, JTree).serializeObject();
	}

	public JTree loadScriptJTree() {
		JTree jTree = new SerializeGeneric<JTree>(serFolder + serFileScriptJTree).deserializeObject();
		if (jTree != null)
			return jTree;
		else
			return new JTree();
	}

	public void saveTableJTreeExpansionState(JTree tree) {
		FileOperations.writeStringToFile(serFolder + serFileTableJTreeExpansion, TreeUtil.getExpansionState(tree, 0));
	}

	public void restoreTableJTreeExpansionState(JTree tree) {
		// collapse the complete tree first
		for (int i = tree.getRowCount() - 1; i > 0; i--)
			tree.collapseRow(i);
		String expansionState = FileOperations.readFirstLineInString(serFolder + serFileTableJTreeExpansion);
		if (expansionState != null)
			TreeUtil.restoreExpanstionState(tree, 0, expansionState);
	}

	public void saveScriptJTreeExpansionState(JTree tree) {
		FileOperations.writeStringToFile(serFolder + serFileScriptJTreeExpansion, TreeUtil.getExpansionState(tree, 0));
	}

	public void restoreScriptJTreeExpansionState(JTree tree) {
		// collapse the complete tree first
		for (int i = tree.getRowCount() - 1; i > 1; i--)
			tree.collapseRow(i);
		String expansionState = FileOperations.readFirstLineInString(serFolder + serFileScriptJTreeExpansion);
		if (expansionState != null)
			TreeUtil.restoreExpanstionState(tree, 0, expansionState);
	}

	public void saveAdvancedOptTable(DefaultTableModel tableModel) {
		List<String> parameterList = new ArrayList<String>();
		for (int i = 0; i < tableModel.getRowCount(); i++) {
			String parameter = (String) tableModel.getValueAt(i, 0);
			String value = (String) tableModel.getValueAt(i, 1);
			parameterList.add(parameter + serSepParaValue + value);
			FileOperations.writeListToFile(serFolder + serAdvancedOptTableModel, parameterList);
		}
	}

	public void restoreAdvancedOptTable(DefaultTableModel tableModel) {
		// DefaultTableModel tableModel = new DefaultTableModel();
		while (tableModel.getRowCount() > 0)
			tableModel.removeRow(0);
		try (BufferedReader br = new BufferedReader(new InputStreamReader(
				new FileInputStream(serFolder + serAdvancedOptTableModel), GlobalVars.charsetName))) {
			String line = br.readLine();
			while (line != null) {
				String parameterValue[] = line.split(serSepParaValue);
				if (parameterValue.length == 2)
					tableModel.addRow(new String[] { parameterValue[0], parameterValue[1] });
				else if (parameterValue.length == 1)
					tableModel.addRow(new String[] { parameterValue[0], "" });
				line = br.readLine();
			}
		} catch (FileNotFoundException e) {
			// log.error(e, e);
		} catch (IOException e) {
			// log.error(e, e);
		}
		// return tableModel;
	}
}
