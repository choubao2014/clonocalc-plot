#!/bin/bash

# This script installs the R packages in particular for the execution of
# ClonoCalc and ClonoPlot. For simplicity and to ensure not to inadvertedly
# induce incompatibilities with other installed programs, we refrain from
# installing the Java package and merely test for their existance.

set -e

if ! which java ; then
	echo "E: We could not find any version of java. Please install Java of version 1.8 or later. If you are confident that Java is installed, then please adjust the PATH settings. If no other version of Java is yet installed on this machine, run"
        echo "sudo apt-get install openjdk-8-jre openjfx"
	exit
fi

JAVAVERSION=$(java -version 2>&1 | grep version | cut -f2 -d\")
if echo "$JAVAVERSION" | egrep -q "^1.[89]" ; then
	echo "I: Version '$JAVAVERSION' is sufficient."
else
	echo "E: Java version '$JAVAVERSION' of '$(which java)' is too old. Please install version 1.8 or newer."
	exit
fi


if ! which apt-get ; then
	echo "W: Sorry, this installation is primarily aiming for Debian and Ubuntu distributions. or others that support the apt package system."
        echo "   This installation will hence not install system packages and gives instructions for manual intervention."
	exit
fi


if ! which wget ; then
	echo "E: Please install wget utility to allow the auto-download of the ClonoSuite ZIP file"
	exit
fi


RPACKAGES='"Rserve","data.table","ggplot2","gplots","stringr","tcR","RCircos"'
RPACKAGESDEB="r-cran-rserve r-cran-data.table r-cran-ggplot2 r-cran-gplots r-cran-stringr" # tcR and RCircos are not available as packages
RPACKAGES2="'xtable', 'iterators', 'backports', 'pkgmaker', 'registry', 'rngtools', 'gridBase', 'foreach', 'doParallel', 'mime', 'rprojroot', 'R6', 'DBI', 'BH', 'NMF', 'irlba', 'evaluate', 'highr', 'markdown', 'yaml', 'brew', 'desc', 'commonmark', 'xml2', 'dplyr', 'gridExtra', 'igraph', 'stringdist', 'knitr', 'roxygen2'"
RPACKAGES2DEB="r-cran-xtable r-cran-iterators r-cran-backports r-cran-pkgmaker r-cran-registry r-cran-rngtools r-cran-gridbase r-cran-foreach r-cran-doparallel r-cran-mime r-cran-r6 r-cran-dbi r-cran-nmf r-cran-irlba r-cran-evaluate r-cran-highr r-cran-markdown r-cran-yaml r-cran-brew r-cran-xml2 r-cran-dplyr r-cran-gridextra r-cran-igraph r-cran-knitr"
# Missing: 'rprojroot','bh','roxygen2','stringdist','desc','commonmark'

#------------------------------R install/update-----------------------------------------
installR(){

	if which apt-get; then

		sudo apt-get update
		sudo apt-get install r-base-dev r-recommended circos

		#RCurl needs libcurl
		echo "I: install curl, libssl-dev, libcurl4-openssl-dev, libxml2-dev and libcairo2-dev for R ..."
		sudo apt-get install curl libssl-dev libcurl4-openssl-dev libxml2-dev libcairo2-dev

	fi


	echo "I: check if R is installed"
	RVersionString=$(Rscript -e 'strsplit(R.version$version.string," ")[[1]][3]'|grep -P '[0-9]+(\.[0-9]+)+' -o)

	if [ "$RVersionString" > "3.2.1" ]; then
		echo "R installation complete!"
	else
		echo "R installation failed! Try to restart or visit http://www.jason-french.com/blog/2013/03/11/installing-r-in-linux/ to get more information how to install R on your os."
		#read -n 1 -s -p "Press any key to continue."
		exit
	fi
}

installRpackages(){
	if which apt-get; then
		echo "I: Updating apt repository"
		sudo apt-get update
		echo "I: Ensuring availability of system packages for R packages: $RPACKAGESDEB"
		sudo apt-get install $RPACKAGESDEB
		RPACKAGES='"tcR","RCircos"' # the only package yet not available as a Debian package

		echo "I: Now installing known dependencies of missing packages '$PACKAGES': $RPACKAGES2DEB"
		if ! sudo apt-get install $RPACKAGES2DEB; then
			echo "I: Could not install all packages known to be dependencies of tcR and RCircos, will iterate"
			for p in $RPACKAGES2DEB; do
				echo $p
				sudo apt-get install $p && echo "I: Installation of '$p' successful" || echo "W: Installation of '$p' failed"
			done
		fi
		echo "I: Completed preparation with system packages"
	fi


	ERRORLOG="R_error_log.txt"
	echo "I: Performing installation of packages with R - as root"
	sudo Rscript -e "list <- c($RPACKAGES); for(x in list) { if (!require(x,character.only = TRUE)){ install.packages(x, dependencies=T, repos='https://cran.uni-muenster.de'); if(!require(x,character.only = TRUE)){ stop(paste('Package', x, 'installation failed!')); }}}" 2>&1 | tee $ERRORLOG

	if grep -q 'Execution halted' $ERRORLOG; then
		echo "E: R package installation failed! Please inspect '$ERORLOG' for details."
		exit
	fi
	rm -f $ERRORLOG

}


if type -p Rscript; then
	RVersionString=$(Rscript -e 'strsplit(R.version$version.string," ")[[1]][3]'|grep -P '[0-9]+(\.[0-9]+)+' -o)
	if [ "$RVersionString" > "3.2.1" ]; then
		echo "$RVersionString is sufficent."
	else
		echo "$RVersionString is not sufficent. Try to update R"
		installR
	fi
else
	#R frest install
	echo "R not found -> install R"
	installR
fi

installRpackages

echo "I: Installation of R completed"


#------------------------------Download/install ClonoCalc/Plot-----------------------------------------
CLONOSUITELINKPREFIX="https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master"
CLONOCALCLINKPREFIX="$CLONOSUITELINKPREFIX/ClonoCalc"
CLONOPLOTLINKPREFIX="$CLONOSUITELINKPREFIX/ClonoPlot"

CLONOCALCFILE="ClonoCalc.zip"
if ! wget -O $CLONOCALCFILE "$CLONOCALCLINKPREFIX/ClonoCalc.zip" ; then
	echo "E: Failed to download '$CLONOCALCLINKPREFIX/ClonoCalc.zip'"
	exit
fi
unzip $CLONOCALCFILE -d ClonoCalc
rm -f $CLONOCALCFILE

CLONOPLOTFILE="ClonoPlot.zip"
if ! wget -O $CLONOPLOTFILE "$CLONOPLOTLINKPREFIX/ClonoPlot.zip"; then
	echo "E: Failed to download from '$CLONOPLOTLINKPREFIX/ClonoPlot.zip'"
	exit
fi
unzip $CLONOPLOTFILE -d ClonoPlot
rm -f $CLONOPLOTFILE

#create launcher
CreateLauncher(){
	LAUNCHERNAME=$1
	LAUNCHERFILE="$LAUNCHERNAME.desktop"
	EXEC=$2
	ICONPATH=$3

	echo "[Desktop Entry]" >> $LAUNCHERFILE
	echo "Name=$LAUNCHERNAME" >> $LAUNCHERFILE
	echo "Exec=$EXEC" >> $LAUNCHERFILE
	echo "Terminal=false" >> $LAUNCHERFILE
	echo "Type=Application" >> $LAUNCHERFILE
	echo "Icon=$ICONPATH" >> $LAUNCHERFILE
	if ! chmod u+x $LAUNCHERFILE; then
		echo "E: Could not chmod '$LAUNHERFILE' while in $(pwd)"
		exit
	fi
}

CreateLauncherForJar(){
	LAUNCHERNAME=$1
	EXECPATH=$2
	EXECJAR=$3
	ICONNAME=$4

	echo 'cd $(dirname "$0")' >> $EXECPATH/$LAUNCHERNAME.sh
	echo "java -jar $EXECJAR" >> $EXECPATH/$LAUNCHERNAME.sh
	if ! chmod u+x $EXECPATH/$LAUNCHERNAME.sh; then
		echo "E: Could not chmod '$EXECPATH/$LAUNCHERNAME.sh' while in $(pwd)"
		exit
	fi
	CreateLauncher $LAUNCHERNAME $EXECPATH/$LAUNCHERNAME.sh $EXECPATH/$ICONNAME
}

echo "Downloading icons from Bitbucket"
if ! wget -O ClonoCalc/ClonoCalc_Icon.svg "$CLONOCALCLINKPREFIX/Icons/ClonoCalc_Icon.svg"; then
	echo "E: Failed download from '$CLONOCALCLINKPREFIX/Icons/ClonoCalc_Icon.svg'"
	exit
fi

if !  wget -O ClonoPlot/ClonoPlot_Icon.svg "$CLONOPLOTLINKPREFIX/Icons/ClonoPlot_Icon.svg"; then
	echo "E: Failed download from '$CLONOPLOTLINKPREFIX/Icons/ClonoPlot_Icon.svg'"
	exit
fi

echo "Create Launchers"
CreateLauncherForJar "ClonoCalc" "$(pwd)/ClonoCalc" "ClonoCalc.jar" ClonoCalc_Icon.svg
CreateLauncherForJar "ClonoPlot" "$(pwd)/ClonoPlot" "ClonoPlot.jar" ClonoPlot_Icon.svg
chmod 755 ClonoCalc/ClonoCalc.sh ClonoPlot/ClonoPlot.sh
chmod +x *.desktop

echo "Downloading tutoriales from Bitbucket"
#wget -O ClonoCalc_Tutorial.pdf "$CLONOCALCLINKPREFIX/ClonoCalc_Tutorial.pdf"
#wget -O ClonoPlot_Tutorial.pdf "$CLONOPLOTLINKPREFIX/ClonoPlot_Tutorial.pdf"
wget -O ClonoSuite-Tutorial.pdf "$CLONOSUITELINKPREFIX/ClonoSuite-Tutorial.pdf"

echo "Downloading samples for ClonoCalc from Bitbucket"
SAMPLESCLONOCALC=$(pwd)/samples_ClonoCalc
FORWARDPACKED=sample_R1_50k.fastq.gz
REVERSEPACKED=sample_R2_50k.fastq.gz

mkdir -p $SAMPLESCLONOCALC
cd $SAMPLESCLONOCALC
for f in $FORWARDPACKED $REVERSEPACKED
do
	if [ -r "$f" ]; then
		if gzip -t "$f"; then
			echo "Sample data '$f' already downloaded - skipping"
			continue
		else
			rm -f "$f"
		fi
	fi
	wget -O "$f" "$CLONOCALCLINKPREFIX/testData/$f"
done
cd ..

#add samples to ClonoCalc config
echo "OutputPath = $SAMPLESCLONOCALC" >> ClonoCalc/config.txt
echo "ForwardReadFile = $SAMPLESCLONOCALC/$FORWARDPACKED" >> ClonoCalc/config.txt
echo "ReverseReadFile = $SAMPLESCLONOCALC/$REVERSEPACKED" >> ClonoCalc/config.txt

echo "Downloading samples for ClonoPlot from Bitbucket"
SAMPLESCLONOPLOT=$(pwd)/samples_ClonoPlot
mkdir -p $SAMPLESCLONOPLOT
cd $SAMPLESCLONOPLOT
if [ -r testData.zip ] -a unzip -t testData.zip; then
	echo "I: ClonoPlot testData already available - skipping download"
else
	wget -O testData.zip "$CLONOPLOTLINKPREFIX/testData.zip"
fi
unzip testData.zip
rm testData.zip
cd ..

cd ClonoPlot/tmp
echo "$SAMPLESCLONOPLOT/samplesA" >> selectedTableFolders.txt
echo "$SAMPLESCLONOPLOT/samplesB" >> selectedTableFolders.txt

echo ",0,1" > serScriptJTreeExpansion.txt
echo ",0,1,4" > serTableJTreeExpansion.txt
echo "1011 597 110 214" > selectedWindowSize.txt

echo "Installation finished successfully. To start the applications, run"
echo "  ./ClonoCalc/ClonoCalc.sh   or"
echo "  ./ClonoPlot/ClonoPlot.sh"
exit
