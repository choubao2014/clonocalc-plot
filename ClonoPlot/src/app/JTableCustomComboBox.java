package app;

import java.util.HashMap;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.table.TableCellEditor;

public class JTableCustomComboBox extends JTableCustom {

	private static final long serialVersionUID = 1L;

	HashMap<Integer, TableCellEditor> tableCellEditorMap = new HashMap<Integer, TableCellEditor>();

	public TableCellEditor getCellEditor(int row, int column) {
		int modelColumn = convertColumnIndexToModel(column);

		if (modelColumn == 1 && tableCellEditorMap.containsKey(row))
			return tableCellEditorMap.get(row);
		else
			return super.getCellEditor(row, column);
	}

	public void clearComboBoxesMap() {
		tableCellEditorMap.clear();
	}

	public void addRowCombobox(int row, List<String> items) {
		addRowCombobox(row, items.toArray(new String[items.size()]));
	}

	public void addRowCombobox(int row, String items[]) {
		JComboBox<String> comboBox = new JComboBox<String>(items);
		DefaultCellEditor defaultCellEditor = new DefaultCellEditor(comboBox);
		tableCellEditorMap.put(row, defaultCellEditor);
	}
}
