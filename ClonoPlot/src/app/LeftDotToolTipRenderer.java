package app;

import java.awt.Component;
import java.awt.FontMetrics;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/*
 *  https://tips4java.wordpress.com/2008/11/12/left-dot-renderer/
 *  Posted by Rob Camick on November 12, 2008
 *  Simple text renderer that will display the text right justified with
 *  leading dots when the column width is not large enough to display the
 *  entire text.�
 */
class LeftDotToolTipRenderer extends DefaultTableCellRenderer {
	/**
	 * 
	 */
	private static final long serialVersionUID = 636343695510822153L;

	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		JLabel c = (JLabel) super.getTableCellRendererComponent(table, value,
				isSelected, hasFocus, row, column);
		// set a unique tooltip for each row
		c.setToolTipText(c.getText());

		// Determine the width available to render the text

		int availableWidth = table.getColumnModel().getColumn(column)
				.getWidth();
		availableWidth -= table.getIntercellSpacing().getWidth();
		Insets borderInsets = getBorder().getBorderInsets((Component) this);
		availableWidth -= (borderInsets.left + borderInsets.right);
		String cellText = getText();
		FontMetrics fm = getFontMetrics(getFont());

		// Not enough space so start rendering from the end of the string
		// until all the space is used up

		if (fm.stringWidth(cellText) > availableWidth) {
			String dots = "...";
			int textWidth = fm.stringWidth(dots);
			int i = cellText.length() - 1;

			for (; i > 0; i--) {
				textWidth += fm.charWidth(cellText.charAt(i));

				if (textWidth > availableWidth) {
					break;
				}
			}

			setText(dots + cellText.substring(i + 1));
		}
		return this;
	}
}