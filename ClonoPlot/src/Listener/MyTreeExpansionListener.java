package Listener;

import javax.swing.SwingUtilities;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;

import org.apache.log4j.Logger;

import app.PanelConfig;

public class MyTreeExpansionListener implements TreeExpansionListener {
	private static final Logger log = Logger
			.getLogger(MyTreeExpansionListener.class);

	private PanelConfig panelConfig = null;

	public MyTreeExpansionListener(PanelConfig panelConfig) {
		this.panelConfig = panelConfig;
	}

	@Override
	public void treeExpanded(final TreeExpansionEvent event) {
		log.debug(event.getPath().toString());
		// panelConfig.resizeTableTree();
		String fullPath = event.getPath().toString();
		fullPath = fullPath.substring(1, fullPath.length() - 1);
		String[] steps = fullPath.split(", ");
		if (steps.length == 3) {
			// log.debug("fixed");
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					panelConfig.collapsePathInScriptTree(event.getPath());
				}
			});
		}
	}

	@Override
	public void treeCollapsed(TreeExpansionEvent event) {
		// panelConfig.resizeTableTree();
		// log.debug("tree collapsed");
	}

}
