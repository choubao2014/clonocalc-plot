package Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;

import app.GlobalVars;

public class CheckBoxShowAdvancedOptionsListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent arg0) {
		JCheckBox cb = (JCheckBox) arg0.getSource();
		GlobalVars.singleton().setShowAdvancedOptions(cb.isSelected());
	}

}
