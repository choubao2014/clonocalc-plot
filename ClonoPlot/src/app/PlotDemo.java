package app;

//
// PlotDemo demo - REngine and graphics
//
// http://svn.rforge.net/org/trunk/rosuda/REngine/Rserve/test/PlotDemo.java
//

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import javaFX.SynchronousJFXDirChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import util.CreateScript;
import util.FileOperations;
import util.OSDetection.OperatingSys;
import util.ScriptParser.Script;

/**
 * A demonstration of the use of Rserver and graphics devices to create graphics
 * in R, pull them into Java and display them. It is a really simple demo.
 */
public class PlotDemo {
	private static final Logger log = Logger.getLogger(PlotDemo.class);

	GlobalVars gv;

	private RConnection c;
	private String device = "pdf"; // device we'll call (this would work with
	private int ppi = getPPI();
	private int plotObjectCounter = 0;

	// private final String previewFileName = "\"preview.pdf\"";
	private String previewFileName = "preview.pdf";
	private final String previewPrefix = "preview";
	private final String previewPostfix = ".pdf";
	private final String deviceType = "CairoJPEG";
	private final String deviceParameter = "";
	private final String rscriptStartRServe = "DO_NOT_DELETE_start_Rserve.txt";
	private String currentDir;
	RserveExtensions rserveExtensions = new RserveExtensions();

	// private final int defaultWidth = 800;
	// private final int defaultHeight = 800;

	// private final String deviceParameter =
	// ",quality=90,width = 8, height = 10, units = \"in\", res=300";

	// pretty much any bitmap device)

	public PlotDemo() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				startRserveProcess();
				plotDemoOpenConnection(new String[] {});
				initLibrarysAndFuncs();

				// call finish if GUI is ready and set up
				while (!GlobalVars.setReadyToPlot(true)) {
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						log.error(e, e);
					}
				}
			}
		}).start();
	}

	private void startRserveProcess() {
		try {
			Process proc = null;
			String pathToRscript = GlobalVars.mainScriptsDirAbsolutePath + File.separator + rscriptStartRServe;
			ProcessBuilder builder;
			if (GlobalVars.os == OperatingSys.Windows) {
				builder = new ProcessBuilder("Rscript", pathToRscript);
				Map<String, String> envs = builder.environment();
				envs.put("Path", envs.get("Path"));
				log.debug(envs.get("Path"));
			} else if (GlobalVars.os == OperatingSys.Mac) {
				builder = new ProcessBuilder("/usr/local/bin/Rscript", pathToRscript);
			} else {
				builder = new ProcessBuilder("Rscript", pathToRscript);
			}
			builder.redirectErrorStream(true);
			proc = builder.start();

			InputStream stdout = proc.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(stdout));

			String line;
			while ((line = reader.readLine()) != null) {
				log.info(line);
				if (line.contains("Rserv started in daemon mode") || line.contains("Starting Rserve..."))
					break;
			}

		} catch (Exception e) {
			log.error(e, e);
		}

	}

	public void stopRserve() {
		try {
			// close connection and stop process
			if (c != null && c.isConnected())
				c.shutdown();
		} catch (RserveException e) {
			log.error(e, e);
		}
	}

	public void testRemoteConnection(String serverIp) {
		serverIp = "52.35.75.193";
		try {
			if (c != null)
				c.close();
			// connect to Rserve (if the user specified a server at the command
			// line, use it, otherwise connect locally)
			c = new RConnection(serverIp);
			log.info("connected to Rserve");

			for (String file : listAllFilesInDir()) {
				System.out.println(file);
			}

		} catch (RserveException rse) { // RserveException (transport layer -
			// e.g. Rserve is not running)
			log.error(rse, rse);
		} catch (Exception e) { // something else
			log.error(e, e);
		}
	}

	// get available remote files
	public String[] listAllFilesInDir() {
		try {
			return rEval("list.files()").asStrings();
		} catch (REXPMismatchException e) {
			log.error(e, e);
		}
		return null;
	}

	public void plotDemoOpenConnection(String args[]) {

		// testRemoteConnection("");

		try {
			if (c != null)
				c.close();
			// connect to Rserve (if the user specified a server at the command
			// line, use it, otherwise connect locally)
			c = new RConnection((args.length > 0) ? args[0] : "127.0.0.1");
			log.info("connected to Rserve");

			// if Cairo is installed, we can get much nicer graphics, so try to
			// load it
			if (c.parseAndEval("suppressWarnings(require('Cairo',quietly=TRUE))").asInteger() > 0)
				device = deviceType; // great, we can use Cairo device
			// else
			// System.out
			// .println("(consider installing Cairo package for better bitmap
			// output)");
			currentDir = getwd();

			log.info(getRVersion());

		} catch (RserveException rse) { // RserveException (transport layer -
			// e.g. Rserve is not running)
			log.error(rse, rse);
		} catch (REXPMismatchException mme) { // REXP mismatch exception (we got
			// something we didn't think we
			// get)
			log.error(mme, mme);
		} catch (Exception e) { // something else
			log.error(e, e);
		}
	}

	private void openDevice() {
		openDevice(0, 0);
	}

	private void openDevice(int width, int height) {
		try {
			// if (width == 0 && height == 0) {
			// width = defaultWidth;
			// height = defaultHeight;
			// }
			// calc aspect ratio
			// double aspectRatio = (double) width / (double) height;
			// if (width > height) {
			// width = defaultWidth;
			// height = (int) (width * 1 / aspectRatio);
			// } else {
			// height = defaultHeight;
			// width = (int) (height * aspectRatio);
			// }

			// width *= pixelPerInch;
			// height *= pixelPerInch;

			width = 10;
			height = 10;

			previewFileName = previewPrefix + new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date())
					+ previewPostfix;

			// we are careful here - not all R binaries support jpeg
			// so we rather capture any failures
			String command = device + "(\"" + previewFileName + "\",width=" + width + ",height=" + height
					+ deviceParameter + ")";
			System.out.println(command);
			REXP xp = rEval(command);

			if (xp.inherits("try-error")) { // if the result is of the class
											// try-error then there was a
											// problem
				log.error("Can't open " + device + " graphics device:\n" + xp.asString());
				// this is analogous to 'warnings', but for us it's sufficient
				// to get just the 1st warning
				REXP w = c.eval("if (exists('last.warning') && length(last.warning)>0) names(last.warning)[1] else 0");
				if (w.isString())
					log.error(w.asString());
				return;
			}
		} catch (RserveException rse) { // RserveException (transport layer -
			// e.g. Rserve is not running)
			log.error(rse, rse);
		} catch (REXPMismatchException mme) { // REXP mismatch exception (we got
			// something we didn't think we
			// get)
			log.error(mme, mme);
		} catch (Exception e) { // something else
			log.error(e, e);
		}
	}

	public void plotTest() {
		try {
			// try to open a device
			openDevice();

			// ok, so the device should be fine - let's plot - replace this by
			// any plotting code you desire ...
			rVoidEval("data(iris); attach(iris); plot(Sepal.Length, Petal.Length, col=unclass(Species)); dev.off()");

			// There is no I/O API in REngine because it's actually more
			// efficient to use R for this
			// we limit the file size to 1MB which should be sufficient and we
			// delete the file as well
			REXP xp = rEval("r=readBin(" + previewFileName + ",'raw',1024*1024); unlink(" + previewFileName + "); r");

			// now this is pretty boring AWT stuff - create an image from the
			// data and display it ...
			Image img = Toolkit.getDefaultToolkit().createImage(xp.asBytes());

			Frame f = new Frame("Test image");
			f.add(new PlotPic(img));
			f.addWindowListener(new WindowAdapter() { // just so we can close
														// the window
				public void windowClosing(WindowEvent e) {
					f.dispose();
				}
			});
			f.pack();
			f.setVisible(true);

			// close RConnection, we're done
			// c.close();
		} catch (REXPMismatchException mme) { // REXP mismatch exception (we got
												// something we didn't think we
												// get)
			System.out.println(mme);
			mme.printStackTrace();
		} catch (Exception e) { // something else
			System.out.println("Something went wrong, but it's not the Rserve: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public String rVoidEval(String command) {
		// try {
		// ok, so the device should be fine - let's plot - replace this by
		// any plotting code you desire ...
		// log.info(command);
		// lastError <- tryCatch(a+b, error = c)
		// if (tryCatch)
		// command = "try({" + command + "})";
		// System.out.println(command);
		return rserveExtensions.TryVoidEval(c, command);

		// } catch (RserveException rse) { // RserveException (transport layer -
		// // e.g. Rserve is not running)
		// log.error("Rserve not running? " + rse, rse);
		// try {
		// REXP r = c.eval("traceback()");
		// String error = r.asString();
		// log.error(error);
		// } catch (RserveException | REXPMismatchException e) {
		// log.error(e, e);
		// }
		//
		// } catch (Exception e) { // something else
		// log.error("Something went wrong, but it's not the Rserve: " + e, e);
		// try {
		// REXP r = c.eval("x");
		// String error = r.asString();
		// log.error(error);
		// } catch (RserveException | REXPMismatchException error) {
		// log.error(error, error);
		// }
		// }
	}

	public REXP rEval(String command) {
		// try {
		// ok, so the device should be fine - let's plot - replace this by
		// any plotting code you desire ...
		// log.info(command);
		return rserveExtensions.TryEval(c, "try(" + command + ")");

		// } catch (RserveException rse) { // RserveException (transport layer -
		// // e.g. Rserve is not running)
		// log.error("Rserve not running? " + rse, rse);
		// // } catch (REXPMismatchException mme) { // REXP mismatch exception
		// // (we got
		// // // something we didn't think we
		// // // get)
		// // log.error(mme, mme);
		// } catch (Exception e) { // something else
		// log.error("Something went wrong, but it's not the Rserve: " + e, e);
		// }
		// return null;
	}

	public String parseAndEvalRetStr(String command) {
		try {
			// ok, so the device should be fine - let's plot - replace this by
			// any plotting code you desire ...
			REXP xp = c.parseAndEval(command);
			return xp.asString();

		} catch (RserveException rse) { // RserveException (transport layer -
										// e.g. Rserve is not running)
			System.out.println(rse);
		} catch (REXPMismatchException mme) { // REXP mismatch exception (we got
												// something we didn't think we
												// get)
			System.out.println(mme);
			mme.printStackTrace();
		} catch (Exception e) { // something else
			System.out.println("Something went wrong, but it's not the Rserve: " + e.getMessage());
			e.printStackTrace();
		}
		return "error";
	}

	public void initLibrarysAndFuncs() {
		// load libraries
		for (String rCodeLoadLibrary : CreateScript.getRCodeLoadLibrarys())
			rVoidEval(rCodeLoadLibrary);

		// load scripts
		for (String rCodeLoadScript : CreateScript.getRCodeLoadScripts())
			rVoidEval(rCodeLoadScript);
		log.debug("initLibrarysAndFuncs finished");
	}

	public void updateDataTables() {
		gv = GlobalVars.singleton();

		// try to delete old list
		rVoidEval("rm(" + CreateScript.tmpListName + ")");

		// read tables
		for (String rCodeReadTable : CreateScript.getRCodeReadTables(gv.getSelectedTables()))
			rVoidEval(rCodeReadTable);

		// create R list with tables
		rVoidEval(CreateScript.getRCodeCreateList(gv.getSelectedTablesNames()));

		// preprocess list
		rVoidEval(CreateScript.getRCodePreprocessingList(gv, false));

		log.debug("tables updated");
	}

	public void updateDataTablesNames() {
		rVoidEval(CreateScript.getRCodeUpdateNamesList(GlobalVars.singleton().getSelectedTablesNames()));
		log.debug("tables names updated");
	}

	public String getwd() {
		REXP x = rserveExtensions.TryEval(c, "getwd()");
		try {
			return x.asString();
		} catch (REXPMismatchException e1) {
			log.error(e1, e1);
			return "";
		}
	}

	public String getRVersion() {
		REXP x = rserveExtensions.TryEval(c, "as.character(R.version[\"version.string\"])");
		try {
			return x.asString();
		} catch (REXPMismatchException e1) {
			log.error(e1, e1);
			return e1.getMessage();
		}
	}

	public void plotScriptPDFBoxPerformance(Script script) {
		updateDataTablesNames();

		String scriptAbsolutePath = script.getAbsolutePathToScript();
		String command = "source(\"" + scriptAbsolutePath.replaceAll("\\\\", "/") + "\"); ";

		String ret = rVoidEval(command);

		if (ret != null)
			return;

		String plotObjectName = getPlotObjectName(scriptAbsolutePath, script.getParameterLine());

		String tmpTitle = script.tryToGetValue("title");
		String title = tmpTitle.substring(1, tmpTitle.length() - 1);
		File tmpResultFile = new File(currentDir + File.separator + title + previewPostfix);

		float fwidth = Float.parseFloat(script.tryToGetValue("width"));
		float fheight = Float.parseFloat(script.tryToGetValue("height"));
		BufferedImage img = getPDFBoxBufferedImage(plotObjectName, fwidth, fheight, title, tmpResultFile);

		if (img == null)
			return;

		JFrame f = new JFrame(title);

		f.getContentPane().setLayout(new GridBagLayout());
		((JComponent) f.getContentPane()).setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.blue));
		f.getContentPane().add(new JLabel(new ImageIcon(img)));

		f.pack();
		f.setVisible(true);

		final DefaultTableModel tableModel = script.getDefaultTableModel();
		// timer updates window every intervall if the window was resized
		Timer t = new Timer(100, new ActionListener() {
			// int width = f.getContentPane().getWidth();
			// int height = f.getContentPane().getHeight();
			float flastWorkingWidth;
			float flastWorkingHeight;
			BufferedImage img;

			@Override
			public void actionPerformed(ActionEvent e) {
				// System.out.println(f.getContentPane().getWidth() + " "
				// + f.getContentPane().getHeight());
				// System.out.println(f.getContentPane().getComponent(0)
				// .getWidth()
				// + " "
				// + f.getContentPane().getComponent(0).getHeight());
				// if (f.getContentPane().getWidth() == width
				// && f.getContentPane().getHeight() == height)
				// return;
				if (f.getContentPane().getWidth() == f.getContentPane().getComponent(0).getWidth()
						&& f.getContentPane().getHeight() == f.getContentPane().getComponent(0).getHeight())
					return;
				log.info("repaint " + title);
				// System.out.println(frame.getWidth() + ":" +
				// frame.getHeight());
				// System.out.println(width + ":" + height);
				// JLabel jbl = (JLabel) f.getContentPane().getComponent(0);
				// System.out.println(f.getContentPane().getWidth() + ":" +
				// f.getContentPane().getHeight());
				float fwidth = (float) f.getContentPane().getWidth() / (float) ppi;
				float fheight = (float) f.getContentPane().getHeight() / (float) ppi;
				img = getPDFBoxBufferedImage(plotObjectName, fwidth, fheight, title, tmpResultFile);

				if (img != null) {
					flastWorkingWidth = fwidth;
					flastWorkingHeight = fheight;
					// update size values in gui
					for (int i = 0; i < tableModel.getRowCount(); i++) {
						String paraName = (String) tableModel.getValueAt(i, 0);
						if (paraName.equals("width")) {
							tableModel.setValueAt(Double.toString(fwidth), i, 1);
						} else if (paraName.equals("height")) {
							tableModel.setValueAt(Double.toString(fheight), i, 1);
						}
					}
				} else {
					img = getPDFBoxBufferedImage(plotObjectName, flastWorkingWidth, flastWorkingHeight, title,
							tmpResultFile);
				}
				if (img != null) {
					f.getContentPane().removeAll();
					f.getContentPane().add(new JLabel(new ImageIcon(img)));
					f.pack();
				}
				// width = f.getContentPane().getWidth();
				// height = f.getContentPane().getHeight();
			}
		});
		t.start();

		f.addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent evt) {
				t.restart();
			}
		});

		// f.add(new PlotPic(img));
		f.addWindowListener(new WindowAdapter() { // just so we can
													// close
			// the window
			public void windowClosing(WindowEvent e) {
				img.getGraphics().dispose();
				img.flush();
				f.dispose();
				t.stop();
			}
		});
		// f.addComponentListener(new MyComponentListener(scriptAbsolutePath,
		// scriptParameterFinal, width, height, title, this));

		f.addMouseListener(new MouseAdapter() {

			public void mousePressed(MouseEvent event) {
				if (((event.getModifiers() & InputEvent.BUTTON3_MASK) != 0)) {
					showMenu(event.getX(), event.getY());
				}
			}

			protected void showMenu(int x, int y) {
				JPopupMenu popup = new JPopupMenu();
				JMenuItem mi = new JMenuItem("Save as");
				popup.add(mi);
				mi.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent event) {
						saveImageAs();
					}
				});
				popup.show(f, x, y);
			}

			protected void saveImageAs() {
				List<javafx.stage.FileChooser.ExtensionFilter> extensionFilters = new ArrayList<javafx.stage.FileChooser.ExtensionFilter>();
				extensionFilters.add(new ExtensionFilter("PDF", "*.pdf"));
				extensionFilters.add(new ExtensionFilter("JPG", "*.jpg"));
				extensionFilters.add(new ExtensionFilter("BMP", "*.bmp"));
				extensionFilters.add(new ExtensionFilter("GIF", "*.gif"));
				extensionFilters.add(new ExtensionFilter("PNG", "*.png"));
				String[] allowedTypes = new String[] { "pdf", "jpg", "bmp", "gif", "png" };

				File target = SynchronousJFXDirChooser.saveFile("Select a file", extensionFilters);

				if (target == null)
					return;

				String fileExtension = FileOperations.getFileExtension(target.getName());
				if (!Arrays.asList(allowedTypes).contains(fileExtension)) {
					String newTitle = f.getTitle() + " ----- Invalid File extension: \"" + target.getName()
							+ "\" -----";
					changeTitleForSec(f, newTitle, 10);
					log.info("invalid fileex: " + target.getAbsolutePath());
					return;
				}

				try {
					if (fileExtension.equals("pdf"))
						Files.copy(tmpResultFile.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);
					else {
						BufferedImage img = getBufferedImageFromPDFFile(tmpResultFile);
						ImageIOUtil.writeImage(img, target.getAbsolutePath(), 300);
					}
					String newTitle = f.getTitle() + " ----- Saved plot to: \"" + target.getAbsolutePath() + "\" -----";
					changeTitleForSec(f, newTitle, 5);
				} catch (IOException e) {
					String newTitle = f.getTitle() + " ----- Error message: \"" + e.getMessage() + "\" -----";
					changeTitleForSec(f, newTitle, 30);
					log.error(e, e);
				}
			}
		});
	}

	public static void changeTitleForSec(JFrame f, String newTitle, int durationInSec) {
		String oldTitle = f.getTitle();
		f.setTitle(newTitle);
		Timer timerLbl = new Timer(durationInSec * 1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				((Timer) e.getSource()).stop();
				f.setTitle(oldTitle);
			}
		});
		timerLbl.start();
	}

	public BufferedImage getPDFBoxBufferedImage(String plotObjectName, float width, float height, String title,
			File tmpResultFile) {

		// pdf(file = paste0(title, ".pdf"), height = height, width = width)
		// print(myplot)
		// dev.off()
		String command = "pdf(file = paste0(\"" + title + "\", \".pdf\"), height = " + height + ", width = " + width
				+ ");" + "print(" + plotObjectName + ");" + "dev.off();";

		// System.out.println(command);
		String ret = rVoidEval(command);

		if (ret != null)
			return null;

		return getBufferedImageFromPDFFile(tmpResultFile);
	}

	/**
	 * Creates an r plot object in the Rserve session. This allows to resize a
	 * plot without doing the recalculating
	 */
	public String getPlotObjectName(String scriptAbsolutePath, final String scriptParameterFinal) {

		// try to open a device
		// openDevice(width, height);

		String scriptParameter = scriptParameterFinal;

		String filename = new File(scriptAbsolutePath).getName();
		String filenamewoex = FileOperations.getNamewoExtension(filename);
		String plotObjectName = "obj_" + plotObjectCounter;

		String command = "pdf(\"tmp.pdf\");" + plotObjectName + " <- " + filenamewoex + "(tmpList" + scriptParameter
				+ "); ";

		// increase counter to support simultaneous plots
		plotObjectCounter++;
		// System.out.println(command);
		String ret = rVoidEval(command);

		if (ret != null)
			return null;

		return plotObjectName;
	}

	public BufferedImage getBufferedImageFromPDFFile(File file) {
		try {
			// PDDocument document = PDDocument.load(file);

			// String tmpFilePath = System.getProperty("java.io.tmpdir")
			// + File.separator + "scratch.tmp";
			// File tmpFile = new File(tmpFilePath);
			// if (tmpFile.exists())
			// tmpFile.delete();
			// RandomAccess scratchFile = new RandomAccessFile(tmpFile, "rw");
			PDDocument document = PDDocument.load(file, MemoryUsageSetting.setupMainMemoryOnly());
			PDFRenderer pdfRenderer = new PDFRenderer(document);

			BufferedImage img = pdfRenderer.renderImageWithDPI(document.getNumberOfPages() - 1, ppi, ImageType.RGB);
			document.close();
			// Runtime runtime = Runtime.getRuntime();
			// long memory = runtime.totalMemory() - runtime.freeMemory();
			// System.out.println("Used memory is bytes: " + memory);
			// System.out.println("Used memory is megabytes: "
			// + bytesToMegabytes(memory));

			return img;

		} catch (IOException ex) {
			log.error(ex, ex);
		}
		return null;
	}

	public static long bytesToMegabytes(long bytes) {
		return bytes / MEGABYTE;
	}

	private static final long MEGABYTE = 1024L * 1024L;

	public static int getPPI() {
		Dimension screen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		log.info("screen width: " + screen.getWidth());
		log.info("screen height: " + screen.getHeight());
		int pixelPerInch = java.awt.Toolkit.getDefaultToolkit().getScreenResolution();
		log.info("pixelPerInch: " + pixelPerInch);
		return pixelPerInch;

		// double height = screen.getHeight() / pixelPerInch;
		// double width = screen.getWidth() / pixelPerInch;
		// double x = Math.pow(height, 2);
		// double y = Math.pow(width, 2);
	}

	public void replot() {

	}

}