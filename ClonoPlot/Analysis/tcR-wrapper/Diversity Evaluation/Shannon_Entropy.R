#'IMPORTANT INFORMATION
#'This is only wrapper script for a function from the R tcR-package.
#'To get more details about the tcR-package please visit
#'https://cran.r-project.org/web/packages/tcR/index.html
#'or
#'http://imminfo.github.io/tcr/
#'---------------------------------------------------------------------------------------------
#' @description
#' Shannon Entropy on read count in a barplot. (INFO: This is a wrapper script for a function from the R tcR-package)
#' 
#' @param title string() Set the title of the plot
#' @param width float() Set the width of the diagram.
#' @param height float() Set the height of the diagram.

Shannon_Entropy <- function(dataFrameList, height = 5, width = 15, title="Shannon entropy") {
	if(width == 15)
		width = 1 * length(dataFrameList)
	# Compute diversity of repertoire using Chao index.
	repDiversity(dataFrameList, 'entropy', 'read.count')
	div <- sapply(dataFrameList, function (x) entropy(x$Read.count, .do.norm = T))

	d <- data.frame(x=names(dataFrameList), Value=div)
	plot <- ggplot(d, aes(x, Value)) + geom_bar(stat="identity") +
	  theme(axis.text.x = element_text(angle = 90, hjust = 1)) + labs(x="")
	
	return(plot)
	
	#pdf(file = paste0(title, ".pdf"), height = height, width = width)
	#par(mar=c(10,4,1.5,0))
	#barplot(div, main=title, xlab="", las=2)
	#dev.off()
}
