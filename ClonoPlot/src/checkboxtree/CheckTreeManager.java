package checkboxtree;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.JCheckBox;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeCellEditor;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import app.GlobalVars;

//@author Santhosh Kumar T - santhosh@in.fiorano.com 
public class CheckTreeManager extends MouseAdapter implements
		TreeSelectionListener {
	private CheckTreeSelectionModel selectionModel;
	private JTree tree = new JTree();
	int hotspot = new JCheckBox().getPreferredSize().width;
	private int mousePressedX = 0, mousePressedY = 0;

	public CheckTreeManager(JTree tree, boolean leafEdit) {
		if (leafEdit) {
			tree.setEditable(true);
			DefaultTreeCellEditor editor = new MyTreeCellEditor(tree,
					(DefaultTreeCellRenderer) tree.getCellRenderer());
			tree.setCellEditor(editor);
		}

		this.tree = tree;
		selectionModel = new CheckTreeSelectionModel(tree.getModel());
		tree.setCellRenderer(new CheckTreeCellRenderer(tree.getCellRenderer(),
				selectionModel));
		tree.addMouseListener(this);
		selectionModel.addTreeSelectionListener(this);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// System.out.println("mouse pressed" + e.getX() + " " + e.getY());
		mousePressedX = e.getX();
		mousePressedY = e.getY();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// System.out.println("mouse released" + e.getX() + " " + e.getY());
		int mouseReleasedX = e.getX();
		int mouseReleasedY = e.getY();
		if (Math.abs(mouseReleasedX - mousePressedX) > 5
				|| Math.abs(mouseReleasedY - mousePressedY) > 5)
			return;

		TreePath path = tree.getPathForLocation(mouseReleasedX, mouseReleasedY);

		if (path == null)
			return;
		if (e.getX() > tree.getPathBounds(path).x + hotspot)
			return;

		boolean selected = selectionModel.isPathSelected(path, true);
		selectionModel.removeTreeSelectionListener(this);

		try {
			if (selected) {
				GlobalVars.singleton().delSampleFromTableAndTree(path, this);
			} else {
				GlobalVars.singleton().addSampleToTableAndTree(path, this);
			}
		} finally {
			selectionModel.addTreeSelectionListener(this);
			tree.treeDidChange();
		}
	}

	// buggy, do not triggered on mac os x (vmware)
	public void mouseClicked(MouseEvent me) {
		// TreePath path = tree.getPathForLocation(me.getX(), me.getY());
		// if (path == null)
		// return;
		// if (me.getX() > tree.getPathBounds(path).x + hotspot)
		// return;
		//
		// boolean selected = selectionModel.isPathSelected(path, true);
		// selectionModel.removeTreeSelectionListener(this);
		//
		// try {
		// if (selected)
		// selectionModel.removeSelectionPath(path);
		// else
		// selectionModel.addSelectionPath(path);
		// } finally {
		// selectionModel.addTreeSelectionListener(this);
		// tree.treeDidChange();
		// panelConfig.updateSelectedTableCounter();
		// }
	}

	public CheckTreeSelectionModel getSelectionModel() {
		return selectionModel;
	}

	public void valueChanged(TreeSelectionEvent e) {
		tree.treeDidChange();
	}

	private static class MyTreeCellEditor extends DefaultTreeCellEditor {

		public MyTreeCellEditor(JTree tree, DefaultTreeCellRenderer renderer) {
			super(tree, renderer);
		}

		@Override
		public Object getCellEditorValue() {
			String value = (String) super.getCellEditorValue();
			return new Resource(value);
		}

		@Override
		public boolean isCellEditable(EventObject e) {
			return super.isCellEditable(e)
					&& ((TreeNode) lastPath.getLastPathComponent()).isLeaf();
		}
	}

	private static class Resource {

		String name;

		public Resource(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		@SuppressWarnings("unused")
		public void setName(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return getName();
		}
	}

}