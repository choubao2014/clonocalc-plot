﻿param([switch] $UninstallFirst,
      [switch] $UninstallOnly,
      [switch] $NoInstall,
      [switch] $Force32bit)

$ErrorActionPreference = 'Stop'

$Script:LogFile      = 'JavaUpdate.log'
$Script:ConfigFile   = 'JavaUpdate.config.txt'
$Script:LastId32File = 'JavaUpdateLastID32.txt'
$Script:LastId64File = 'JavaUpdateLastID64.txt'

## Author: Joakim Svendsen, Copyright 2013, all rights reserved.
## Mail: "joakimbs" using Google's mail services.

function MyLog {
    param([string] $Message)
    (Get-Date).ToString('yyyy-MM-dd HH:mm:ss') + "`t$Message" | Out-File -FilePath $Script:LogFile -Append
    Write-Host (Get-Date).ToString('yyyy-MM-dd HH:mm:ss') "`t$Message"
}

function Get-ConfigData {
    
    param([ValidateScript({Test-Path -PathType Leaf -Path $_})] [string] $ConfigFile)
    
    $ConfigData = @{}

    foreach ($Line in (Get-Content -Path $ConfigFile)) {
        
        $Key, $Value = $Line -split '\s*=\s*'
        
        $ConfigData.$Key = $Value
        
    }
    
    if (-not $ConfigData.ContainsKey('DownloadURL') -or `
          -not $ConfigData.ContainsKey('32-bit') -or `
          -not $ConfigData.ContainsKey('64-bit') -or `
          -not $ConfigData.ContainsKey('UserAgent') -or `
          -not $ConfigData.ContainsKey('InstallString') -or `
          -not $ConfigData.ContainsKey('UninstallSwitches') -or `
          -not $ConfigData.ContainsKey('UninstallTimeout') -or `
          -not $ConfigData.ContainsKey('UninstallDisplayNameWildcardString')) {
        
        MyLog "Error. A required field is missing in $Script:ConfigFile ('DownloadURL', '32-bit', '64-bit', 'UserAgent', 'InstallString', 'UninstallDisplayNameWildcardString', 'UninstallTimeout' or 'UninstallString')"
        exit 1
        
    }
    
    $ConfigData
    
}

function Get-HtmlString {
    
    param([string] $Url,
          [string] $UserAgent)
    
    $WebClient = New-Object System.Net.WebClient
    $WebClient.Headers['User-Agent'] = $UserAgent
    
    $ErrorActionPreference = 'SilentlyContinue'
    $HtmlString = $WebClient.DownloadString($Url)
    
    if ($?) {
        $ErrorActionPreference = 'Stop'
        return $HtmlString
    }
    else {
        $ErrorActionPreference = 'Stop'
        MyLog "Error downloading ${Url}: $($Error[0])"
        exit 2
    }
    
}

function Get-UrlsFromHtml {
    
    param([string] $HtmlString,
          $ConfigData)
    
    #$HtmlString = $HtmlString -replace '\s+', ' '

    try {
        Add-Type -Path (Join-Path (Get-Location) HtmlAgilityPack.dll) -ErrorAction Stop
    }
    catch {
        MyLog ("Failed to load HtmlAgilityPack.dll from " + (Get-Location) + ': ' + $Error[0])
        exit 3
    }
    try {
        $HtmlDoc = New-Object HtmlAgilityPack.HtmlDocument
        $HtmlDoc.LoadHtml($HtmlString)
        
        if ($env:PROCESSOR_ARCHITECTURE -ieq 'x86' -or $Force32bit) {
            $Url = $HtmlDoc.DocumentNode.SelectNodes("//a[@title=`"$($ConfigData['32-bit'])`"]") |
                Select -Last 1 -Expand Attributes |
                Where { $_.Name -eq 'href' } |
                Select -Exp Value

            if ($Url -match '^https?://.+BundleId=(\d+)') {
                # Check if it's different from the current last ID in the file for 32-bit
                if (-not (Test-Path -PathType Leaf -Path $Script:LastId32File)) {
                    MyLog "Error finding last 32-bit ID file: $Script:LastId32File"
                    exit 9
                }
                
                if (((Get-Content -Path $Script:LastId32File) -join '' -replace '\s+') -ne $Matches[1]) {
                    return $Url, $Matches[1]
                }
                else {
                    MyLog "Already have the latest 32-bit version of Java."
                    exit 8
                }
            }
            else {
                MyLog "Error. The retrieved 32-bit URL does not seem to be valid ($Url)"
                exit 5
            }

        }
        
        elseif ($env:PROCESSOR_ARCHITECTURE -ieq 'AMD64') {
            
            $Url = $HtmlDoc.DocumentNode.SelectNodes("//a[@title=`"$($ConfigData['64-bit'])`"]") |
                Select -Last 1 -Expand Attributes |
                Where { $_.Name -eq 'href' } |
                Select -Exp Value
            
            if ($Url -match '^https?://.+BundleId=(\d+)') {
                # Check if it's different from the current last ID in the file for 64-bit
                if (-not (Test-Path -PathType Leaf -Path $Script:LastId64File)) {
                    MyLog "Error finding last 64-bit ID file: $Script:LastId32File"
                    exit 9
                }
                
                if (((Get-Content -Path $Script:LastId64File) -join '' -replace '\s+') -ne $Matches[1]) {
                    return $Url, $Matches[1]
                }
                else {
                    MyLog "Already have the latest 64-bit version of Java."
                    exit 8
                }
            }
            else {
                MyLog "Error. The retrieved 64-bit URL does not seem to be valid ($Url)"
                exit 5
            }
            
        }
        
        else {
            MyLog "Error. Unknown processor architecture. Exiting."
            exit 6
        }

    }
    catch {
        MyLog ("Something went wrong with parsing the HTML: " + $Error[0])
        exit 4
    }

}

$ConfigData = Get-ConfigData $Script:ConfigFile

if (-not $UninstallOnly) {
    $HtmlString = Get-HtmlString $ConfigData['DownloadURL'] $ConfigData['UserAgent']
    $JavaDownloadUrl, $Id = Get-UrlsFromHtml $HtmlString $ConfigData
}
# Uninstall stuff. Try to uninstall everything whose DisplayName matches the wildcard string in the config file.
if ($UninstallFirst -or $UninstallOnly) {
    
    $UninstallRegPath = 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall'
    $RegUninstallString = Get-ChildItem $UninstallRegPath | ForEach-Object {
        Get-ItemProperty ($_.Name -replace '^HKEY_LOCAL_MACHINE', 'HKLM:') # ugh...
    } | Where-Object { $_.DisplayName -like $ConfigData.'UninstallDisplayNameWildcardString' } | Select -ExpandProperty UninstallString
    
    $JavaGUIDs = @()
    $RegUninstallString | ForEach-Object {
        if ($_ -match '(\{[^}]+\})') {
            $JavaGUIDs += $Matches[1]
        }
    }
    
    $Failed = $false
    # Now this is a fun error on uninstalls:
    # "There was a problem starting C:\Program Files\Java\jre7\bin\installer.dll. The specified module could not be found"
    # So... Start the uninstall in a job, return the exit code (if/when done), wait for the specified number of seconds,
    # and kill rundll32.exe if it takes longer than the timeout... ugh. Oracle, I curse you! Currently testing with 7u45,
    # which consistently does this if it's installed silently more than once ("reconfigured" (and broken) the second time).
    foreach ($JavaGUID in $JavaGUIDs) {
        $Result = Start-Job -Name UninstallJob -ScriptBlock {
            Start-Process -Wait -NoNewWindow -PassThru -FilePath msiexec.exe -ArgumentList $args
            } -ArgumentList ("/X$JavaGUID " + $ConfigData.UninstallSwitches)
        
        Wait-Job -Name UninstallJob -Timeout $ConfigData.UninstallTimeout | Out-Null
        $Timeout = 0
        while (1) {
            
            if ((Get-Job -Name UninstallJob).State -eq 'Completed') {
                
                MyLog "Presumably successfully uninstalled Java with GUID: $JavaGUID"
                break
                
                #$JobResult = Receive-Job -Name UninstallJob
                #if ($JobResult.ExitCode -eq 0) {
                    
                #    break
                #}
                #else {
                #    MyLog ("Failed to uninstall Java with GUID: $JavaGUID (" + $Error[0] + ')')
                #    $Failed = $true
                #    break
                #}
            }
            # Let's kill rundll32.exe ... ugh.
            else {
                Get-Process -Name rundll32 -ErrorAction SilentlyContinue | Stop-Process -Force -ErrorAction SilentlyContinue
                Start-Sleep -Seconds 10
                $Timeout += 10
                if ($Timeout -ge 40) {
                    MyLog "Timed out waiting for rundll32.exe to die or job to finish."
                    $Failed = $true
                    break
                }
            }
            
            Wait-Job -Name UninstallJob -Timeout $ConfigData.UninstallTimeout | Out-Null
            
        } # end of infinite while (1)

        Remove-Job -Name UninstallJob
        
    } # end of foreach JavaGUID

    if ($Failed) { MyLog "Exiting because a Java uninstall previously failed."; exit 10 }

    # Reset the ID files.
    'Uninstalled' | Out-File $Script:LastId32File
    'Uninstalled' | Out-File $Script:LastId64File
    
}

if ($UninstallOnly) { exit 11 }
if ($NoInstall) { MyLog "-NoInstall was specified. Exiting."; exit 12 }

$JavaTempFilePath = Join-Path $env:TEMP 'JavaUpdateTemp.exe'
MyLog "Trying to download new Java from URL: $JavaDownloadUrl"
$JavaDownloader = New-Object Net.WebClient
$ErrorActionPreference = 'SilentlyContinue'
$JavaDownloader.DownloadFile($JavaDownloadUrl, $JavaTempFilePath)
if (-not $?) {
    MyLog ("Error. Failed to download Java installer from ${Url}: " + $Error[0])
    exit 7
}

$ErrorActionPreference = 'Stop'

try {
    
    $Install = Start-Process -Wait -NoNewWindow -PassThru -FilePath $JavaTempFilePath -ArgumentList $ConfigData['InstallString'] -ErrorAction Stop

    if ($Install.ExitCode -eq 0) {
        MyLog "Successfully updated Java."
        if ($Env:PROCESSOR_ARCHITECTURE -eq 'x86' -or $Force32bit) { $Id | Out-File $Script:LastId32File }
        else { $Id | Out-File $Script:LastId64File }
    }
    else {
        MyLog ("Failed to update Java. Exit code of installer: " + $Install.ExitCode)
    }

}
catch {
    MyLog "Failed to install Java: $($Error[0])"
}
