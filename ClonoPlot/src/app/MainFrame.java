package app;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import javaFX.JavaFXInitializer;

import javafx.application.Platform;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import util.FileOperations;

public class MainFrame {
	// private static final Logger log = Logger.getLogger(MainFrame.class);

	JFrame frame = new JFrame("ClonoPlot");

	JPanel currentPanel;

	private enum ProgramState {
		config, running, finished
	};

	ProgramState programState = ProgramState.config;

	PanelFinished panelFinished = new PanelFinished(this, frame);
	PanelLoading panelLoading = new PanelLoading(this, frame);
	PanelConfig panelConfig = new PanelConfig(this, frame);

	public MainFrame() {
		frame.setMinimumSize(new Dimension(500, 450));
		// Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		// frame.setMaximumSize(new Dimension(screenSize.width,
		// screenSize.height));
		// frame.setSize(new Dimension(700, 400));
		// frame.setMaximumSize(new Dimension(600, 350));

		// add panelConfig only once and take changes into the panel
		panelConfig.repaintConfigScreen();
		frame.add(panelConfig.getPanel());
		currentPanel = panelConfig.getPanel();
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent we) {
				showExitDialog(frame, programState);
			}
		});
		frame.setSize(700, 600);

		// restore window size and position
		GlobalVars gv = GlobalVars.singleton();
		String windowSize = FileOperations.readFirstLineInString(gv.tmpFolder
				+ File.separator + gv.selectedWindowSize);
		if (windowSize != null) {
			String windowSizeArr[] = windowSize.split(" ");
			if (windowSizeArr != null && windowSizeArr.length == 4) {
				frame.setSize(Integer.parseInt(windowSizeArr[0]),
						Integer.parseInt(windowSizeArr[1]));
				frame.setLocation(Integer.parseInt(windowSizeArr[2]),
						Integer.parseInt(windowSizeArr[3]));
			}
		}

		frame.setVisible(true);

	}

	public void showExitDialog(Component c, ProgramState programState) {
		// save folder stuff
		GlobalVars.singleton().onExit();
		panelConfig.saveSelected(true);

		// save window size and position
		GlobalVars gv = GlobalVars.singleton();
		Dimension dim = frame.getSize();
		java.awt.Point loc = frame.getLocation();
		FileOperations.writeStringToFile(gv.tmpFolder + File.separator
				+ gv.selectedWindowSize, dim.width + " " + dim.height + " "
				+ (int) loc.getX() + " " + (int) loc.getY());

		// distinguish current panel
		if (programState == ProgramState.config) {
			// if (JOptionPane.showOptionDialog(c,
			// "Save changes in configuration?", "Exit",
			// JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
			// null, null, JOptionPane.OK_OPTION) == JOptionPane.OK_OPTION)
			System.exit(0);
		} else if (programState == ProgramState.running) {
			if (JOptionPane.showOptionDialog(c,
					"Current progress will be lost! Are you sure?", "Exit",
					JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
					null, null, JOptionPane.OK_OPTION) == JOptionPane.OK_OPTION) {
				// stop Rserve
				gv.plotDemo.stopRserve();
				System.exit(0);
			}
		} else if (programState == ProgramState.finished) {
			// stop Rserve
			gv.plotDemo.stopRserve();
			Platform.exit();
			System.exit(0);
		}
	}

	// called by panelConfig, if start button pressed
	public void switchToLoadingScreen() {
		programState = ProgramState.running;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				frame.remove(currentPanel);
				panelLoading.repaintLoadingScreen();
				frame.add(panelLoading.getPanel());
				currentPanel = panelLoading.getPanel();
				frame.validate();
				frame.revalidate();
			}
		});

	}

	// called by panelLoading, if cancel button pressed
	public void switchToConfigScreen() {
		programState = ProgramState.config;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				frame.remove(currentPanel);
				panelConfig.repaintConfigScreen();
				frame.add(panelConfig.getPanel());
				currentPanel = panelConfig.getPanel();
				frame.validate();
				frame.revalidate();
			}
		});

	}

	// called by bashExecution thread, if execution finished
	public void switchToFinishScreen() {
		programState = ProgramState.finished;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// Point locOld = frame.getLocation();
				// int x = locOld.x;
				// int y = locOld.y;
				// Dimension dimold = frame.getSize();
				// frame.setLocation(x, y);
				frame.remove(currentPanel);
				panelFinished.repaintFinishedScreen();
				frame.add(panelFinished.getPanel());
				currentPanel = panelFinished.getPanel();
				SwingUtilities.updateComponentTreeUI(frame);

				// frame.setSize(600, 700);

				// frame.pack();
				// frame.setSize(dimold);
				// log.debug(locOld);

				// frame.setLocation(100, y);
				// frame.setLocation(x, 100);

				// log.debug(frame.getLocation());
			}
		});

	}

	public static void main(String[] args) throws InterruptedException {
		// File file = new File(
		// "/home/krebbel/tables/id_WT2_CD8_i11_TACGTA_final.csv");
		// log.debug(file.getAbsolutePath());
		// log.debug(file.getName());
		// log.debug(file.getPath());
		// log.debug(file.getParent());

		// init data
		// ScriptParser.parseHeader(
		// "dataFrameList, height = 5, width = 15, title=\"Coverage of
		// clonotypes\", intervals = c(0,0.01,0.1), intervals.label =
		// c(\"S\",\"M\",\"L\"), intervals.label.legend=c(\"S <0.01\",\"M
		// <0.1\",\"L >=0.1\"),");
		// ScriptParser.parseParameter("intervals.label.legend=c(\"S <0.01\",\"M
		// <0.1\",\"L >=0.1\")", null);
		// javax.swing.plaf.metal.MetalLookAndFeel
		// javax.swing.plaf.nimbus.NimbusLookAndFeel
		// com.sun.java.swing.plaf.motif.MotifLookAndFeel
		// com.sun.java.swing.plaf.gtk.GTKLookAndFeel

		// javax.swing.UIManager.LookAndFeelInfo[] bla = javax.swing.UIManager
		// .getInstalledLookAndFeels();
		// String look = UIManager.getSystemLookAndFeelClassName();
		// for (javax.swing.UIManager.LookAndFeelInfo info :
		// javax.swing.UIManager
		// .getInstalledLookAndFeels()) {
		// if ("javax.swing.plaf.nimbus.NimbusLookAndFeel".equals(info
		// .getClassName())) {
		// try {
		// javax.swing.UIManager.setLookAndFeel(info.getClassName());
		// } catch (ClassNotFoundException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// } catch (InstantiationException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// } catch (IllegalAccessException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// } catch (UnsupportedLookAndFeelException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// break;
		// }
		// }

		try {
			JavaFXInitializer.initialize();
		} catch (InterruptedException e) {
			// Failed to initialise JavaFX
			e.printStackTrace();
		}
		//
		// System.out.println(ScriptParser.parseFileName("id_WT2_CD4_i10_CGTGAT_final.csv"));
		// System.out.println(ScriptParser.parseFileName("KO1_CD4_i1_GTGTCA.xls"));
		//
		// System.out.println(ScriptParser
		// .parseParamLine("@param together integer(hallo 123 , 1) beide
		// zusammen (mode ={0,1}) in einem plot"));
		// ScriptParser
		// .parseScriptFull("/home/619501/git/plotgui/scripts/Moeller_Frac_Abun_Func.R");

		GlobalVars.singleton();
		new MainFrame();
		// SwingUtilities.invokeLater(new Runnable() {
		// @Override
		// public void run() {
		// new MainFrame();
		// }
		// });

		// new PlotDemo().testMeanFunction();

		// PlotDemo rplotter = new PlotDemo();
		// rplotter.plotTest();
		// PlotDemo plotDemo = GlobalVars.singleton().plotDemo;
		// plotDemo.plotTest();
		// plotDemo.initLibrarysAndFuncs();
		// plotDemo.parseAndEval(
		// "id_0 <-
		// data.frame(fread(\"C:/Users/Mo/Documents/R/tcr/Mio_0_75_i6_i6_ACTCAT_TCR.xls\",
		// header=T, stringsAsFactors=F))");
		// System.out.println(plotDemo.parseAndEvalRetStr("id_0[1,4]"));

		// RConnection c;
		// try {
		// c = new RConnection();
		// double d[] = c.eval("rnorm(10)").asDoubles();
		// System.out.println(c.eval("5+5").asInteger());
		// c.parseAndEval("plot(5)");
		// Thread.sleep(10000);
		// } catch (REXPMismatchException | REngineException
		// | InterruptedException e) {
		// e.printStackTrace();
		// }
	}
}
