package util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import app.GlobalVars;
import checkboxtree.CheckTreeManager;

public class TreeRegex {
	private List<List<TreePath>> lastToggeledPaths = new ArrayList<List<TreePath>>();

	public static enum RegexNodeSelectionMode {
		addCompleteTree, delCompleteTree, addParentNodeOnly, delParentNodeOnly
	}

	public String treeRegexNodeSelection(JTree tree, final CheckTreeManager treeCheckManager, String regex,
			RegexNodeSelectionMode selectionMode, GlobalVars gv) {
		if (regex == null || regex.equals(""))
			return "Regex is empty!";
		Pattern pattern = Pattern.compile(regex);
		// System.out.println("regex " + regex);

		TreePath selectedPath = tree.getSelectionPath();
		DefaultMutableTreeNode node;

		// Look at all nodes in the tree
		if (selectionMode == RegexNodeSelectionMode.addCompleteTree
				|| selectionMode == RegexNodeSelectionMode.delCompleteTree) {
			node = (DefaultMutableTreeNode) tree.getModel().getRoot();
		} else if (selectedPath != null) {
			// Look at parents only
			node = (DefaultMutableTreeNode) selectedPath.getLastPathComponent();
			if (node.isLeaf())
				node = (DefaultMutableTreeNode) node.getParent();
		} else {
			return "No tree node selected!";
		}
		List<TreePath> toggeledPaths = treeRegexNodeSelectionReK(node, treeCheckManager, pattern, selectionMode, gv);
		if (!toggeledPaths.isEmpty())
			lastToggeledPaths.add(toggeledPaths);

		return "";
	}

	public List<TreePath> treeRegexNodeSelectionReK(TreeNode node, CheckTreeManager treeCheckManager, Pattern pattern,
			RegexNodeSelectionMode selectionMode, GlobalVars gv) {
		List<TreePath> toggeledPaths = new ArrayList<TreePath>();
		for (int i = 0; i < node.getChildCount(); i++) {
			TreeNode child = node.getChildAt(i);
			// rec
			if (child.getChildCount() > 0) {
				toggeledPaths.addAll(treeRegexNodeSelectionReK(child, treeCheckManager, pattern, selectionMode, gv));
			} else if (child.isLeaf()) {
				TreePath childPath = TreeFuncs.getPath(child);
				Matcher matcher = pattern.matcher(child.toString());
				// add && matcher.group().equals(child.toString()) for strict
				// evaluation
				if (matcher.find()) {
					boolean childSelected = treeCheckManager.getSelectionModel().isPathSelected(childPath, true);

					if (selectionMode == RegexNodeSelectionMode.addCompleteTree
							|| selectionMode == RegexNodeSelectionMode.addParentNodeOnly) {
						if (!childSelected) {
							toggeledPaths.add(childPath);
							// System.out.println(childPath.toString());
							gv.addSampleToTableAndTree(childPath, treeCheckManager);
						}
					} else {
						if (childSelected) {
							toggeledPaths.add(childPath);
							gv.delSampleFromTableAndTree(childPath, treeCheckManager);
						}
					}
					// System.out.println("match find: " + matcher.group());
				}
			}
		}
		// TreePath scriptCheckedPaths[] = treeCheckManager.getSelectionModel()
		// .getSelectionPaths();
		// for (TreePath path : scriptCheckedPaths) {
		// System.out.println(path);
		// }
		return toggeledPaths;
	}

	public boolean restoreLastChanges(CheckTreeManager treeCheckManager, GlobalVars gv) {
		// nothing to restore
		if (lastToggeledPaths.isEmpty())
			return false;

		// restore nodes
		List<TreePath> lastChanged = lastToggeledPaths.get(lastToggeledPaths.size() - 1);
		for (TreePath path : lastChanged) {
			// checked => unchecked before
			if (treeCheckManager.getSelectionModel().isPathSelected(path, true)) {
				gv.delSampleFromTableAndTree(path, treeCheckManager);
			}
			// unchecked => checked before
			else {
				gv.addSampleToTableAndTree(path, treeCheckManager);
			}
		}
		// delete last changes
		lastToggeledPaths.remove(lastToggeledPaths.size() - 1);
		return true;
	}
}
