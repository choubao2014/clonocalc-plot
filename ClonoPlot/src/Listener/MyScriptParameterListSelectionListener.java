package Listener;

import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import app.PanelConfig;

public class MyScriptParameterListSelectionListener implements ListSelectionListener {

	private PanelConfig panelConfig = null;

	public MyScriptParameterListSelectionListener(PanelConfig panelConfig) {
		this.panelConfig = panelConfig;
	}

	@Override
	public void valueChanged(ListSelectionEvent arg0) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				panelConfig.changeScriptParameterInATable();
			}
		});
	}
}
