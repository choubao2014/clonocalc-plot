#!/usr/bin/env bash

# Script to install ClonoCalc and ClonoPlot of the ClonoSuite and its dependencies.

set -e

BASEDIR=$(dirname "$0")
cd $BASEDIR

INSTALLDIR=ClonoCalcPlot_install
if [ -d "$INSTALLDIR" ]; then
	echo "E: Found folder '$INSTALLDIR' in the current working directory. This may be a remnant from an earlier unsuccessful installation. Please delete it or move it away."
	exit
fi
echo "I: Creating new folder '$INSTALLDIR' for installation"
mkdir  $INSTALLDIR
cd $INSTALLDIR

for i in /usr/sbin/installer /usr/bin/curl /bin/cat
do
	if [ ! -x "$i" ]; then
		echo "E: Could not find system tool $i"
		exit
	fi
done

#------------------------------R install/update-----------------------------------------
installR(){
	echo "I: Initiated installation of R"
	RINSTALLER="R-3.3.1.pkg"
	curl --output $RINSTALLER "https://cran.r-project.org/bin/macosx/old/R-3.3.1.pkg" 
	echo "W: R installation needs root permissions, please enter your password:"
	sudo installer -verboseR -pkg $RINSTALLER -target /
	rm $RINSTALLER

	#check if install was successful
	if type -p Rscript; then
		echo "I: Installation of R completed."
	else
		echo "E: R installation failed, please try restart or manual installation."
		rm -- "$0"
		exit
	fi
}

echo "I:Checking for R"
if type -p Rscript; then
	echo 'strsplit(R.version$version.string," ")[[1]][3]' > RscriptTest.R
	RVersionString=$(Rscript RscriptTest.R)
	RVersionString=$([[ $RVersionString =~ [0-9]+(\.[0-9]+)+ ]] && echo ${BASH_REMATCH[0]})
		if [[ "$RVersionString" > "3.2.1" ]]; then
		echo "I: $RVersionString is suffient."
	else
		echo "I: $RVersionString is not suffient. Trying to update R"
		installR
	fi
	rm RscriptTest.R
else
	#R fresh install
	echo "I: R not found"
	installR
fi

#------------------------------R install/update packages-----------------------------------------
#echo "I: Installing missing packages for R as regular user"
ERRORLOG="R_error_log.txt"
(Rscript -e 'list <- c("Rserve","data.table","ggplot2","gplots","stringr","tcR","RCircos"); for(x in list) { if (!require(x,character.only = TRUE)){ install.packages(x, dependencies=T, repos="https://cran.uni-muenster.de"); if(!require(x,character.only = TRUE)){ stop(paste("Package", x, "installation failed!")); }}}' 2>&1 || true) | tee $ERRORLOG
#cat $ERRORLOG | grep 'Execution halted'
if grep -q 'Execution halted' "$ERRORLOG"; then
	cat "$ERRORLOG"
   	echo "E: R package installation failed (Error code $?)!"
	read -n 1 -s -p "Press any key to continue."
	exit
else
	echo "I: R package installtion successfully completed."
fi
rm $ERRORLOG

#------------------------------java install-----------------------------------------

installJava(){
	echo "I: Downloading portable java version to installation folder"
	JAVAFILE="jre-macosx.tar.gz"
	curl --output $JAVAFILE -v -j -k -L -H "Cookie: oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u111-b14/jre-8u111-macosx-x64.tar.gz"
	echo "I: Unpacking java"
	tar zxvf $JAVAFILE -C java
	echo "I: Removing downloaded file '$JAVAFILE'"
	rm $JAVAFILE
}


a=$(which java)

echo "I: Creating 'java' subdirectory"
mkdir -p java

if [ -z "$a" ]; then
	echo "a=$a"
	echo "W: Could not find 'java' application in path."
	installJava
elif [ $(java -version 2>&1| grep version | cut -f2 -d'"' |  cut -f1,2 -d"." | tr -d ".") -ge 18 ]; then
	echo "I: Java version is >= 1.8 - no download of Java required"
else
	echo "W: Java version is < 1.8"
	java -version
	installJava

	#get java path
	cd java
	cd `ls -m1`
	JAVAFOLDERNAME=$(basename "$PWD")
	cd ..
	cd ..
fi

#------------------------------Download/install ClonoCalc/Plot-----------------------------------------
#https://mathiasbynens.be/notes/shell-script-mac-apps
#example call:
#appify your-shell-script.sh "Your App Name"
appify(){
	APPNAME=${2:-$(basename "${1}" '.sh')};
	CONTENTS="${APPNAME}.app/Contents";
	DIR="$CONTENTS/MacOS";
	RESDIR="$CONTENTS/Resources";
	INFOFILE="$CONTENTS/Info.plist";
	ICONPATH=$3 #/Users/test/Downloads/ClonoPlot_Icon.icns
	ICONNAME=$(basename $ICONPATH ".icns") #ClonoPlot_Icon.icns
	ICONNAME="${ICONNAME%.*}" #ClonoPlot_Icon

	if [ -a "${APPNAME}.app" ]; then
		echo "${PWD}/${APPNAME}.app already exists :(";
	else
		echo "${1} to ${DIR}/${APPNAME}"
		mkdir -p "${DIR}";
		mv "${1}" "${DIR}/${APPNAME}";
		chmod +x "${DIR}/${APPNAME}";

		#move icon
		mkdir -p "${RESDIR}";
		mv $ICONPATH $RESDIR
		#create xml for icon
		echo '<?xml version="1.0" encoding="UTF-8"?>' >> $INFOFILE
		echo '<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">' >> $INFOFILE
		echo '<plist version="1.0">' >> $INFOFILE
		echo "<dict>" >> $INFOFILE
		echo "	<key>CFBundleIconFile</key>" >> $INFOFILE
		echo "	<string>$ICONNAME</string>" >> $INFOFILE
		echo "</dict>" >> $INFOFILE
		echo "</plist>" >> $INFOFILE
	fi;
}

#createJavaStarter "`pwd`/$CLONOPLOTDIR" "ClonoPlot.jar" ClonoPlot
createJavaStarter(){
	PATHTOJAR=$1 #/Users/testuser/test
	JARNAME=$2 #ClonoPlot.jar
	STARTERNAME=$3 #ClonoPlot
	STARTERSCRIPT="$STARTERNAME.sh" #ClonoPlot.sh

	echo "#!/usr/bin/env bash" >> $STARTERSCRIPT

	#load default path
	echo "if [ -x /usr/libexec/path_helper ]; then" >> $STARTERSCRIPT
		echo "	eval `/usr/libexec/path_helper -s`" >> $STARTERSCRIPT
	echo "fi" >> $STARTERSCRIPT

	echo 'BASEDIR=$(dirname "$0")' >> $STARTERSCRIPT
	echo "cd \$BASEDIR" >> $STARTERSCRIPT
	echo "cd .." >> $STARTERSCRIPT

	if [ -n "$JAVAFOLDERNAME" ]; then
		echo "export PATH=\$(pwd)/java/$JAVAFOLDERNAME/Contents/Home/bin:\$PATH" >> $STARTERSCRIPT
	fi

	echo "cd $STARTERNAME" >> $STARTERSCRIPT
	echo "java -jar $JARNAME" >> $STARTERSCRIPT
	appify $STARTERSCRIPT $STARTERNAME $4
}

downloadUnpackCreate(){
	NAME=$1 #ClonoPlot
	APPDIR=$NAME #ClonoPlot
	FILENAME="$NAME.zip" #ClonoPlot.zip
	DOWNLOADLINK=$2

	echo "I: Downloading and installing '$NAME'"
	if ! curl --output $FILENAME $DOWNLOADLINK; then
		echo "E: Failed to download '$DOWNLOADLINK' to '$FILENAME' in $(PWD)'"
		exit
	fi

	echo "I: Unzip of '$FILENAME' to '$APPDIR'"
	unzip $FILENAME -d $APPDIR
	rm $FILENAME
}

CLONOSUITELINKPREFIX="https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/"
CLONOCALCLINKPREFIX="$CLONOSUITELINKPREFIX/ClonoCalc"
CLONOPLOTLINKPREFIX="$CLONOSUITELINKPREFIX/ClonoPlot"

echo "I: Downloading and unpacking ClonoPlot and ClonoCalc from Bitbucket"
downloadUnpackCreate ClonoCalc "$CLONOCALCLINKPREFIX/ClonoCalc.zip"
downloadUnpackCreate ClonoPlot "$CLONOPLOTLINKPREFIX/ClonoPlot.zip"

echo "I: Downloading icons from Bitbucket"
ICONPATHCLONOCALC="$(pwd)/ClonoCalc_Icon.icns"
ICONPATHCLONOPLOT="$(pwd)/ClonoPlot_Icon.icns"
curl --output $ICONPATHCLONOCALC "$CLONOCALCLINKPREFIX/Icons/ClonoCalc_Icon.icns"
curl --output $ICONPATHCLONOPLOT "$CLONOPLOTLINKPREFIX/Icons/ClonoPlot_Icon.icns"

echo "I: Downloading tutoriales from Bitbucket"
#curl --output ClonoCalc_Tutorial.pdf "$CLONOCALCLINKPREFIX/ClonoCalc_Tutorial.pdf" 
#curl --output ClonoPlot_Tutorial.pdf "$CLONOPLOTLINKPREFIX/ClonoPlot_Tutorial.pdf"
curl --output ClonoSuite-Tutorial.pdf "$CLONOSUITELINKPREFIX/ClonoSuite-Tutorial.pdf"

echo "I: Downloading samples for ClonoCalc from Bitbucket"
SAMPLESCLONOCALC=$(pwd)/samples_ClonoCalc
FORWARDPACKED=sample_R1_50k.fastq.gz
REVERSEPACKED=sample_R2_50k.fastq.gz
FORWARDUNPACKED=sample_R1_50k.fastq
REVERSEUNPACKED=sample_R2_50k.fastq

mkdir -p "$SAMPLESCLONOCALC"
cd "$SAMPLESCLONOCALC"
A="$CLONOCALCLINKPREFIX/testData/$FORWARDPACKED"
echo "I: Retrieving 1st sample data set for ClonoCalc from '$A'"
curl --output $FORWARDPACKED $A
gunzip $FORWARDPACKED #gunzip will autodelete the packed file
A="$CLONOCALCLINKPREFIX/testData/$REVERSEPACKED"
echo "I: Retrieving 2st sample data set for ClonoCalc from '$A'"
curl --output $REVERSEPACKED $A 
gunzip $REVERSEPACKED
cd ..

#add samples to ClonoCalc config
echo "OutputPath = $SAMPLESCLONOCALC" >> ClonoCalc/config.txt
echo "ForwardReadFile = $SAMPLESCLONOCALC/$FORWARDUNPACKED" >> ClonoCalc/config.txt
echo "ReverseReadFile = $SAMPLESCLONOCALC/$REVERSEUNPACKED" >> ClonoCalc/config.txt

echo "I: Downloading samples for ClonoPlot from Bitbucket"
SAMPLESCLONOPLOT=$(pwd)/samples_ClonoPlot
mkdir $SAMPLESCLONOPLOT
cd $SAMPLESCLONOPLOT
echo "I: Retrieving '$CLONOPLOTLINKPREFIX/testData.zip'"
curl --output testData.zip "$CLONOPLOTLINKPREFIX/testData.zip"
unzip testData.zip
rm testData.zip
cd ..

cd ClonoPlot/tmp
echo "$SAMPLESCLONOPLOT/samplesA" >> selectedTableFolders.txt
echo "$SAMPLESCLONOPLOT/samplesB" >> selectedTableFolders.txt

echo ",0,1" > serScriptJTreeExpansion.txt
echo ",0,1,4" > serTableJTreeExpansion.txt
echo "1011 597 110 214" > selectedWindowSize.txt
cd ../..

echo $(pwd)

echo "I: Create Launchers"
createJavaStarter "`pwd`/ClonoPlot" "ClonoPlot.jar" ClonoPlot $ICONPATHCLONOPLOT
createJavaStarter "`pwd`/ClonoCalc" "ClonoCalc.jar" ClonoCalc $ICONPATHCLONOCALC

echo "I: Move ClonoPlot and ClonoCalc to Launcher folders"
mv ClonoPlot "ClonoPlot.app/Contents"
mv ClonoCalc "ClonoCalc.app/Contents"

echo "I: Copy java folder to launchers to ensure free movement"
cp -R java ClonoCalc.app/Contents
mv java ClonoPlot.app/Contents

echo "I: Installation finished successfully."
