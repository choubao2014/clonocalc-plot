package app;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

public class PanelLoading implements ActionListener {
	MainFrame mainFrame = null;
	JFrame outerFrame = null;

	JPanel panelLoading = new JPanel();

	JTextPane txtpDebug = GlobalVars.singleton().txtpDebug;
	JScrollPane spTxtpDebug = GlobalVars.singleton().spTxtpDebug;

	// JTextArea taDebugPrints = new JTextArea();
	// JScrollPane spTextArea = new JScrollPane(taDebugPrints);

	JButton btnCancel = new JButton("Cancel");

	public JPanel getPanel() {
		return panelLoading;
	}

	public PanelLoading(MainFrame mainFrame, JFrame outerFrame) {
		this.mainFrame = mainFrame;
		this.outerFrame = outerFrame;
		GlobalVars.singleton().PipelineFinshedListener = this;
		// GlobalVars.singleton().txtpDebug = txtpDebug;

		initComponents();

		// buildLoadingScreen();
	}

	private void initComponents() {
		// taDebugPrints.setEditable(false);
		txtpDebug.setEditable(false);

		btnCancel.addActionListener(this);
	}

	private void buildLoadingScreen() {
		// border layout to use the full window size
		panelLoading.setLayout(new BorderLayout());

		// panelLoading.add(spTextArea, BorderLayout.CENTER);

		panelLoading.add(spTxtpDebug, BorderLayout.CENTER);

		panelLoading.add(btnCancel, BorderLayout.SOUTH);
	}

	public void repaintLoadingScreen() {
		// first remove all components from the panel
		panelLoading.removeAll();
		// add the components to the panel
		buildLoadingScreen();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// called if bash execution thread is finished
		if (arg0 == null) {
			mainFrame.switchToFinishScreen();
		} else if (arg0.getSource() == this.btnCancel) {
			// ask user if he/she wants to cancel the running program
			int retValue = JOptionPane.showOptionDialog(outerFrame, "Do you really want to abort the execution?",
					"Abort execution", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, null,
					new String[] { "Yes", "No" }, "No");
			if (retValue == JOptionPane.YES_OPTION) {
				// stop execution
				// System.out.println("call stop");
				GlobalVars.singleton().bashExecution.stopScript();
				// clear textPane
				// txtpDebug.setText("");
				// go back to config screen
				mainFrame.switchToConfigScreen();
			}
		}

	}
}
