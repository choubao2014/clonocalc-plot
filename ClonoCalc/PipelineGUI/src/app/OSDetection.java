package app;

public class OSDetection {
	private static String strOS = System.getProperty("os.name").toLowerCase();

	public enum OperatingSys {
		Linux, Mac, Windows, Unsupported
	};

	public static OperatingSys getOperatingSys() {
		if (isWindows()) {
			return OperatingSys.Windows;
		} else if (isMac()) {
			return OperatingSys.Mac;
		} else if (isUnix()) {
			return OperatingSys.Linux;
		}
		return OperatingSys.Unsupported;
	}

	public static boolean isWindows() {
		return (strOS.indexOf("win") >= 0);
	}

	public static boolean isMac() {
		return (strOS.indexOf("mac") >= 0);
	}

	public static boolean isUnix() {
		return (strOS.indexOf("nix") >= 0 || strOS.indexOf("nux") >= 0 || strOS.indexOf("aix") > 0);
	}

	public static boolean isSolaris() {
		return (strOS.indexOf("sunos") >= 0);
	}

}
