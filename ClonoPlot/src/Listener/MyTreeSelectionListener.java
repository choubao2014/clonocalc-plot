package Listener;

import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import app.PanelConfig;

public class MyTreeSelectionListener implements TreeSelectionListener {
	// private static final Logger log =
	// Logger.getLogger(MyTreeSelectionListener.class);

	private PanelConfig panelConfig = null;

	public MyTreeSelectionListener(PanelConfig panelConfig) {
		this.panelConfig = panelConfig;
	}

	@Override
	public void valueChanged(final TreeSelectionEvent e) {
		// DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.getPath()
		// .getLastPathComponent();
		// log.debug("You selected " + e.getPath());

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				panelConfig.changeScriptParameterTable(e.getPath());
			}
		});
	}

}
