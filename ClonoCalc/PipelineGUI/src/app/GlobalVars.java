package app;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import Listener.BarcodeListener;
import Listener.IdListener;
import Listener.NameListener;
import Listener.ReadsListener;
import app.OSDetection.OperatingSys;

//Singleton Pattern
public class GlobalVars {
	public final static OperatingSys os = OSDetection.getOperatingSys();
	public boolean isLinuxOS;

	private static final int lightFactorGreen = 210, lightFactorRed = 150;
	public static final Color colorInvalid = new Color(255, lightFactorRed,
			lightFactorRed);
	// public static final Color colorInvalid = Color.RED;
	public static final Color colorValid = new Color(lightFactorGreen, 255,
			lightFactorGreen);
	// public static final Color colorValid = Color.GREEN;
	public static final Color colorDefault = Color.WHITE;
	public static final Color colorWarning = Color.ORANGE;

	private static GlobalVars singleton = null;

	public static final boolean internalVersion = false;
	public static final boolean internalVersionUseMixcr = false;

	public final String userDir = (new File(
			System.getProperty("java.class.path"))).getAbsoluteFile()
			.getParentFile().toString().contains(":") ? (new File(
			System.getProperty("java.class.path"))).getAbsoluteFile()
			.getParentFile().toString().split(":")[1] : (new File(
			System.getProperty("java.class.path"))).getAbsoluteFile()
			.getParentFile().toString();

	public final String cbAutoCompleteText = "Autocompletion";
	public final String cbUseOwnDemultiplexerText = "Own Demultiplexer";
	public final String errorCheckingText = "Barcode error cheking";
	private boolean autoComplete = true;
	private boolean showAdvancedOptions = false;
	private boolean useOwnDemultiplexer = false;
	public boolean errorChecking = true;

	// define Hashmap for config Variables
	Map<String, JTextField> hmConfVars = new HashMap<String, JTextField>();

	public final String defaultTxtForwardReadFile = "Choose a .fastq or .fastq.gz file";
	public JTextField txtForwardReadFile = new JTextField(
			defaultTxtForwardReadFile, 40);
	public final String defaultTxtReverseReadFile = "Choose a .fastq.gz file";
	public JTextField txtReverseReadFile = new JTextField(
			defaultTxtReverseReadFile, 40);
	public final String defaultTxtOutputFolder = "Choose a .fastq.gz file";
	public JTextField txtOutputFolder = new JTextField(
			defaultTxtReverseReadFile, 40);

	private final String defaultParaBarcodeSplitter = "--bol --mismatches 0 \\--prefix ./ --suffix .fastq";
	private final String defaultParaMitcr = "-species mm -gene TRB -pset flex -cysphe 0 -level 2";
	private final String defaultParaMitcrTcrPackage = "-species mm -gene TRB -pset flex -cysphe 0 -level 2";
	private final String defaultParaPear = "";
	private final String defaultParaMixcrAlign = "--loci TRB -s mmu -f";
	private final String defaultParaMixcrAssemble = "";
	private final String defaultParaMixcrExportClones = "-count -fraction -nFeature CDR3 -aaFeature CDR3 -vHit -jHit -dHit";

	public JTextField txtParaBarcodeSplitter = new JTextField(
			defaultParaBarcodeSplitter, 40);
	public JTextField txtParaMitcr = new JTextField(defaultParaMitcr, 40);
	public JTextField txtParaMitcrTcrPackage = new JTextField(
			defaultParaMitcrTcrPackage, 40);
	public JTextField txtParaPear = new JTextField(defaultParaPear, 40);
	public JTextField txtParaMixcrAlign = new JTextField(defaultParaMixcrAlign,
			40);
	public JTextField txtParaMixcrAssemble = new JTextField(
			defaultParaMixcrAssemble, 40);
	public JTextField txtParaMixcrExportClones = new JTextField(
			defaultParaMixcrExportClones, 40);

	private final String defaultBarcodeFile = "barcodes.txt";
	public JTextField txtBarcodeFile = new JTextField("barcodes.txt");

	private List<JLabel> listLblSamples = new ArrayList<JLabel>();
	private List<JTextField> listTxtSamplesName = new ArrayList<JTextField>();
	private List<JTextField> listTxtSamplesId = new ArrayList<JTextField>();
	private List<JTextField> listTxtSamplesBarcode = new ArrayList<JTextField>();
	private final int nrOfSamplesDefault = 6;
	private int nrOfSamples = 0;

	// sample stuff
	private final int columnSizeName = 20;
	private final int columnSizeId = 2;
	private final int columnSizeBarcode = 5;

	// barcode splitter parameter options
	public JTextField txtBarcodeStartPos = new JTextField(3);
	public JTextField txtMaxHammingDist = new JTextField(3);

	// define the global text field listener
	private ReadsListener listenerReads = new ReadsListener();
	private NameListener listenerName = new NameListener();
	private IdListener listenerId = new IdListener();
	private BarcodeListener listenerBarcode = new BarcodeListener();

	public JTextPane txtpDebug = new JTextPane();
	public JScrollPane spTxtpDebug = new JScrollPane(txtpDebug);

	// public JTextArea textArea = new JTextArea();

	public BashExecution bashExecution = new BashExecution();
	// set by panel loading constructor
	public ActionListener PipelineFinshedListener;

	// Config variables defined in config.txt
	public final String confFileName = "config.txt";
	public final String confBackupFileName = "config_backup.txt";
	public final String confVOutputPath = "OutputPath";
	public final String confVForwardReadFile = "ForwardReadFile";

	public final String confVReverseReadFile = "ReverseReadFile";
	public final String confVPARAbarcodesplitter = "PARAbarcodesplitter";
	public final String confVPARAmitcr = "PARAmitcr";
	public final String confVPARAmitcrTCRPACKAGE = "PARAmitcrTCRPACKAGE";
	public final String confVPARApear = "PARApear";
	public final String confVBarcodeFile = "BarcodeFile";
	public final String confVPARAmixcrAlign = "PARAmixcrAlign";
	public final String confVPARAmixcrAssemble = "PARAmixcrAssemble";
	public final String confVPARAmixcrExportClones = "PARAmixcrExportClones";

	private GlobalVars() {	
		// find out os
		isLinuxOS = isLinuxOS();
		// init HashMap
		hmConfVars.put(confVOutputPath, txtOutputFolder);
		hmConfVars.put(confVForwardReadFile, txtForwardReadFile);
		hmConfVars.put(confVReverseReadFile, txtReverseReadFile);
		hmConfVars.put(confVPARAbarcodesplitter, txtParaBarcodeSplitter);
		hmConfVars.put(confVPARAmitcr, txtParaMitcr);
		hmConfVars.put(confVPARAmitcrTCRPACKAGE, txtParaMitcrTcrPackage);
		hmConfVars.put(confVPARApear, txtParaPear);
		hmConfVars.put(confVBarcodeFile, txtBarcodeFile);

		txtOutputFolder.getDocument().putProperty("owner", txtOutputFolder);
		txtOutputFolder.getDocument().addDocumentListener(listenerReads);

		txtForwardReadFile.getDocument().putProperty("owner",
				txtForwardReadFile);
		txtForwardReadFile.getDocument().addDocumentListener(listenerReads);

		txtReverseReadFile.getDocument().putProperty("owner",
				txtReverseReadFile);
		txtReverseReadFile.getDocument().addDocumentListener(listenerReads);

		spTxtpDebug.setAutoscrolls(true);

		txtBarcodeStartPos.setText("3");
		txtMaxHammingDist.setText("0");
	}
	
	
	public static GlobalVars singleton() {
		if (singleton == null) {
			singleton = new GlobalVars();
			
			
			
			// init config
			singleton.parseConfigFile();
			// fill rows up to nrOfSamplesDefault
			for (int i = singleton.nrOfSamples; i < singleton.nrOfSamplesDefault; i++) {
				singleton.addSampleRow();
			}
		}		
		//System.out.println(singleton.txtForwardReadFile.getText());
		return singleton;
	}

	public boolean getAutoComplete() {
		return autoComplete;
	}

	public void setAutoComplete(boolean autoComplete) {
		this.autoComplete = autoComplete;
	}

	public boolean getShowAdvancedOptions() {
		return showAdvancedOptions;
	}

	public boolean getUseOwnMultiplexer() {
		return useOwnDemultiplexer;
	}

	public void setUseOwnMultiplexer(boolean value) {
		useOwnDemultiplexer = value;
	}

	public void setShowAdvancedOptions(boolean showAdvancedOptions) {
		this.showAdvancedOptions = showAdvancedOptions;
	}

	public void addSampleRow() {
		pirvateAddSampleRow("", "", "");
	}

	public void addSampleRow(String name, String id, String barcode) {
		// if (id.substring(0, 1).equals("i"))
		// id = id.substring(1);
		pirvateAddSampleRow(name, id, barcode);
	}

	private void pirvateAddSampleRow(String name, String id, String barcode) {
		if (nrOfSamples >= 20)
			return;

		nrOfSamples++;

		JLabel lblSample = new JLabel("Sample " + (nrOfSamples));
		JTextField txtSampleName = new JTextField(columnSizeName);
		JTextField txtSampleId = new JTextField(columnSizeId);
		JTextField txtSampleBarcode = new JTextField(columnSizeBarcode);

		// set the owner, to get it if an DocumentEvent triggered
		txtSampleName.getDocument().putProperty("owner", txtSampleName);
		txtSampleId.getDocument().putProperty("owner", txtSampleId);
		txtSampleId.getDocument().putProperty("type", "id");
		txtSampleBarcode.getDocument().putProperty("owner", txtSampleBarcode);
		txtSampleBarcode.getDocument().putProperty("type", "barcode");

		// set fields as neighbors, for later auto completion
		txtSampleId.getDocument().putProperty("neighbor", txtSampleBarcode);
		txtSampleBarcode.getDocument().putProperty("neighbor", txtSampleId);

		txtSampleName.getDocument().addDocumentListener(listenerName);
		txtSampleId.getDocument().addDocumentListener(listenerId);
		txtSampleBarcode.getDocument().addDocumentListener(listenerBarcode);

		// add to list before validation check
		listLblSamples.add(lblSample);
		listTxtSamplesName.add(txtSampleName);
		listTxtSamplesId.add(txtSampleId);
		listTxtSamplesBarcode.add(txtSampleBarcode);

		// listenerName.insertUpdate(txtSampleName.create);
		// do this here, to activate Listener
		txtSampleName.setText(name);
		txtSampleId.setText(id);
		txtSampleBarcode.setText(barcode);
	}

	public void removeLastSampleRow() {
		if (nrOfSamples <= 0)
			return;

		JLabel lblSample = listLblSamples.get(listLblSamples.size() - 1);
		JTextField txtSampleName = listTxtSamplesName.get(listTxtSamplesName
				.size() - 1);
		JTextField txtSampleId = listTxtSamplesId
				.get(listTxtSamplesId.size() - 1);
		JTextField txtSampleBarcode = listTxtSamplesBarcode
				.get(listTxtSamplesBarcode.size() - 1);

		listLblSamples.remove(lblSample);
		listTxtSamplesName.remove(txtSampleName);
		listTxtSamplesId.remove(txtSampleId);
		listTxtSamplesBarcode.remove(txtSampleBarcode);

		nrOfSamples--;
	}

	public int getNumberOfSamples() {
		return nrOfSamples;
	}

	public JLabel getLblSamples(int i) {
		if (i >= 0 && i < nrOfSamples)
			return listLblSamples.get(i);
		return null;
	}

	public JTextField getTxtSamplesName(int i) {
		if (i >= 0 && i < nrOfSamples)
			return listTxtSamplesName.get(i);
		return null;
	}

	public JTextField getTxtSamplesId(int i) {
		if (i >= 0 && i < nrOfSamples)
			return listTxtSamplesId.get(i);
		return null;
	}

	public JTextField getTxtSamplesBarcode(int i) {
		if (i >= 0 && i < nrOfSamples)
			return listTxtSamplesBarcode.get(i);
		return null;
	}

	public void detectDupsInTxtSampleNames() {
		HelpMethods.detectDuplicateInJTextFieldList(listTxtSamplesName);
	}

	public void detectDupsInTxtSampleId() {
		HelpMethods.detectDuplicateInJTextFieldList(listTxtSamplesId);
	}

	public void detectDupsInTxtSampleBarcode() {
		HelpMethods.detectDuplicateInJTextFieldList(listTxtSamplesBarcode);
	}
		


	public void restoreDefaults() {
		for (int i = nrOfSamples; i > 0; i--) {
			removeLastSampleRow();
		}
		for (JTextField txt : hmConfVars.values()) {
			txt.setText("");
		}
		txtParaBarcodeSplitter.setText(defaultParaBarcodeSplitter);
		txtParaMitcr.setText(defaultParaMitcr);
		txtParaMitcrTcrPackage.setText(defaultParaMitcrTcrPackage);
		txtParaPear.setText(defaultParaPear);
		txtBarcodeFile.setText(defaultBarcodeFile);

		for (int i = nrOfSamples; i < nrOfSamplesDefault; i++) {
			addSampleRow();
		}
	}

	private void parseConfigFile() {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(userDir + File.separator
					+ confFileName));
			String line;
			while ((line = br.readLine()) != null) {
				// use / for windows compatibility 
				line = line.replace("\\", "/");
				// check for empty or comment lines
				if (line.equals("") || line.substring(0, 1).equals("#"))
					continue;
				// conf variable
				else if (line.contains("=")) {
					// split line by '='
					String[] vars = line.split("=");
					if (vars.length != 2)
						continue;
					// delete all whitespace chars
					String key = vars[0].replaceAll("\\s+", "");

					String value = vars[1];
					if (!value.contains("\"")) {
						value = vars[1].replaceAll("\\s+", "");
					} else {
						int start = value.indexOf("\"") + 1;
						int end = value.lastIndexOf("\"");
						value = value.substring(start, end);
					}
					

					JTextField txtValue = hmConfVars.get(key);
					if (txtValue != null)
						txtValue.setText(value);
					// else
					// System.out.println(key + " = " + value + " not found!");
				}
				// sample description
				else if (line.contains("&")) {
					String[] vars = line.replaceAll("\\s+", "").split("&");
					if (vars.length != 3)
						continue;
					addSampleRow(vars[0], vars[1], vars[2]);
				}				
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	public boolean isLinuxOS() {
		
		// OS-depended
		Boolean isLinux;
		String[] command;
		if (os.toString().contains("Windows")){
			command = new String[]{"CMD", "/C", "dir"};
			isLinux=false;			
		}else{
			command = new String[]{"uname"};			
			isLinux=true;
		}
		
		ProcessBuilder builder = new ProcessBuilder( command );
		builder.redirectErrorStream(true);
		try {
			Process proc = builder.start();
			proc.waitFor();
			InputStream stdout = proc.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					stdout));

			String line = reader.readLine();
			if (line != null && line.contains("Darwin"))
				return isLinux;
			else
				return isLinux;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isLinux;
	}

	public int getBarcodeStartPos() {
		return Integer.parseInt(txtBarcodeStartPos.getText());
	}

	public int getMaxHammingDist() {
		return Integer.parseInt(txtMaxHammingDist.getText());
	}
}
