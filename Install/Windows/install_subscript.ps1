
Set-PSDebug -Strict
set-strictMode -version latest

function Update-Environment {

#####################################################################
##
## (C) 2015 Michael Miklis (michaelmiklis.de)
##
##
## Filename:      Update-Environment.ps1
##
## Version:       1.0
##
## Release:       Final
##
## Requirements:  -none-
##
## Description:   Refresh the current environment variables
##
## This script is provided 'AS-IS'.  The author does not provide
## any guarantee or warranty, stated or implied.  Use at your own
## risk. You are free to reproduce, copy & modify the code, but
## please give the author credit.
##
####################################################################

    <#
    .SYNOPSIS
    Refresh all environment variables

    .DESCRIPTION
    Reads all environement variables from registry for scope user and machine
    and updates the current process environment variables

    .EXAMPLE
    Update-Environment
    #>


    # first update the environment variables from the machine scope
    $ENV_MACHINE = ([Environment]::GetEnvironmentVariables('Machine'))
    foreach ($EnvVar in $ENV_MACHINE.keys) {
        Set-Item "Env:$($EnvVar)" -Value $ENV_MACHINE[$EnvVar]
    }

    # second update the environment variables from the user scope
    $ENV_USER = ([Environment]::GetEnvironmentVariables('User'))
    foreach ($EnvVar in $ENV_USER.keys) {
        Set-Item "Env:$($EnvVar)" -Value $ENV_USER[$EnvVar]
    }

    # now Update the Path variable (path variable gets
    # combined by User:Path and Machine:Path
    # User:Path has precedence over Machine:Path
    if ($ENV_USER.ContainsKey('Path')) {
        Set-Item env:Path -Value ((($ENV_USER.Path -split ";") + ($ENV_MACHINE.Path -split ";") | select -Unique) -join ";")
    }

}

#download file with progress bar
#example
#downloadFile "http://somesite/largefile.zip" "c:\temp\largefile.zip"
#https://blogs.msdn.microsoft.com/jasonn/2008/06/13/downloading-files-from-the-internet-in-powershell-with-progress/
function downloadFile($url, $targetFile)
{
	"Downloading $url to $targetFile"
    $uri = New-Object "System.Uri" "$url"
    $request = [System.Net.HttpWebRequest]::Create($uri)
    $request.set_Timeout(15000) #15 second timeout
    $response = $request.GetResponse()
    $totalLength = [System.Math]::Floor($response.get_ContentLength()/1024)
    $responseStream = $response.GetResponseStream()
    $targetStream = New-Object -TypeName System.IO.FileStream -ArgumentList $targetFile, Create
    $buffer = new-object byte[] 10KB
    $count = $responseStream.Read($buffer,0,$buffer.length)
    $downloadedBytes = $count
    while ($count -gt 0)
    {
        [System.Console]::CursorLeft = 0
        [System.Console]::Write("Downloaded {0}K of {1}K", [System.Math]::Floor($downloadedBytes/1024), $totalLength)
        $targetStream.Write($buffer, 0, $count)
        $count = $responseStream.Read($buffer,0,$buffer.length)
        $downloadedBytes = $downloadedBytes + $count
    }
    "`nFinished Download"
    $targetStream.Flush()
    $targetStream.Close()
    $targetStream.Dispose()
    $responseStream.Dispose()
}

#open folder browser dialog, return selected folder path
#example
#$installDirClonoPlot = Get-Folder
#https://stackoverflow.com/questions/25690038/how-do-i-properly-use-the-folderbrowserdialog-in-powershell
function Get-Folder()
{
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms")

    $foldername = New-Object System.Windows.Forms.FolderBrowserDialog
	$foldername.Description = "Choose install folder for ClonoCalc"

	$folder = [System.Environment]::GetFolderPath([System.Environment+SpecialFolder]::Desktop)
    if($foldername.ShowDialog() -eq "OK")
    {
        $folder = $foldername.SelectedPath
    }
	Else
	{
		Write-Error "Operation cancelled by user. Use Desktop as default folder."
	}
    return $folder
}

#unzip file to specific path
#example
#Unzip Expand-ZIPFile –File "C:\howtogeeksite.zip" –Destination "C:\temp\howtogeek"
#http://www.howtogeek.com/tips/how-to-extract-zip-files-using-powershell/
function Expand-ZIPFile($file, $destination)
{
    $shell = new-object -com shell.application
    $zip = $shell.NameSpace($file)
    foreach($item in $zip.items())
    {
        $shell.Namespace($destination).copyhere($item)
    }
}

#check if R version >=3.2.0 is available
function checkIfCurrentRVersionIsInstalled()
{
    try{
        "strsplit(R.version`$version.string,`" `")[[1]][3]" | out-file $downloadDir\install.R -encoding ASCII
        $installedRVersion = & Rscript $downloadDir\install.R
        $installedRVersion = ($installedRVersion -split " ")[1].Replace("`"","")
        [System.Version]$installedRVersion -gt [System.Version]"3.2.0"
    }catch{
        $false
    }
}

#define download dir for R installer
$downloadDir = $env:temp

#install/update java
#http://www.powershelladmin.com/wiki/PowerShell_Java_Auto-Update_Script
cd .\Java-Update
#"Bypass" the PowerShell Execution Policy
#see details: https://blog.netspi.com/15-ways-to-bypass-the-powershell-execution-policy/
PowerShell -ExecutionPolicy Bypass -File .\Update-Java.ps1
If (Test-Path "$downloadDir\JavaUpdateTemp.exe"){
	Remove-Item "$downloadDir\JavaUpdateTemp.exe"
}
cd ..

#set install dir for clonoCalc to script path
$executionDir = (Get-Item -Path ".\" -Verbose).FullName
$installDirClonoPlot = "$executionDir\ClonoPlot"
$installDirClonoCalc = "$executionDir\ClonoCalc"
New-Item -ItemType Directory -Path $installDirClonoPlot
New-Item -ItemType Directory -Path $installDirClonoCalc
"Use $installDirClonoPlot as install folder for ClonoPlot"

#define R version to install
$rVersion = "3.3.1"

#check if R is already installed
if(-Not(checkIfCurrentRVersionIsInstalled))
{
    #downlaod R, if you want to use the newest version delete the 'old/' in the link
    downloadFile "http://ftp5.gwdg.de/pub/misc/cran/bin/windows/base/old/$rVersion/R-$rVersion-win.exe" "$downloadDir\R-$rVersion-win.exe"

    #install R silent, block until installation finished
    Start-Process -FilePath "$downloadDir\R-$rVersion-win.exe" -ArgumentList '/SILENT' -wait

    #add R to User-PATH-Var, assume R is installed to standard folder %ProgramFiles%
    $ProgramFilesPath = [System.Environment]::GetFolderPath([System.Environment+SpecialFolder]::ProgramFiles)
    $RPath = "$ProgramFilesPath\R\R-$rVersion\bin"
    $UserPathContent = [environment]::GetEnvironmentVariable("Path", "User")
    [Environment]::SetEnvironmentVariable("Path", $UserPathContent + ";" + $RPath, "User")

    #set default R library path to user home dir, the usual restrictions do not allow R to write to the install dir
    $RLibraryPath = "$env:USERPROFILE\R\R-$rVersion\library"
    #first create dir, otherwise R do not accept the library path
    New-Item -ItemType Directory -Path $RLibraryPath
    [Environment]::SetEnvironmentVariable("R_LIBS_USER", $RLibraryPath, "User")

    #call Update-Environment() function
    Update-Environment

    Remove-Item "$downloadDir\R-$rVersion-win.exe"
}

#install R packages to the new library path
"list <- c(`"Rserve`",`"data.table`",`"ggplot2`",`"gplots`",`"stringr`",`"tcR`"); for(x in list) { if (!require(x,character.only = TRUE)){ install.packages(x, dependencies=T, repos=`"https://cran.uni-muenster.de`"); if(!require(x,character.only = TRUE)){ stop(paste(`"Package`", x, `"installation failed!`")); }}}"  | out-file $downloadDir\install.R -encoding ASCII
#"install.packages(c(`"Rserve`",`"data.table`",`"ggplot2`",`"gplots`",`"stringr`",`"tcR`"), dependencies=T, repos=`"https://cran.uni-muenster.de`")" | out-file $downloadDir\install.R -encoding ASCII
#& ($RPath + "\Rscript") $downloadDir\install.R
& Rscript $downloadDir\install.R

$CLONOPLOTLINKPREFIX = "https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoPlot"
$CLONOCALCLINKPREFIX = "https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoCalc"
$CLONOSUITELINKPREFIX = "https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master"


"Downloading and ClonoPlot from bitbucket"
downloadFile "$CLONOPLOTLINKPREFIX/ClonoPlot.zip" "$downloadDir\ClonoPlot.zip"
downloadFile "$CLONOCALCLINKPREFIX/ClonoCalc.zip" "$downloadDir\ClonoCalc.zip"

"Extract Apps to installDirClonoPlot"
Expand-ZIPFile "$downloadDir\ClonoPlot.zip" "$installDirClonoPlot"
Expand-ZIPFile "$downloadDir\ClonoCalc.zip" "$installDirClonoCalc"


"Delete temporary files"
Remove-Item "$downloadDir\ClonoPlot.zip"
Remove-Item "$downloadDir\ClonoCalc.zip"

"Downloading tutorial"
downloadFile "$CLONOSUITELINKPREFIX/ClonoSuite-Tutorial.pdf" "$executionDir\ClonoSuite-Tutorial.pdf"


"Downloading icons"
$IconClonoPlot = "ClonoPlot_Icon.ico"
$IconClonoCalc = "ClonoCalc_Icon.ico"

downloadFile "$CLONOPLOTLINKPREFIX/Icons/$IconClonoPlot" "$installDirClonoPlot\$IconClonoPlot"
downloadFile "$CLONOCALCLINKPREFIX/Icons/$IconClonoCalc" "$installDirClonoCalc\$IconClonoCalc"


function createShortcut($ShortcutName, $installDirClonoPlot, $JarFile, $IconLocation)
{
    $WshShell = New-Object -comObject WScript.Shell
    $Shortcut = $WshShell.CreateShortcut("$executionDir\$ShortcutName.lnk")
    $Shortcut.TargetPath = "$installDirClonoPlot\$JarFile"
    $Shortcut.WorkingDirectory = "$installDirClonoPlot"
    $Shortcut.IconLocation = $IconLocation
    $Shortcut.Save()
}

"Create shortcuts"
createShortcut "ClonoPlot" $installDirClonoPlot "ClonoPlot.jar" "$installDirClonoPlot\$IconClonoPlot"
createShortcut "ClonoCalc" $installDirClonoCalc "ClonoCalc.jar" "$installDirClonoCalc\$IconClonoCalc"


#$WshShell = New-Object -comObject WScript.Shell
#$Shortcut = $WshShell.CreateShortcut("$executionDir\ClonoPlot.lnk")
#$Shortcut.TargetPath = "$installDirClonoPlot\ClonoPlot.jar"
#$Shortcut.WorkingDirectory = "$installDirClonoPlot"
#$Shortcut.Save()

"Downloading samples for ClonoPlot from Bitbucket"
$SAMPLESCLONOPLOT = "$executionDir\samples_ClonoPlot"
mkdir $SAMPLESCLONOPLOT
cd $SAMPLESCLONOPLOT
downloadFile "$CLONOPLOTLINKPREFIX/testData.zip" "$SAMPLESCLONOPLOT\testData.zip"
Expand-ZIPFile "$SAMPLESCLONOPLOT\testData.zip" $pwd.Path
Remove-Item testData.zip
cd ..

"Downloading samples for ClonoCalc from Bitbucket"
$SAMPLESCLONOCALC = "$executionDir\samples_ClonoCalc"
mkdir $SAMPLESCLONOCALC
cd $SAMPLESCLONOCALC
downloadFile "$CLONOCALCLINKPREFIX/testData/sample_R1_50k.fastq.gz" "$SAMPLESCLONOCALC\sample_R1_50k.fastq.gz"
downloadFile "$CLONOCALCLINKPREFIX/testData/sample_R2_50k.fastq.gz" "$SAMPLESCLONOCALC\sample_R2_50k.fastq.gz"
cd ..

cd $installDirClonoPlot\tmp
"$SAMPLESCLONOPLOT\samplesA`n$SAMPLESCLONOPLOT\samplesB" | out-file selectedTableFolders.txt -encoding ASCII

",0,1" > serScriptJTreeExpansion.txt
",0,1,4" > serTableJTreeExpansion.txt
"1011 597 110 214" > selectedWindowSize.txt
cd ../..

"Installation is finished successfully."
