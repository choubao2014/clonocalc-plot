
#clear workspace
rm(list=ls())

setwd("C:/Users/Dino/Desktop/TestData/10-01-17_12_41_15/tables")
id_test_i6_ACTCAT <- read.table(file="test_i6_ACTCAT.xls", sep="\t", header=T, strip.white=T, stringsAsFactors=F)
id_test2_i8_AGACAC <- read.table(file="test2_i8_AGACAC.xls", sep="\t", header=T, strip.white=T, stringsAsFactors=F)

tmpList <- list(
id_test_i6_ACTCAT = id_test_i6_ACTCAT, 
id_test2_i8_AGACAC = id_test2_i8_AGACAC)
t <- scan("C:/Users/Dino/Desktop/TestData/10-01-17_12_41_15/log/overview.txt", what=character(), sep="\n")
tHP <- scan("C:/Users/Dino/Desktop/TestData/10-01-17_12_41_15/log/overview_high_precision.txt", what=character(), sep="\n")
library(data.table)

#determine column names with regular expressions
#the function assume that seq counts occur before other integer-based columns like Total.insertions
#otherways the pattern must consider number length
renameToMiTCRPattern <- function(dataFrame) {
  
  #try to build a tcr stucture
  newDataFrame <- data.frame("Read.count" = integer(0), "Percentage" = numeric(0), 
                             "CDR3.nucleotide.sequence" = character(0), "CDR3.amino.acid.sequence" = character(0), 
                             "V.segments" = character(0), "J.segments" = character(0), "D.segments" = character(0),
                             "Last.V.nucleotide.position" = integer(0), "First.D.nucleotide.position" = integer(0), 
                             "Last.D.nucleotide.position" = integer(0), "First.J.nucleotide.position" = integer(0), 
                             "VD.insertions" = integer(0), "DJ.insertions" = integer(0), "Total.insertions" = integer(0),
                             stringsAsFactors=FALSE)
  
  #add NA rows
  newDataFrame[1:length(dataFrame[,1]),1] <- NA
  
  if(rownames(dataFrame)[1] != "1"){
    if(length(grep("^[A-Za-z]*$", rownames(dataFrame)[1])) == 0) {
      #cat("unexpected row names detected")
    }
    else {
      newDataFrame[, 4] <- rownames(dataFrame)
    }
  }
  
  #determine the important columns first
  
  #how many lines will be tested?
  testlen <- 5
  
  #define the "important" columns
  impName <- c(	"Read.count", "Percentage", 		"CDR3.nucleotide.sequence", 
                "CDR3.amino.acid.sequence", "V.segments", 		"J.segments", 		"D.segments")
  
  # define the regular expression for the column machting
  # Percentage  0.999 or 0,999 or 0.01E-4
  # CDR3.nucleotide.sequence allow , to handle nun.nuc.sequences
  # CDR3.amino.acid.sequence avoid confusion with Quality like IIIIIIIII and the CDR3.nucleotide.sequence
  
  #		          Read.count	  Percentage	          CDR3.nucleotide.sequence
  pattern <- c(	"^[0-9]+$", 	"^[0-9]*(\\.|\\,)[0-9]*",	"^[a|c|g|t|A|C|G|T|\\,]+$",
                #CDR3.amino.acid.sequence	     
                "^[A-Za-z\\*~]+[^(I|A|C|G|T|a|c|g|t)][A-Za-z\\*~]+$",	
                #V.segments		            J.segments		            D.segments
                "^(TRBV|trbv|TRAV|trav)",	"^(TRBJ|trbj|TRAJ|traj)",	"^(TRBD|trbd|TRAD|trad)"	)
  
  
  grep(pattern[2], dataFrame[,2][1:testlen])
  
  for(n in colnames(dataFrame)) {
    for(i in 1:length(pattern))
      #check if column is always set, use only the first match
      if(is.na(newDataFrame[,impName[i]][1]) && 
         length(grep(pattern[i], dataFrame[,n][1:testlen])) == testlen) {
        newDataFrame[,impName[i]][1:length(dataFrame[,n])] <- dataFrame[,n]
        break
      }
  }
  
  #for v,d,j lines like, convert TRBV19*00(58),TRBV24*00(52,6) to TRBV19,TRBV24
  #process is slow, but should be occur rarely   
  for(i in c(5,6,7))
  {
    if(length(grep("\\*", newDataFrame[,i][1:testlen])) == testlen)
    {
      newDataFrame[,i] <- sapply(newDataFrame[,i] , 
                                 function(x){return(gsub("\\*[^A-Z]+\\([^A-Z]+\\)","",x[1]))}, USE.NAMES = FALSE)
    }
  }  
  
  #check if all "important" columns are filled
  for(n in names(impName)[-7]) {
    if(is.na(newDataFrame[1,n]))
      warning("No match for column ", n, " found!")
  }
  
  #try to determine the rest
  restName <- c("Last.V.nucleotide.position", "First.D.nucleotide.position", "Last.D.nucleotide.position", 
                "First.J.nucleotide.position", "VD.insertions", "DJ.insertions", "Total.insertions")
  for(n in colnames(dataFrame)) {
    for(a in restName) {
      if(a == n)
        newDataFrame[, a] <- dataFrame[,n]
    }
  }
  return(newDataFrame)
}


#del lines with out-of-frame or stop AA's
delOutOfFrameAndStop <- function(tbl)
{
	#find lines which contains a *
	tmp <- grep(c('\\*'), tbl[,"CDR3.amino.acid.sequence"])
	#del lines from the tmp vector
	if(length(tmp) != 0)
	  tbl <- tbl[-tmp, ]
	#find lines which contains a ~
	tmp <- grep(c('~'), tbl[,"CDR3.amino.acid.sequence"])
	#del lines from the tmp vector
	if(length(tmp) != 0)
	  tbl <- tbl[-tmp, ]

	return(tbl)
}

cut_mixcr_vj <- function(tbl)
{
  tbl[,"V.segments"] <- gsub("\\*00","",tbl[,"V.segments"])
  tbl[,"J.segments"] <- gsub("\\*00","",tbl[,"J.segments"])
  tbl[,"D.segments"] <- gsub("\\*00","",tbl[,"D.segments"])
  return(tbl)
}

#R-Function for merging lines with identical AA.Sequences
group_AA_TCR <- function(dataFrame) {
    #working with data.table improves the performance significant            
    tmp <- data.table(dataFrame)
   
    #merge lines with identical AA.Sequence
    tmp <- tmp[, list(     CDR3.nucleotide.sequence = CDR3.nucleotide.sequence[1],
				V.segments = V.segments[1],
				J.segments = J.segments[1],
				D.segments = D.segments[1],
				Last.V.nucleotide.position = Last.V.nucleotide.position[1],
				First.D.nucleotide.position = First.D.nucleotide.position[1],
				Last.D.nucleotide.position = Last.D.nucleotide.position[1],
				First.J.nucleotide.position = First.J.nucleotide.position[1],
				VD.insertions = VD.insertions[1],
				DJ.insertions = DJ.insertions[1],
				Total.insertions = Total.insertions[1],
                                      Read.count = sum(Read.count),
                                      Percentage = sum(Percentage)
                                ),                               
                        by = list(CDR3.amino.acid.sequence)]
                     
    #sort tabel descendend by Seq..Count
    tmp <- tmp[with(tmp, order(-Read.count)),]
   
    #convert data.table to data.frame
    tmp <- data.frame(tmp)
   
    #use column AA.Sequence as row.names
    #tmp <- data.frame(tmp[,-1], row.names = tmp[,"AA.Sequence"])
   
    #return data.frame with the selected columns
    tmp[,c("Read.count","Percentage","CDR3.nucleotide.sequence","CDR3.amino.acid.sequence",               
 	"V.segments","J.segments","D.segments","Last.V.nucleotide.position","First.D.nucleotide.position",
 	"Last.D.nucleotide.position","First.J.nucleotide.position","VD.insertions",            
 	"DJ.insertions","Total.insertions")]
}

#save tmpList
untouchedlist <- tmpList

#for(n in names(tmpList)) {
  #del all lines Count <2
  #tmpList[[n]] <- tmpList[[n]][which(tmpList[[n]][1] > 1),];
#}

#save Count <2
cuttedlist <- tmpList

for(n in names(tmpList)) {
	#convert to mitcr data-frame
	tmpList[[n]] <- renameToMiTCRPattern(tmpList[[n]])

	#cut *00
	tmpList[[n]] <- cut_mixcr_vj(tmpList[[n]])

	#delete out-of-frame und stops
	tmpList[[n]] <- delOutOfFrameAndStop(tmpList[[n]])
	
	#convert to numeric
	tmpList[[n]][,"Percentage"]<-as.numeric(tmpList[[n]][,"Percentage"]) 
	tmpList[[n]][,"Read.count"]<-as.numeric(tmpList[[n]][,"Read.count"]) 

	#merge duplicated AA-Seqs
	tmpList[[n]] <- group_AA_TCR(tmpList[[n]])

	#convert to data.frame
	tmpList[[n]]<-data.frame(tmpList[[n]],stringsAsFactors=F)

	#norm percentage to 1
	tmpList[[n]][,"Percentage"]<-tmpList[[n]][,"Read.count"]/sum(tmpList[[n]][,"Read.count"])

	#write data.frames csv
	#important: row.names=F,col.names=T
	tmpFilename <- paste0(n, "_post_processed.csv" )
	write.table(file=tmpFilename,tmpList[[n]],quote=4,row.names=F,col.names=T,dec=".",sep=";")
}

#save tmpList
wihout1count_norm_list <- tmpList
  
#save doku stats 
#tmpfinalstr contains the final log data
tmpfinalstr<- c("Cryo.Nr & ID & time & RNA & raw & post assembling(\\%) & mitcr(\\%) & ct mitcr & wo ones(\\%) & ct wo ones(\\%) & wo oof and stops & ct wo oof and stops(\\%) & $\\Delta$ ct \\\\ \\hline \\hline","")
tmpfinalstr_high_precision <- tmpfinalstr

options(warn=-1)

for(i in 2:length(t))
{
  tmp <- strsplit(t[i],"[&]")[[1]]
  tmpHP <- strsplit(tHP[i],"[&]")[[1]]
  tmpCryoNr <- tmp[1]
  tmpCryoNr <- gsub("[.]","dot",tmpCryoNr)
  tmpCryoNr <- gsub("[\\]","",tmpCryoNr)
  tmpCryoNr <- gsub(" ","",tmpCryoNr) 
  tmpRawReads <- as.numeric(tmpHP[5])
  
  for(n in names(wihout1count_norm_list)) {
    #search for CryoNr in the name
    n_clear <- gsub("id_","",n)
    n_clear <- gsub("_i[0-9]+_[A-Z]+$","",n_clear)
    if(tmpCryoNr == n_clear)
    {
      #mitcr(\%) & ct mitcr
      tmpMitcr <- sum(untouchedlist[[n]][,1])
      tmpMitcrPercent <- round((tmpMitcr/tmpRawReads)*100, digits=3)
      tmpMitcrPercentRound <- round((tmpMitcr/tmpRawReads)*100, digits=0)
      tmpMitcrRound <- round(sum(untouchedlist[[n]][,1])/1000000, digits=2)
      tmpMitcrClonotypes <- length(untouchedlist[[n]][,1])
      #update vector
      tmp[7] <- paste0(tmpMitcrRound,"(",tmpMitcrPercentRound,")")
      tmpHP[7] <- paste0(tmpMitcr,"(",tmpMitcrPercent,")")
      tmp[8] <- format(tmpMitcrClonotypes, big.mark=".")
      tmpHP[8] <- tmp[8]
      
      #wo 1er(\%) & ct wo 1er(\%)
      tmpOhne1er <- sum(cuttedlist[[n]][,1])
      tmpOhne1erPercent <- round((tmpOhne1er/tmpRawReads)*100, digits=3)
      tmpOhne1erPercentRound <- round((tmpOhne1er/tmpRawReads)*100, digits=0)
      tmpOhne1erRound <- round(sum(cuttedlist[[n]][,1])/1000000, digits=2)
      tmpOhne1erClonotypes <- length(cuttedlist[[n]][,1])
      tmpOhne1erClonotypesPercent <- round((tmpOhne1erClonotypes/tmpMitcrClonotypes)*100, digits=3)
      tmpOhne1erClonotypesPercentRound <- round((tmpOhne1erClonotypes/tmpMitcrClonotypes)*100, digits=0)
      #update vector
      tmp[9] <- paste0(tmpOhne1erRound,"(",tmpOhne1erPercentRound,")")
      tmpHP[9] <- paste0(tmpOhne1er,"(",tmpOhne1erPercent,")")
      tmp[10] <- paste0(format(tmpOhne1erClonotypes, big.mark="."),"(",tmpOhne1erClonotypesPercentRound,")")
      tmpHP[10] <- paste0(format(tmpOhne1erClonotypes, big.mark="."),"(",tmpOhne1erClonotypesPercent,")")
      
      #wo oof and stops & ct wo oof and stops(\%)
      tmpOOF <- sum(wihout1count_norm_list[[n]][,1])
      tmpOOFPercent <- round((tmpOOF/tmpRawReads)*100, digits=3)
      tmpOOFPercentRound <- round((tmpOOF/tmpRawReads)*100, digits=0)
      tmpOOFRound <- round(sum(wihout1count_norm_list[[n]][,1])/1000000, digits=2)
      tmpOOFClonotypes <- length(wihout1count_norm_list[[n]][,1])
      tmpOOFClonotypesPercent <- round((tmpOOFClonotypes/tmpMitcrClonotypes)*100, digits=3)
      tmpOOFClonotypesPercentRound <- round((tmpOOFClonotypes/tmpMitcrClonotypes)*100, digits=0)
      #update vector
      tmp[11] <- paste0(tmpOOFRound,"(",tmpOOFPercentRound,")")
      tmpHP[11] <- paste0(tmpOOF,"(",tmpOOFPercent,")")
      tmp[12] <- paste0(format(tmpOOFClonotypes, big.mark="."),"(",tmpOOFClonotypesPercentRound,")")
      tmpHP[12] <- paste0(format(tmpOOFClonotypes, big.mark="."),"(",tmpOOFClonotypesPercent,")")
      
      #insert & again
      tmpfinalstr[i] <- tmp[1]
      tmpfinalstr_high_precision[i] <- tmpHP[1]
      for(a in 2:length(tmp))
      {
        tmpfinalstr[i] <- paste(tmpfinalstr[i],"&",tmp[a])
        tmpfinalstr_high_precision[i] <- paste(tmpfinalstr_high_precision[i],"&",tmpHP[a])
      }
	tmpfinalstr[i] <- paste(tmpfinalstr[i],"& \\\\\\hline")
	tmpfinalstr_high_precision[i] <- paste(tmpfinalstr_high_precision[i],"& \\\\\\hline")
    }
  }
}

options(warn=0)

#clean up
rm(tmpList)
rm(tmpFilename)
rm(n)

#save workspace

write(tmpfinalstr, file="C:/Users/Dino/Desktop/TestData/10-01-17_12_41_15/log/overview_post_processing.txt")
write(tmpfinalstr_high_precision, file="C:/Users/Dino/Desktop/TestData/10-01-17_12_41_15/log/overview_post_processing_high_precision.txt")
save.image("C:/Users/Dino/Desktop/TestData/10-01-17_12_41_15/tables/workspace_tcr_post_processed.RData")
