![Icon_CC.png](https://bitbucket.org/repo/r4jBde/images/2057338154-Icon_CC.png)

### What is ClonoCalc? ###

ClonoCalc provides a GUI for the sub-tasks of processing raw NGS data by wrapping available implementations of various algorithms. ClonoCalc runs on Linux, Mac OS X and  Windows systems. Instructions for the automated or manual installation are given below.

ClonoCalc's Java front-end handles only the user input, while the main processing part is passed to a user-modifyable shell script. That script executes all tools for the individual analysis steps and aggregates their output. ClonoCalc ships with interfaces to a series of current well-established tools and comprises the following features.

* Input and demultiplexing of samples: To allow for analysing multiple samples that have been sequenced in the same run, ClonoCalc first splits the raw data (FASTQ format) and classifies the individual reads according to their barcode wrapping the [FASTX Barcode Splitter](https://github.com/agordon/fastx_toolkit). The user selects input and output files for the analysis and enters the barcodes to identify specific samples. A readable name can be chosen for every sample and the user is supported by an auto completion and error correction system.

* Clonotype determination: Raw NGS data is processed by the tool MiXCR  that performs paired-end read merging if using the Illumina system.  Despite this option MiXCR  is choosen for implementation processing raw NGS data as one of the most efficient and accurate tools to extract human or animal source BCR and TCR clonotypes providing corrections of erroneous sequences introduced by NGS [MiXCR](https://github.com/milaboratory/mixcr).  The call parameters (processing options) for MiXCR are read from a configuration file.

* Automatic documentation: A report of the data analysis process will be saved automatically. 
This allows the user to review, track and save specific options. 
The call parameters (options) can be modified if desired and will be stored for later reproduction of the analysis. A log file is stored that contains all information on the performed run, including the configuration parameters and the output of MiXCR. 

* Flexibility: Other back-ends replacing MiXCR may be used by adjusting the call script. This modular script-based approach allows for modification and extension without the need for recompilation.

![Icon_CP.png](https://bitbucket.org/repo/r4jBde/images/119155554-Icon_CP.png)

### What is Clonoplot? ###

ClonoPlot provides a user-friendly GUI for script based visualization in R for comparative BCR and TCR repertoire analysis. The user does not need to be familiar with R in order to create commonly used figures. The desired figure types can be easily chosen from the GUI. ClonoPlot runs on Windows, Linux and Mac OS X with the following features:

Input: From ClonoCalc or independent runs of [MiXCR](https://github.com/milaboratory/mixcr) ClonoPlot receives a tab-delimited file with clonotype sequences (both as amino acid and nucleotide sequence) and their abundances software) 

* Primary descriptive analysis is performed with the tcR package [tcR](https://imminfo.github.io/tcr/).

* The distribution of clonotypes are compared between samples and visualized with support of the R packages [RCircos](https://cran.r-project.org/web/packages/RCircos/index.html), [gplots](https://cran.r-project.org/web/packages/gplots/index.html) (for Venn diagrams) and statistics. 

* Documentation: ClonoPlot stores the program state and supports profiles to quickly switch between projects and configurations.
A preview mode allows the user to review and save specific visualizations. A batch mode allows multiple plots to be created and saved at once. 

* Flexibility: The set of available diagram types can be extended, if required. 
To do so, the user provides a corresponding script written in the R programming language. 
ClonoPlot then automatically reads this script file from its working directory and makes the new plot type available in the GUI.

### What do you need? ###
* R and Java compatible OS like Linux, Windows or Mac OS X
* Java Version 1.8 or later (needs ~ 300 MB free disk space)
* R Version 3.2.2 or later (needs ~ 500 MB free disk space)
* R-packages: *Rserve*, *data.table*, *tcR*, *gplots*, *ggplot2*, *stringr* and *RCircos*

### Installation ###

The installtion process is meant to be as easy and as transparant as possible. The lack of transparency spoke against the Windows-installer and we avoided administrative privileges to install software like R and Java, which the typical user of our software is like to have already installed. Hence, for each operating system two installation instructions were created:
 * one for a manual installation and
 * an alternatively provided script for an automated installation.

** Windows:**

Note: On Windows 10 deactivate the activated Linux Bash Shell.

Manual install:

* Install Java: If you already installed Java please open a command prompt (press Windows-Button + R and type **cmd**) and check via **java -version**, whether your installed version is >= 1.8.
If not or you have not installed Java yet, please visit https://java.com/en/download/ to download the newest java installer.
* Install R: If you already installed R please open a command prompt and check via the command **R --version**, whether your installed version is >= 3.2.2. If you are sure that R is installed, but you get the message *'R' is not recognized as an internal or external command, operable program or batch file.*, please continue reading at the next subitem. If not or you have not install R yet, please visit https://cran.r-project.org/bin/windows/base/ to download and install the newest R version.
* Install R-packages: Simply start a command prompt, type ```
Rscript -e 'list <- c("Rserve","data.table","ggplot2","gplots","stringr","tcR","RCircos");
            for(x in list) {
               if (!require(x,character.only = TRUE)){
                  install.packages(x, dependencies=T);
                  if(!require(x,character.only = TRUE))
                     stop(paste("Package", x, "installation failed!"))
               }
            }'
``` to install the required packages. 
* Install ClonoPlot: First download the file [*ClonoPlot.zip*](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoPlot/ClonoPlot.zip).
Extract the content of *ClonoPlot.zip* to a local folder (called installation folder). ClonoPlot use this installation folder as a temporary folder, i.e there must exists some MB of free space for profiles and temporary script files (of course you need also the permission to write).
To test ClonoPlot you can download the provided file [testData.zip](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoPlot/testData.zip) and extract the four sample files.
For more information about how to get started, follow the [ClonoSuite Tutorial](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoSuite-Tutorial.pdf).
Now you can start ClonoPlot from the command prompt *java -jar ClonoPlot.jar* or with a double-click in the file browser if you linked the java runtime with jar-files.
* Install ClonoCalc: First download the file [*ClonoCalc.zip*](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoCalc/ClonoCalc.zip).
Extract the content of *ClonoCalc.zip* to a local folder (called installation folder). ClonoCalc use this installation folder as a temporary folder, i.e there must exists some GB of free space temporary files (of course you need also the permission to write).
To test ClonoCalc you can download the provided files [*sample_R1_50k.zip*](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoCalc/testData/sample_R1_50k.fastq.gz) and [*sample_R2_50k.zip*](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoCalc/testData/sample_R2_50k.fastq.gz). 
Now you can start ClonoCalc from the command prompt *java -jar ClonoCalc.jar* or with a double-click in the file browser if you linked the java runtime with jar-files.
When you first start ClonoCalc, there already filled some basic input fields related to the sample files.
For more information about how to get started, follow the [ClonoSuite Tutorial](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoSuite-Tutorial.pdf).

Automated installation:

* First download the file [*ClonoCalcPlot_Install.zip*](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/Install/Windows/ClonoCalcPlot_Install.zip).
* Extract the content of *ClonoCalcPlot_Install.zip* to a local folder (called installation folder). ClonoPlot and ClonoCalc use this installation folder as a temporary folder, i.e there must exists some MB of free space for profiles and temporary script files (of course you need also the permission to write).
* To install ClonoPlot and ClonoCalc and all it dependencies, click right on *ClonoCalcPlot_install.cmd* and select *Run as administrator*.
* After the installation you can delete the installation files namely *ClonoCalcPlot_install.cmd*, *install_subscript.ps1* and the folder *Java-Update*. 

* ClonoPlot and ClonCalc shortcuts can now be used to start the applications the next time.

Expericend users can check the file *install_subscript.ps1* to get more involved into the installation process.
The script will install the newest version of Java (if you have already the newest version this step is skipped).
Afterwards version 3.3.1 of R will be installed (this step will be skipped, if the installation script find a R Version >= 3.2.2)
Finally, the script will install the mentioned R libraries and extract the ClonoPlot and ClonoCalc files to the installation folder.


**Linux:**

Manual installation:

* Install Java: If you already installed Java please open a terminal and check via **java -version**, whether your installed version is >= 1.8.
If not or you have not installed Java yet, please install these from your Linux distribution. On Debian/Ubuntu type **sudo apt-get update** to update your package index and afterwards **sudo apt-get install default-jre** to install Java. Alternatively visit https://java.com/en/download/ to get more information how download and install java for your Linux distribution.
* Install R: If you have already installed R please open a terminal and check via the command **R --version**, whether your installed version is >= 3.2.2.
If not or you have not install R yet, please type **sudo apt-get install r-base** to install R. This command will download and install R from the official packet sources of your Linux distribution. After the installation is finished, please check via  **R --version**, whether the installed R version is >= 3.2.2. All The
current Ubuntu releases (16.04) includes a sufficiently recent version
(3.2.3) of R. If your Linux distributions does not use the apt package manager oder install an R version < 3.2.2 please visit [Jason French Blog](http://www.jason-french.com/blog/2013/03/11/installing-r-in-linux/) to get an introduction how to install a newer version from the CRAN repositories.
* Install build environment for R package: To build packages for R you will need a C/C++/Fortran compiler, because there exists no binary packages for Linux. 
The package **r-base-dev** contains all needed compilers as dependencies and is marked as Recommends for R. I.e. **r-base-dev** is already installed, if your Linux distribution install recommend packages (Recommends are installed by default, if you use Ubuntu >= 10.04). See http://packages.ubuntu.com/xenial/r-base-dev for more details.
* Install R-packages: Simply start a terminal, type ```
Rscript -e 'list <- c("Rserve","data.table","ggplot2","gplots","stringr","tcR","RCircos");
            for(x in list) {
               if (!require(x,character.only = TRUE)){
                  install.packages(x, dependencies=T);
                  if(!require(x,character.only = TRUE))
                     stop(paste("Package", x, "installation failed!"))
               }
            }'
``` to install the required packages. 
* Install ClonoPlot: First download the file [*ClonoPlot.zip*](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoPlot/ClonoPlot.zip).
Extract the content of *ClonoPlot.zip* to a local folder (called installation folder). ClonoPlot use this installation folder as a temporary folder, i.e there must exists some MB of free space for profiles and temporary script files (of course you need also the permission to write).
To test ClonoPlot you can download the provided file [testData.zip](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoPlot/testData.zip) and extract the four sample files.
For more information about how to get started, follow the [ClonoSuite Tutorial](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoSuite-Tutorial.pdf).
Now you can start ClonoPlot from the terminal *java -jar ClonoPlot.jar* or with a double-click in the file browser if you linked the java runtime with jar-files.
* Install ClonoCalc: First download the file [*ClonoCalc.zip*](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoCalc/ClonoCalc.zip).
Extract the content of *ClonoCalc.zip* to a local folder (called installation folder). ClonoCalc use this installation folder as a temporary folder, i.e there must exists some GB of free space temporary files (of course you need also the permission to write).
To test ClonoCalc you can download the provided files [*sample_R1_50k.fastq.gz*](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoCalc/testData/sample_R1_50k.fastq.gz) and [*sample_R2_50k.fastq.gz*](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoCalc/testData/sample_R2_50k.fastq.gz). 
Now you can start ClonoCalc from the terminal *java -jar ClonoCalc.jar* or with a double-click in the file browser if you linked the java runtime with jar-files.
When you first start ClonoCalc, there already filled some basic input fields related to the sample files.
For more information about how to get started, follow the [ClonoSuite Tutorial](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoSuite-Tutorial.pdf).

Automated installation:

* The installation is written for the Ubuntu distribution, please goto the *Manual install* part above to get an instruction for other Linux distributions.
* First download the file [*ClonoCalcPlot_install.sh*](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/Install/Linux/ClonoCalcPlot_install.sh).
* Move the downloaded file *ClonoCalcPlot_install.sh* to a local folder (called installation folder). ClonoPlot uses this installation folder as a temporary folder, i.e there must exists some MB of free space for profiles and temporary script files (of course you need also the permission to write).
* Now right-click into the folder to open the context menu and click *Open Terminal Here*. First type in **chmod u+x ClonoCalcPlot_install.sh** to mark the script as executable. Second, if you have not installed R please type **sudo ./ClonoCalcPlot_install.sh** to execute the script and start the installation process. Otherwise you can run the script without administrative privileges with the command **./ClonoCalcPlot_install.sh**.
* The script will check R and Java versions and will do updates only if the found versions are not sufficent.
* The installation will create launchers for ClonoCalc and ClonoPlot in the installation folder. Feel free to move the launchers to your favorite place.
Please do not move the folders *java*, *ClonoPlot* or *ClonoCalc*, because the launchers will use absolute paths to these folders.
* The installation will also download sample files and the joint tutorial for ClonoCalc and ClonoPlot.


** Mac OS X:**

Manual installation:

* Install Java: If you already installed Java please open a terminal (use spotlight search and type **terminal**) and check via **java -version**, whether your installed version is >= 1.8.
If not or you have not installed Java yet, please visit https://java.com/en/download/ to download the newest java installer.
* Install R: If you already installed R please open a terminal and check via the command **R --version**, whether your installed version is >= 3.2.2.
If not or you have not install R yet, please visit https://cran.r-project.org/bin/macosx/ to download and install the newest R version.
* Install R-packages: Simply start a terminal, type ```
Rscript -e 'list <- c("Rserve","data.table","ggplot2","gplots","stringr","tcR","RCircos");
               for(x in list) { if (!require(x,character.only = TRUE)){
                  install.packages(x, dependencies=T);
                  if(!require(x,character.only = TRUE))
                     stop(paste("Package", x, "installation failed!"));
                  }
               }'
```
to install the required packages. 
* Install ClonoPlot: First download the file [*ClonoPlot.zip*](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoPlot/ClonoPlot.zip).
Extract the content of *ClonoPlot.zip* to a local folder (called installation folder). ClonoPlot use this installation folder as a temporary folder, i.e there must exists some MB of free space for profiles and temporary script files (of course you need also the permission to write).
To test ClonoPlot you can download the provided file [testData.zip](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoPlot/testData.zip) and extract the four sample files.
For more information about how to get started, follow the [ClonoSuite Tutorial](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoSuite-Tutorial.pdf).
Now you can start ClonoPlot from the terminal *java -jar ClonoPlot.jar* or with a double-click in the file browser if you linked the java runtime with jar-files.
* Install ClonoCalc: First download the file [*ClonoCalc.zip*](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoCalc/ClonoCalc.zip).
Extract the content of *ClonoCalc.zip* to a local folder (called installation folder). ClonoCalc use this installation folder as a temporary folder, i.e there must exists some GB of free space temporary files (of course you need also the permission to write).
To test ClonoCalc you can download the provided files [*sample_R1_50k.fastq.gz*](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoCalc/testData/sample_R1_50k.fastq.gz) and [*sample_R2_50k.fastq.gz*](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoCalc/testData/sample_R2_50k.fastq.gz). 
Now you can start ClonoCalc from the terminal *java -jar ClonoCalc.jar* or with a double-click in the file browser if you linked the java runtime with jar-files.
When you first start ClonoCalc, there already filled some basic input fields related to the sample files.
For more information about how to get started, follow the [ClonoSuite Tutorial](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/ClonoSuite-Tutorial.pdf).

Automated installation:

* First download the file [*ClonoCalcPlot_install.sh*](https://bitbucket.org/ClonoSuite/clonocalc-plot/raw/master/Install/MacOSX/ClonoCalcPlot_install.sh).
* Open a terminal (You can use the Spotlight search and type in *terminal*)
* Move to the download folder via **cd**. Typically the file *ClonoCalcPlot_install.sh* is saved to your download folder.   
In this case use the command **cd ~/Downloads**
* Make the script executable. Use the command **chmod u+x ClonoCalcPlot_install.sh**
* Start the installation process by executing the script via the command **./ClonoCalcPlot_install.sh**
* You need administrative privileges to install R, therefore the installation will ask for your password at start.
* Wait for the message *Installation is finished successfully.*
* The installtion will create a folder named *ClonoCalc_Plot_Install*. Here you can find launchers, the tutorial and sample files for ClonoCalc and ClonoPlot.
* After the installation you can start the apps by double-clicking on one of the launchers. Feel free to move the created launchers to your application-folder.

### Contact ###
Feel free to submit an issue via BitBucket or to email us on faehnrich@anat.uni-luebeck.de
