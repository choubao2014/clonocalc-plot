package app;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JTree;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.apache.log4j.Logger;

import Listener.MyDocumentListener;
import Listener.MyTreeModelListener;
import checkboxtree.CheckTreeManager;
import util.FileOperations;
import util.Gzip;
import util.MySerialization;
import util.OSDetection;
import util.OSDetection.OperatingSys;
import util.ScriptParser;
import util.ScriptParser.Parameter;
import util.ScriptParser.Script;
import util.TreeFuncs;

//Singleton Pattern
public class GlobalVars {
	private static final Logger log = Logger.getLogger(GlobalVars.class);

	// find out os
	public final static OperatingSys os = OSDetection.getOperatingSys();

	public static boolean isReady = false;
	private static final int lightFactorGreen = 210, lightFactorRed = 150;
	public static final Color colorInvalid = new Color(255, lightFactorRed, lightFactorRed);
	public static final Color colorInvalidFont = Color.RED;
	// public static final Color colorInvalid = Color.RED;
	public static final Color colorValid = new Color(lightFactorGreen, 255, lightFactorGreen);
	public static final Color colorValidFont = Color.BLUE;
	// public static final Color colorValid = Color.GREEN;
	public static final Color colorDefault = Color.WHITE;
	public static final Color colorWarning = Color.ORANGE;

	public static final String charsetName = "UTF-8";

	private static GlobalVars singleton = null;

	private boolean namesTableChanged = true;

	public final int jTreeWidth = 500;
	public final int jTreeHeight = 300;
	public final int jTableWidthPref = jTreeWidth;
	public final int jTableHeightPref = 150;
	public final int jTableWidthMin = jTreeWidth;
	public final int jTableHeightMin = 100;

	public final String tmpFolder = "tmp";
	public final String profilesFolder = "profiles";
	public final String selectedTableFoldersFileName = "selectedTableFolders.txt";
	public final String selectedTablesFileName = "selectedTables.txt";
	public final String selectedTablesNamesFileName = "selectedTablesNames.txt";
	public final String selectedScriptsFileName = "selectedScripts.txt";
	public final String selectedScriptsParameterFileName = "selectedScriptsParameter.txt";
	public final String selectedWindowSize = "selectedWindowSize.txt";
	public final String outputFolderFileName = "outputFolder.txt";
	public final static String speciesAlphabetPath = "DO_NOT_DELETE_alphabets.txt";
	public final String rscriptFileName = "rscript.R";
	public final String passwordFileName = "password.txt";
	public final static String defaultScriptFolder = "Analysis";
	public final static String mainScriptsSubfolder = "_DO_NOT_DELETE";
	public final static String scriptDirAbsolutePath = System.getProperty("user.dir") + File.separator
			+ defaultScriptFolder;
	public final static String mainScriptsDirAbsolutePath = scriptDirAbsolutePath + File.separator
			+ mainScriptsSubfolder;
	public final static String scriptParentDirAbsolutePath = System.getProperty("user.dir");
	public final String[] allowedExTable = { "xls", "csv" };
	public final String[] allowedExScript = { "R", "r" };
	public final static String doneScriptStr = "Done Nr.";
	public final static String failedScriptStr = "FAILED Nr.";
	public final String executionFinishedStr = "Execution finished";
	public final String[] warningStrArr = new String[] { "Warning", "Warnmeldung" };

	private String usedSpeciesAlphabet;

	public enum AdvOptEnum {
		cutReadCount, cutReadPercent, prefixForAllPlots, suffixForAllPlots
	}

	// if criticalOpt are changed, data frames need to be updated
	public AdvOptEnum[] criticalAdvOpt = new AdvOptEnum[] { AdvOptEnum.cutReadCount, AdvOptEnum.cutReadPercent };

	private EnumMap<AdvOptEnum, String> advancedOptEnumMap = new EnumMap<AdvOptEnum, String>(AdvOptEnum.class);

	private File lastSelectedDir;
	private File lastSavedFileFolder;
	private String lastSavedFileName;

	// is true if the init process is finished
	private static boolean readyToPlot = false;
	private static ActionListener btnPreviewActionListener;

	public boolean advancedOptions = false;

	// private Krypto krypto; for encrypted bug reports
	public PlotDemo plotDemo;

	public final MySerialization ser = new MySerialization(System.getProperty("user.dir") + File.separator + tmpFolder);

	public int selectedScriptsCount = 0;

	private List<String> selectedScripts = new ArrayList<String>();
	private List<Script> selectedScriptsClass = new ArrayList<Script>();
	private List<String> selectedScriptRFuncNames = new ArrayList<String>();

	public final String cbAutoCompleteText = "Autocompletion";
	// private boolean autoComplete = true;

	public final String defaultTxtOutputFolder = System.getProperty("user.home");
	public JTextField txtOutputFolder = new JTextField(defaultTxtOutputFolder);

	public BashExecution bashExecution = new BashExecution();
	// set by panel loading constructor
	public ActionListener PipelineFinshedListener;

	// tree for tables
	private final String tableRootNodeName = "Tables";
	public DefaultMutableTreeNode tableRootNode = new DefaultMutableTreeNode(tableRootNodeName);
	public DefaultTreeModel tableTreeModel = new DefaultTreeModel(tableRootNode);

	// tree for scripts
	private final String scriptRootNodeName = "";
	public DefaultMutableTreeNode scriptRootNode = new DefaultMutableTreeNode(scriptRootNodeName);
	public DefaultTreeModel scriptTreeModel = new DefaultTreeModel(scriptRootNode);

	MyDefaultTableModel namesTableModel = new MyDefaultTableModel();
	DefaultTableModel advancedOptTableModel = new DefaultTableModel();

	// DefaultTableModel paraTableModel = new DefaultTableModel();

	private HashMap<String, Pair> hashmapTableName = new HashMap<String, Pair>();

	class Pair {
		private TreePath path;
		String name;

		Pair(TreePath path, String name) {
			this.path = path;
			this.name = name;
		}

		public TreePath getPath() {
			return path;
		}
	}

	private HashMap<TreePath, ScriptParser.Script> hashmapScripts = new HashMap<TreePath, ScriptParser.Script>();
	TreePath hashmapParameterFunctionSelectedPath = null;
	private Script selectedScript = null;
	private Parameter selectedParameter = null;

	public JTextPane txtpDebug = new JTextPane();
	public JScrollPane spTxtpDebug = new JScrollPane(txtpDebug);

	private GlobalVars() {
		try {
			// do this first to increase start perfomance
			plotDemo = new PlotDemo();

			// for encrypted bugreports
			// String key =
			// FileOperations.readFirstLineInString(passwordFileName);
			// if (key != null)
			// krypto = new Krypto(
			// FileOperations.readFirstLineInString(passwordFileName),
			// "AES");

		} catch (Exception e) {
			log.error(e, e);
		}

		namesTableModel.setDataVector(null, new String[] { "Path to table", "Name in plot" });

		initAdvancedOptEnumMap();
		advancedOptTableModel.setDataVector(null, new String[] { "Parameter", "Value" });

		fillComponents();

		txtOutputFolder.setBackground(GlobalVars.colorValid);
		txtOutputFolder.getDocument().putProperty("owner", txtOutputFolder);
		txtOutputFolder.getDocument().addDocumentListener(new MyDocumentListener());

		scriptTreeModel.addTreeModelListener(new MyTreeModelListener());

		spTxtpDebug.setAutoscrolls(true);

		// restoreOutfileName();
		// paraTableModel.setDataVector(null, new String[] { "Parameter",
		// "Value" });

		// add sripts in default folder to the tree
		// addScriptFolderToTree(scriptTreeModel, null, defaultScriptFolder,
		// allowedExScript);

		// restore all parameter values
		// restoreScriptParameters();

		// advancedOptTableModel = ser.restoreAdvancedOptTable();
	}

	public static GlobalVars singleton() {
		if (singleton == null) {
			singleton = new GlobalVars();
			isReady = true;
		}
		return singleton;
	}

	private void initAdvancedOptEnumMap() {
		advancedOptEnumMap.put(AdvOptEnum.cutReadCount, "Cut reads by counts lower than");
		advancedOptEnumMap.put(AdvOptEnum.cutReadPercent, "Cut reads by percent [0-100] lower than");
		advancedOptEnumMap.put(AdvOptEnum.prefixForAllPlots, "Suffix for all plot files");
		advancedOptEnumMap.put(AdvOptEnum.suffixForAllPlots, "Prefix for all plot files");
		// public final String cutReadCountStr = "Cut reads by counts lower
		// than";
		// public final String cutReadPercentStr = "Cut reads by percent [0-100]
		// lower than";
		// public final String prefixForAllPlotsTitle = "Suffix for all plot
		// files";
		// public final String suffixForAllPlotsTitle = "Prefix for all plot
		// files";
	}

	private void restoreOutfileName() {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(
				new FileInputStream(tmpFolder + File.separator + outputFolderFileName), charsetName))) {
			String line = br.readLine();

			if (line != null) {
				File folder = new File(line);
				if (folder.exists() && folder.isDirectory())
					txtOutputFolder.setText(line);
			}
		} catch (FileNotFoundException e) {
			// log.error(e, e);
		} catch (IOException e) {
			// log.error(e, e);
		}
	}

	void restoreNamesTable() {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(
				new FileInputStream(tmpFolder + File.separator + selectedTablesNamesFileName), charsetName))) {
			String line = br.readLine();

			int lineCounter = 0;
			String fileNamePath = "";
			while (line != null) {
				if ((lineCounter % 2) == 0) {
					File folder = new File(line);
					if (folder.exists() && folder.isFile()
							&& TreeFuncs.checkForNodeName(tableRootNode, folder.getParent()))
						fileNamePath = line;
					else
						fileNamePath = "";
				} else {
					// if there isnt a valid name or file does not exist
					if (!fileNamePath.equals("")) {
						// check if row alredy exists
						// boolean exist = false;
						for (int i = 0; i < namesTableModel.getRowCount(); i++) {
							if (namesTableModel.getValueAt(i, 0).equals(fileNamePath)) {
								// exist = true;
								namesTableModel.setValueAt(line, i, 1);
								hashmapTableName.get(fileNamePath).name = line;
								break;
							}
						}
						// if (!exist)
						// namesTableModel.addRow(new String[] { fileNamePath,
						// line });
					}
				}
				lineCounter++;
				line = br.readLine();
			}
		} catch (FileNotFoundException e) {
			// log.error(e, e);
		} catch (IOException e) {
			// log.error(e, e);
		}
	}

	public void restoreTableTree(final CheckTreeManager tableTreeCheckManager, JTree tableTree) {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(
				new FileInputStream(tmpFolder + File.separator + selectedTableFoldersFileName), charsetName))) {
			String line = br.readLine();

			while (line != null) {
				File folder = new File(line);
				if (folder.exists() && folder.isDirectory())
					addFilesToTree(tableTree, new File(line), allowedExTable, tableTreeCheckManager);
				line = br.readLine();
			}
		} catch (FileNotFoundException e) {
			// log.error(e, e);
		} catch (IOException e) {
			// log.error(e, e);
		}

		// restore checked tables
		try (BufferedReader br = new BufferedReader(new InputStreamReader(
				new FileInputStream(tmpFolder + File.separator + selectedTablesFileName), charsetName))) {
			String line = br.readLine();

			while (line != null) {
				File file = new File(line);
				if (file.exists() && file.isFile()) {
					final TreePath treePath = addFileToTree(tableTree, file.getParent(), file.getName(),
							tableTreeCheckManager, true);

					// final TreePath scriptTreePath = getTreePathFromPath(
					// (DefaultMutableTreeNode) tableTreeModel.getRoot(),
					// path);
					if (treePath != null) {
						// SwingUtilities.invokeLater(new Runnable() {
						// @Override
						// public void run() {
						// tableTreeCheckManager.getSelectionModel().toggleRemoveSelection(treePath);
						// tableTreeCheckManager.getSelectionModel().addSelectionPaths(new
						// TreePath[] { treePath });
						// }
						// });
						addSampleToTableAndTree(treePath, tableTreeCheckManager);
					}
				}
				line = br.readLine();
			}
		} catch (FileNotFoundException e) {
			// log.error(e, e);
		} catch (IOException e) {
			// log.error(e, e);
		}
	}

	void restoreScriptTree(final CheckTreeManager scriptTreeCheckManager) {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(
				new FileInputStream(tmpFolder + File.separator + selectedScriptsFileName), charsetName))) {
			String line = br.readLine();

			while (line != null) {

				// script name even
				String scriptPath = line;
				// String scriptParameter = br.readLine();
				line = br.readLine();
				// if(scriptParameter == null)

				File script = new File(scriptPath);
				// boolean a = script.exists();
				// boolean b = script.isFile();
				if (!script.exists() || !script.isFile())
					continue;

				int beginRelativeScriptPath = System.getProperty("user.dir").length() + 1;
				String relativeScriptPath = scriptPath.substring(beginRelativeScriptPath);
				final TreePath scriptTreePath = TreeFuncs
						.getTreePathFromPath((DefaultMutableTreeNode) scriptTreeModel.getRoot(), relativeScriptPath);
				if (scriptTreePath != null) {
					// SwingUtilities.invokeLater(new Runnable() {
					// @Override
					// public void run() {
					scriptTreeCheckManager.getSelectionModel().toggleRemoveSelection(scriptTreePath);
					scriptTreeCheckManager.getSelectionModel().addSelectionPaths(new TreePath[] { scriptTreePath });
					// TreePath scriptCheckedPaths[] =
					// scriptTreeCheckManager.getSelectionModel().getSelectionPaths();
					// log.debug(scriptCheckedPaths.length);
					// }
					// });

				}

			}
		} catch (FileNotFoundException e) {
			// log.error(e, e);
		} catch (IOException e) {
			// log.error(e, e);
		}
	}

	void restoreScriptParameters() {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(
				new FileInputStream(tmpFolder + File.separator + selectedScriptsParameterFileName), charsetName))) {
			String line = br.readLine();

			// String fileNamePath = "";
			while (line != null) {

				String scriptPath = line;
				String scriptParameter = br.readLine();
				if (scriptParameter == null)
					break;

				if (new File(scriptPath).exists()) {

					int beginRelativeScriptPath = System.getProperty("user.dir").length() + 1;
					String relativeScriptPath = scriptPath.substring(beginRelativeScriptPath);
					final TreePath scriptTreePath = TreeFuncs.getTreePathFromPath(
							(DefaultMutableTreeNode) scriptTreeModel.getRoot(), relativeScriptPath);
					if (scriptTreePath != null) {
						Script script = hashmapScripts.get(scriptTreePath);
						if (script == null)
							continue;

						List<String> parameters = ScriptParser.parseHeader("dataFrameList" + scriptParameter + ",");
						for (String parameter : parameters) {
							// try to parse "name = value" to "name" and "value"
							String parameterNameValue[] = ScriptParser.parseParameter(parameter);
							if (parameterNameValue == null)
								continue;

							// try to update value in table and hashmap
							script.tryToUpdateParameterValue(parameterNameValue[0], parameterNameValue[1]);
						}
					}
				}
				line = br.readLine();
			}
		} catch (FileNotFoundException e) {
			// log.error(e, e);
		} catch (IOException e) {
			// log.error(e, e);
		}
	}

	public void restoreDefaults() {
		clearComponents();
		addScriptFolderToTreeModel(scriptTreeModel, defaultScriptFolder, allowedExScript);
	}

	private void fillComponents() {
		// do the reload procedure
		restoreOutfileName();

		// add sripts in default folder to the tree
		addScriptFolderToTreeModel(scriptTreeModel, defaultScriptFolder, allowedExScript);

		// restore all parameter values
		restoreScriptParameters();

		ser.restoreAdvancedOptTable(advancedOptTableModel);
	}

	private void clearComponents() {
		log.debug("reload profile");
		// clear all components
		while (tableRootNode.getChildCount() > 0) {
			tableTreeModel.removeNodeFromParent((DefaultMutableTreeNode) tableRootNode.getChildAt(0));
		}

		while (scriptRootNode.getChildCount() > 0) {
			scriptTreeModel.removeNodeFromParent((DefaultMutableTreeNode) scriptRootNode.getChildAt(0));
		}

		hashmapScripts.clear();
	}

	public void reloadProfile() {
		namesTableChanged = true;
		clearComponents();
		fillComponents();
	}

	public int addFilesToTableTree(JTree tree, File folder, final CheckTreeManager tableTreeCheckManager) {
		return addFilesToTree(tree, folder, allowedExTable, tableTreeCheckManager);
	}

	private int addFilesToTree(JTree tree, File folder, String[] allowedExtensions,
			final CheckTreeManager tableTreeCheckManager) {
		// function only restore table tree
		String curPath = folder.getPath();
		String[] objects = folder.list();
		Arrays.sort(objects);
		int counter = 0;
		boolean noTables = true;
		for (int i = 0; i < objects.length; i++) {
			String object = objects[i];
			String newFileName = curPath + File.separator + object;
			File newFile = new File(newFileName);
			if (newFile.isFile()) {
				String extension = "";
				if (object.lastIndexOf('.') > 0) {
					extension = object.substring(object.lastIndexOf('.') + 1);
				}
				if (TreeFuncs.isValidString(allowedExtensions, extension)) {
					if (addFileToTree(tree, curPath, object, tableTreeCheckManager, false) == null)
						counter++;
					noTables = false;
				}
			}
		}
		if (noTables)
			return -1;
		return counter;
	}

	private TreePath addFileToTree(JTree tree, String folder, String file, final CheckTreeManager tableTreeCheckManager,
			boolean mustExist) {
		DefaultTreeModel treeModel = (DefaultTreeModel) tree.getModel();
		DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) treeModel.getRoot();

		DefaultMutableTreeNode folderNode = null;
		for (int i = 0; i < rootNode.getChildCount(); i++) {
			if (rootNode.getChildAt(i).toString().equals(folder)) {
				folderNode = (DefaultMutableTreeNode) rootNode.getChildAt(i);
			}
		}
		// create new node for "unknown" folder
		if (folderNode == null) {
			if (mustExist)
				return null;
			folderNode = new DefaultMutableTreeNode(folder);
			treeModel.insertNodeInto(folderNode, rootNode, rootNode.getChildCount());
		}

		// check if file node already exists
		for (int i = 0; i < folderNode.getChildCount(); i++) {
			if (folderNode.getChildAt(i).toString().equals(file)) {
				return TreeFuncs.getPath(folderNode.getChildAt(i));
			}
		}
		if (mustExist)
			return null;

		// insert file into new folder node
		DefaultMutableTreeNode fileNode = new DefaultMutableTreeNode(file);
		treeModel.insertNodeInto(fileNode, folderNode, folderNode.getChildCount());

		TreePath fileNodePath = new TreePath(fileNode.getPath());
		tableTreeCheckManager.getSelectionModel().toggleRemoveSelection(fileNodePath);

		log.debug("Add file: " + file + " to folder: " + folder);

		if (tree != null)
			tree.scrollPathToVisible(fileNodePath);
		// log.debug("getpath: " + folderNode.getPath());
		return null;
	}

	private int addScriptFolderToTreeModel(DefaultTreeModel treeModel, String scriptFolderRelativePath,
			String[] allowedExtensions) {

		File scriptFolder = new File(scriptFolderRelativePath);
		String[] directories = scriptFolder.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) {
				return new File(current, name).isDirectory();
			}
		});
		if (directories != null)
			for (String subDirectory : directories) {
				addScriptFolderToTreeModel(treeModel, scriptFolderRelativePath + File.separator + subDirectory,
						allowedExtensions);
			}

		String curPath = scriptFolderRelativePath;
		log.debug("curÜath: " + curPath);
		String[] objects = scriptFolder.list();
		if (objects == null)
			return -1;
		int counter = 0;
		boolean noTables = true;
		for (int i = 0; i < objects.length; i++) {

			String object = objects[i];
			String newFileName = curPath + File.separator + object;
			File newFile = new File(newFileName);

			if (!newFile.isFile())
				continue;

			// check for R script
			String extension = "";
			if (object.lastIndexOf('.') > 0) {
				extension = object.substring(object.lastIndexOf('.') + 1);
			}
			if (!TreeFuncs.isValidString(allowedExtensions, extension))
				continue;

			TreePath objectPath = addScriptToTreeModel(treeModel, (DefaultMutableTreeNode) treeModel.getRoot(),
					curPath + File.separator, object);
			if (objectPath != null) {
				counter++;

				Script script = ScriptParser.parseScriptFull(curPath + File.separator + object);
				// set table model of script
				DefaultTableModel paraTableModel = new DefaultTableModel(null, new String[] { "Parameter", "Value" });
				script.setDefaultTableModel(paraTableModel);
				HashMap<String, Parameter> parameterMap = script.getMapParameter();

				if (parameterMap != null) {
					for (Parameter parameter : parameterMap.values()) {
						log.debug("Set " + parameter.getName() + " to " + parameter.getDefaultValue());
						List<String> allowedValues = parameter.getAllowedValues();
						paraTableModel.addRow(new String[] { parameter.getName(), parameter.getDefaultValue() });
						if (allowedValues.size() == 0) {

						}
						// // add combobox
						// else {
						// // if there any allowed values, create a combobox
						// // for it
						// String items[] = allowedValues
						// .toArray(new String[allowedValues.size()]);
						// }
					}
				} else {
					log.debug("no parameters for " + object);
					paraTableModel = new DefaultTableModel(null, new String[] { "", "No Parameters found!" });
				}
				hashmapScripts.put(objectPath, script);
			}
			noTables = false;
		}
		if (noTables)
			return -1;
		return counter;
	}

	private TreePath addScriptToTreeModel(DefaultTreeModel treeModel, DefaultMutableTreeNode parentNode,
			String restPath, String file) {
		log.debug("Rest path:" + restPath);

		DefaultMutableTreeNode fileNode;
		int nextSeparatorPos = restPath.indexOf(File.separator);
		String nextChildNodeName = "";
		if (nextSeparatorPos > 0) {
			nextChildNodeName = restPath.substring(0, nextSeparatorPos);
			restPath = restPath.substring(nextSeparatorPos + 1);

			DefaultMutableTreeNode nextChildNode = null;
			for (int i = 0; i < parentNode.getChildCount(); i++) {
				if (parentNode.getChildAt(i).toString().equals(nextChildNodeName)) {
					nextChildNode = (DefaultMutableTreeNode) parentNode.getChildAt(i);
					return addScriptToTreeModel(treeModel, nextChildNode, restPath, file);
				}
			}
			// create node
			nextChildNode = new DefaultMutableTreeNode(nextChildNodeName);
			treeModel.insertNodeInto(nextChildNode, parentNode, parentNode.getChildCount());

			return addScriptToTreeModel(treeModel, nextChildNode, restPath, file);
		} else {
			// check if file node already exists
			for (int i = 0; i < parentNode.getChildCount(); i++) {
				if (parentNode.getChildAt(i).toString().equals(file))
					return null;
			}

			// insert file into new folder node
			fileNode = new DefaultMutableTreeNode(file);
			treeModel.insertNodeInto(fileNode, parentNode, parentNode.getChildCount());

		}
		return TreeFuncs.getPath(fileNode);
	}

	public List<Script> getSelctedScripts(List<TreePath> checkedPaths) {
		List<Script> selectedScripts = new ArrayList<Script>();
		for (TreePath path : checkedPaths) {
			String filePath = TreeFuncs.getFilePathFromTreePathArbitrary(path);
			if (filePath == null)
				continue;
			else
				selectedScripts.add(hashmapScripts.get(path));
		}
		return selectedScripts;
	}

	public void writeSelectedToFile(List<TreePath> checkedPaths) {
		// check for folder tmp
		File tmpFolderFile = new File(tmpFolder);
		if (!tmpFolderFile.exists())
			tmpFolderFile.mkdirs();

		List<String> outFolder = new ArrayList<String>();
		outFolder.add(txtOutputFolder.getText());

		// save these global, because of the extern checked paths
		selectedScripts.clear();
		selectedScriptsClass = getSelctedScripts(checkedPaths);
		log.debug("checked paths:" + Integer.toString(checkedPaths.size()));
		for (Script script : selectedScriptsClass) {
			String parameterLine = script.getParameterLine();
			if (parameterLine != null) {
				selectedScripts.add(script.getAbsolutePathToScript());
				log.debug(script.getAbsolutePathToScript());
				selectedScripts.add(parameterLine);
				log.debug(parameterLine);
			}
		}

		List<String> selectedTables = getSelectedTables();
		if (selectedTables.size() > 0)
			FileOperations.writeListToFile(tmpFolder + File.separator + selectedTablesFileName, selectedTables);

		List<String> selectedTablesNames = getSelectedTablesNames();
		if (selectedTablesNames.size() > 0)
			FileOperations.writeListToFile(tmpFolder + File.separator + selectedTablesNamesFileName,
					selectedTablesNames);

		if (selectedScripts.size() > 0)
			FileOperations.writeListToFile(tmpFolder + File.separator + selectedScriptsFileName, selectedScripts);
		if (!txtOutputFolder.getText().equals(""))
			FileOperations.writeListToFile(tmpFolder + File.separator + outputFolderFileName, outFolder);

		List<String> selectedScriptsParameter = getSelectedScriptsParameter();
		FileOperations.writeListToFile(tmpFolder + File.separator + selectedScriptsParameterFileName,
				selectedScriptsParameter);
	}

	public List<String> getSelectedTables() {
		List<String> selectedTables = new ArrayList<String>();
		for (int i = 0; i < namesTableModel.getRowCount(); i++)
			selectedTables.add((String) namesTableModel.getValueAt(i, 0));
		return selectedTables;
	}

	public List<String> getSelectedTablesNames() {
		List<String> selectedTablesNames = new ArrayList<String>();
		for (int i = 0; i < namesTableModel.getRowCount(); i++) {
			selectedTablesNames.add((String) namesTableModel.getValueAt(i, 0));
			selectedTablesNames.add((String) namesTableModel.getValueAt(i, 1));
		}
		return selectedTablesNames;
	}

	public List<String> getSelectedScriptsParameter() {
		List<String> selectedScriptsParameter = new ArrayList<String>();
		for (TreePath path : hashmapScripts.keySet()) {
			DefaultTableModel tableModel = hashmapScripts.get(path).getDefaultTableModel();
			String parameterLine = ", ";

			for (int i = 0; i < tableModel.getRowCount(); i++) {
				String paraName = (String) tableModel.getValueAt(i, 0);
				String paraValue = (String) tableModel.getValueAt(i, 1);
				parameterLine += paraName + " = " + paraValue + ", ";
			}
			parameterLine = parameterLine.substring(0, parameterLine.lastIndexOf(","));
			String filePath = TreeFuncs.getFilePathFromTreePathArbitrary(path);
			selectedScriptsParameter.add(System.getProperty("user.dir") + File.separator + filePath);
			selectedScriptsParameter.add(parameterLine);
		}
		return selectedScriptsParameter;
	}

	public void onExit() {
		// serialize tree models
		// ser.saveTableTreeModel(tableTreeModel);
		// ser.saveScriptTreeModel(scriptTreeModel);

		if (tableRootNode.getChildCount() == 0)
			return;
		// save table tree
		List<String> tableFoldersList = new ArrayList<String>();
		for (int i = 0; i < tableRootNode.getChildCount(); i++) {
			tableFoldersList.add(tableRootNode.getChildAt(i).toString());
		}
		FileOperations.writeListToFile(tmpFolder + File.separator + selectedTableFoldersFileName, tableFoldersList);

		ser.saveAdvancedOptTable(advancedOptTableModel);
	}

	public void addSampleToTableAndTree(TreePath path, CheckTreeManager checkTreeManager) {
		namesTableChanged = true;

		// add to tree model
		checkTreeManager.getSelectionModel().addSelectionPath(path);

		if (path.getPathComponent(0).toString().equals(scriptRootNodeName))
			return;

		// List<String> list = TreeFuncs.getListOfFilesFromPath(path);
		List<TreePath> listTreePaths = TreeFuncs.getListOfLeafsFromPath(path);

		for (TreePath filePath : listTreePaths) {
			// check if row alredy exists
			String pathStr = TreeFuncs.treePathToString(filePath);
			boolean exists = false;
			for (int i = 0; i < namesTableModel.getRowCount(); i++) {
				if (namesTableModel.getValueAt(i, 0).equals(pathStr))
					exists = true;
			}
			if (exists)
				continue;
			// log.debug(filePath);

			// try to get old name
			Pair hashmapEntry = hashmapTableName.get(pathStr);
			String name;
			if (hashmapEntry == null) {
				name = pathStr.substring(pathStr.lastIndexOf(File.separator) + 1);
				name = name.substring(0, name.indexOf("."));
				name = ScriptParser.parseFileName(name);
				// save NamesTable name in Hashmap
				hashmapTableName.put(pathStr, new Pair(filePath, name));
			} else {
				name = hashmapEntry.name;
			}
			namesTableModel.addRow(new String[] { pathStr, name });
		}
	}

	public void delSampleFromTableAndTree(TreePath path, CheckTreeManager checkTreeManager) {
		namesTableChanged = true;

		// remove from tree model
		checkTreeManager.getSelectionModel().removeSelectionPath(path);

		if (path.getPathComponent(0).toString().equals(scriptRootNodeName))
			return;

		List<String> list = TreeFuncs.getListOfFilesFromPath(path);

		for (String filePath : list) {
			for (int i = 0; i < namesTableModel.getRowCount(); i++) {
				if (namesTableModel.getValueAt(i, 0).equals(filePath)) {
					// log.debug("remove from table: " +
					// namesTableModel.getValueAt(i, 0));
					namesTableModel.removeRow(i);
				}
			}
		}
	}

	public void namesTableGetUp(int movedRow) {
		if (movedRow > 0) {
			int newPos = movedRow - 1;
			namesTableModel.moveRow(movedRow, movedRow, newPos);
		}
	}

	public void namesTableGetDown(int movedRow) {
		if (movedRow < namesTableModel.getRowCount() - 1) {
			int newPos = movedRow + 1;
			namesTableModel.moveRow(movedRow, movedRow, newPos);
		}
	}

	public Script getTableModelFromPath(TreePath path) {
		hashmapParameterFunctionSelectedPath = path;
		return hashmapScripts.get(path);
	}

	public TreePath getHashmapStrToTreePath(String path) {
		for (TreePath treePath : hashmapScripts.keySet()) {
			if (treePath.toString().equals(path))
				return treePath;
		}
		return null;
	}

	public TreePath getSelectedScriptTablePath() {
		return hashmapParameterFunctionSelectedPath;
	}

	public String createBugReport() {
		File bugReportFolder = new File(txtOutputFolder.getText() + File.separator + "Bug_Report");
		// if (!bugReportFolder.exists())
		// bugReportFolder.mkdir();

		// <source, dest>
		HashMap<String, String> bugReportFilesDirs = new HashMap<String, String>();

		// copy Rscript

		String source = tmpFolder + File.separator + rscriptFileName;
		String destDir = "";
		log.debug("Copy " + source + " to " + destDir);
		bugReportFilesDirs.put(source, destDir);

		// File destFile = new File(bugReportFolder, rscriptFileName);
		// log.debug("Copy " + tmpFolder + File.separator + rscriptFileName
		// + " to " + destFile.getAbsolutePath());
		// copyFileUsingStream(new File(tmpFolder + File.separator
		// + rscriptFileName), destFile);

		// copy selected tables
		File tablesFolder = new File(bugReportFolder, "tables");
		if (!tablesFolder.exists())
			tablesFolder.mkdir();
		for (int i = 0; i < namesTableModel.getRowCount(); i++) {
			File table = new File((String) namesTableModel.getValueAt(i, 0));
			if (table.exists()) {
				// destFile = new File(tablesFolder, table.getName());
				// log.debug("Copy " + table.getAbsolutePath() + " to "
				// + destFile.getAbsolutePath());
				// copyFileUsingStream(table, destFile);
				source = table.getAbsolutePath();
				destDir = "tables";
				log.debug("Copy table " + source + " into " + destDir);
				bugReportFilesDirs.put(source, destDir);
			}
		}

		// copy selected Scripts
		File scriptsFolder = new File(bugReportFolder, "scripts");
		if (!scriptsFolder.exists())
			scriptsFolder.mkdir();
		// remember only every second line is a path
		for (int i = 0; i < selectedScripts.size(); i += 2) {
			File script = new File(selectedScripts.get(i));
			// destFile = new File(scriptsFolder, script.getName());
			// log.debug("Copy " + script.getAbsolutePath() + " to "
			// + destFile.getAbsolutePath());
			// copyFileUsingStream(script, destFile);

			source = script.getAbsolutePath();
			destDir = "scripts";
			log.debug("Copy table " + source + " into " + destDir);
			bugReportFilesDirs.put(source, destDir);
		}

		// save Script output
		String scriptLogName = "script_output.log";
		// destFile = new File(bugReportFolder, "script_output.log");
		// log.debug("Copy TextBox output to " + destFile.getAbsolutePath());
		// Document doc = txtpDebug.getDocument();
		List<String> dummyList = new ArrayList<String>();
		dummyList.add(txtpDebug.getText());
		File destFile = new File(tmpFolder, scriptLogName);
		FileOperations.writeListToFile(destFile.getAbsolutePath(), dummyList);
		source = destFile.getAbsolutePath();
		destDir = "";
		log.debug("Copy table " + source + " into " + destDir);
		bugReportFilesDirs.put(source, destDir);

		// finally save log file
		// destFile = new File(bugReportFolder, "java.log");
		// log.debug("Copy " + "all.log" + " to " + destFile.getAbsolutePath());
		// copyFileUsingStream(new File("all.log"), destFile);
		source = "all.log";
		destDir = "";
		log.debug("Copy table " + source + " into " + destDir);
		bugReportFilesDirs.put(source, destDir);

		// zip bug report folder
		DateFormat dateFormat = new SimpleDateFormat("dd_MM_yy__HH_mm_ss");
		Date date = new Date();
		// Gzip.zipFileOrDir(bugReportFolder.getAbsolutePath(),
		// txtOutputFolder.getText() + File.separator + "bug_report_"
		// + dateFormat.format(date) + ".zip");
		String bugReportEncryptedZipAbsolutePath = txtOutputFolder.getText() + File.separator + "bug_report_encrypted"
				+ dateFormat.format(date) + ".zip";
		String bugReportZipAbsolutePath = txtOutputFolder.getText() + File.separator + "bug_report"
				+ dateFormat.format(date) + ".zip";
		Gzip.encryptZipFileDirList(bugReportFilesDirs, bugReportZipAbsolutePath, null);

		// use these for encrypted bug reports
		// Gzip.encryptZipFileDirList(bugReportFilesDirs,
		// bugReportEncryptedZipAbsolutePath, krypto);
		// Gzip.decryptZip(bugReportZipAbsolutePath,
		// bugReportEncryptedZipAbsolutePath, krypto);
		return bugReportEncryptedZipAbsolutePath;
	}

	public List<String> getSelectedScripts() {
		return selectedScripts;
	}

	public List<String> getSelectedScriptRFuncNames() {
		return selectedScriptRFuncNames;
	}

	public void setSelectedScriptRFuncNames(List<String> selectedScriptRFuncNames) {
		this.selectedScriptRFuncNames = selectedScriptRFuncNames;
	}

	public Script getSelectedScript() {
		return selectedScript;
	}

	public void setSelectedScript(Script selectedScript) {
		this.selectedScript = selectedScript;
	}

	public int getCutReadCount() {
		try {
			return Integer
					.parseInt((String) tryToGetValueFromTableModel(advancedOptTableModel, AdvOptEnum.cutReadCount));
		} catch (Exception e) {
			log.error(e, e);
		}
		return 0;
	}

	public double getCutReadPercent() {
		try {
			return Double.parseDouble(
					(String) tryToGetValueFromTableModel(advancedOptTableModel, AdvOptEnum.cutReadPercent));
		} catch (Exception e) {
			log.error(e, e);
		}
		return 0;
	}

	public String getPrefixNameForAllPlots() {
		return tryToGetValueFromTableModel(advancedOptTableModel, AdvOptEnum.prefixForAllPlots);
	}

	public String getSuffixNameForAllPlots() {
		return tryToGetValueFromTableModel(advancedOptTableModel, AdvOptEnum.suffixForAllPlots);
	}

	public String tryToGetValueFromTableModel(DefaultTableModel defaultTableModel, AdvOptEnum advOptEnum) {
		for (int i = 0; i < defaultTableModel.getRowCount(); i++) {
			if (((String) defaultTableModel.getValueAt(i, 0)).equals(advancedOptEnumMap.get(advOptEnum))) {
				try {
					return (String) defaultTableModel.getValueAt(i, 1);
				} catch (Exception e) {
					log.error(e, e);
				}
			}
		}
		return "";
	}

	public void criticalAdvOptHandling(String advOpt) {
		for (AdvOptEnum advOptEnum : advancedOptEnumMap.keySet()) {
			String value = advancedOptEnumMap.get(advOptEnum);
			if (advOpt.equals(value)) {
				for (AdvOptEnum critical : criticalAdvOpt) {
					if (advOptEnum == critical) {
						namesTableChanged = true;
						log.info("critical adv opt changed! :" + advOpt);
						return;
					}
				}
				return;
			}
		}
	}

	public boolean isNamesTableChanged() {
		return namesTableChanged;
	}

	public void setNamesTableChanged(boolean namesTableChanged) {
		this.namesTableChanged = namesTableChanged;
	}

	public File getLastSelectedDir() {
		return lastSelectedDir;
	}

	public void setLastSelectedDir(File lastSelectedDir) {
		this.lastSelectedDir = lastSelectedDir;
	}

	public File getLastSavedFileFolder() {
		return lastSavedFileFolder;
	}

	public void setLastSavedFileFolder(File lastSavedFileFolder) {
		this.lastSavedFileFolder = lastSavedFileFolder;
	}

	public String getLastSavedFileName() {
		return lastSavedFileName;
	}

	public void setLastSavedFileName(String lastSavedFileName) {
		this.lastSavedFileName = lastSavedFileName;
	}

	public static boolean isReadyToPlot() {
		return readyToPlot;
	}

	public static boolean setReadyToPlot(boolean readyToPlot) {
		GlobalVars.readyToPlot = readyToPlot;
		if (readyToPlot) {
			// check if GUI is ready
			if (btnPreviewActionListener == null)
				return false;
			else
				btnPreviewActionListener.actionPerformed(null);
		}
		return true;
	}

	public ActionListener getBtnPreviewActionListener() {
		return btnPreviewActionListener;
	}

	public void setBtnPreviewActionListener(ActionListener btnPreviewActionListener) {
		GlobalVars.btnPreviewActionListener = btnPreviewActionListener;
	}

	public List<Script> getSelectedScriptsClass() {
		return selectedScriptsClass;
	}

	public HashMap<String, Pair> getHashmapTableName() {
		return hashmapTableName;
	}

	public Parameter getSelectedParameter() {
		return selectedParameter;
	}

	public void setSelectedParameter(Parameter selectedParameter) {
		this.selectedParameter = selectedParameter;
	}

	public String getUsedSpeciesAlphabet() {
		return usedSpeciesAlphabet;
	}

	public void setUsedSpeciesAlphabet(String usedSpeciesAlphabet) {
		usedSpeciesAlphabet = usedSpeciesAlphabet.replace("\r", "");
		this.usedSpeciesAlphabet = usedSpeciesAlphabet;
	}
}
