package Listener;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultMutableTreeNode;

import org.apache.log4j.Logger;

public class MyTreeModelListener implements TreeModelListener {
	private static final Logger log = Logger.getLogger(MyTreeModelListener.class);

	public void treeNodesChanged(TreeModelEvent e) {
		DefaultMutableTreeNode node;
		node = (DefaultMutableTreeNode) (e.getTreePath().getLastPathComponent());
		/*
		 * If the event lists children, then the changed node is the child of
		 * the node we have already gotten. Otherwise, the changed node and the
		 * specified node are the same.
		 */
		try {
			int index = e.getChildIndices()[0];
			node = (DefaultMutableTreeNode) (node.getChildAt(index));
			log.debug(node.getUserObject().toString());
		} catch (NullPointerException ex) {
			log.error(ex, ex);
		}

		// log.debug("The user has finished editing the node.");
		// log.debug("New value: " + node.getUserObject());
	}

	public void treeNodesInserted(TreeModelEvent e) {
	}

	public void treeNodesRemoved(TreeModelEvent e) {
	}

	public void treeStructureChanged(TreeModelEvent e) {
	}
}