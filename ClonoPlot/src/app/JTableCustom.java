package app;

import javax.swing.JTable;

public class JTableCustom extends JTable {

	private static final long serialVersionUID = 1L;

	// only second column is editable aka names of the tables in the plo
	@Override
	public boolean isCellEditable(int row, int col) {
		switch (col) {
		case 1:
			return true;
		default:
			return false;
		}
	}

	public boolean getScrollableTracksViewportWidth() {
		return getPreferredSize().width < getParent().getWidth();
	}
}
