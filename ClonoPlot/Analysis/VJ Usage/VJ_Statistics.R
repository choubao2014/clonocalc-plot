#' Plot the V or J-usage for different samples as a histogram.
#'
#' @param title string() Set the title of the plot.
#' @param width integer() Set the width of the diagram.
#' @param height integer() Set the height of the diagram.
#' @param v.usage bool() If true calc v.usage else calc j.usage.
#' @param plot.sd.error.bars bool() If true plot error bars. 
#' @param norm bool() F=Counts or T=Percentage.
#' @param group.regex Group the input samples by their names.
#' @param font.size float() Set size font for the plot.
#' @param angle.x.label float() Rotation for the x-axis labels.
#' @param x.label.vjust float() Vertical offset for x-axis labels.
#'
#' @examples
#' vj_usage_grouped_mean_histogram(l.test, v.usage=T, group.regex = c('Group WT' = 'WT', 'Group KO' = 'KO'), plot.sd.error.bars = T)
#' @export
#' @importFrom reshape2 dcast
#' @import ggplot2


VJ_Statistics <- function(dataFrameList, title = "Usage mean histogram", height = 10, width = 12, v.usage = TRUE, plot.sd.error.bars = TRUE, norm = TRUE, group.regex = c("Group 1" = "sampleA", "Group 2" = "sampleB"), font.size = 15, angle.x.label = 90, x.label.vjust = 0.5) 
  {
    if (typeof(dataFrameList) != "list")
        stop("dataFrameList is not a list!")
    if (is.null(names(dataFrameList)))
        stop("No data frame names found!")
    if (sum(duplicated(names(dataFrameList))) > 0)
        stop("Not all names are unique!")
  
    l <- dataFrameList
    
    segements <- "J.segments"
    if (v.usage)
      segements <- "V.segments"
    
    # merge list to one big df
    l.df <- data.frame(segments = do.call(c, lapply(l, function(x) x[, segements])))
    # add names column to differ rows
    l.df$name <- rep(names(l), sapply(l, nrow))
    
    # dcast and merge as a workaround for empty group variables (reshape2) group
    # data.frame by AA-lengths and sample names
    l.df.dcast <- reshape2::dcast(l.df, segments ~ name, fun.aggregate = length,
                                  value.var = "segments")
    
    
    # make first column to row names
    rownames(l.df.dcast) <- l.df.dcast[, 1]
    l.df.dcast <- l.df.dcast[, -1]
    
    ylabel <- "Count"
    if (norm) {
      l.df.dcast <- data.frame(apply(l.df.dcast, MARGIN = 2, function(x) x * 100/sum(x)))
      ylabel <- "Percentage"
    }
    
    if (is.null(group.regex)) {
      
      l.df.dcast$genes = rownames(l.df.dcast)

      l.df.melt <- melt(l.df.dcast)
      
      colnames(l.df.dcast) <- c("Genes", "variable", "value")
      
      p <- ggplot(l.df.melt, aes(x = genes, y = value, fill = variable)) + 
        geom_bar(position = "dodge", stat = "identity") + 
        labs(x = "V-Genes", y = ylabel) + 
        theme(axis.text.x = element_text(angle = 90, hjust = 1)) + 
        ggtitle(title) + 
        theme(text = element_text(size = font.size), axis.text.x = element_text(angle = angle.x.label, vjust = x.label.vjust))
      return(p)
      
    }else{
      # match pre test
      for (regex in group.regex) {
          if (sum(grepl(regex, names(l))) == 0)
              stop("no match for \"", regex, "\" in sample names! sample names = \"",
                  Reduce(function(x, y) paste(x, y, sep = "\", \""), names(l)), "\"")
      }
  
      # define groups
      for (i in seq_along(group.regex)) {
          regex <- group.regex[i]
          # get group columns which corresponds to regex
          regex.sub.df <- l.df.dcast[, (grep(regex, colnames(l.df.dcast)[1:length(l)]))]
          regex.sub.df.fct <- function(fct) {
              if (is.vector(regex.sub.df))
                  return(sapply(regex.sub.df, fct))
              apply(regex.sub.df, 1, fct)
          }
          # add 2 columns for each group (mean and sd)
          l.df.dcast[[names(group.regex)[i]]] <- regex.sub.df.fct(mean)
          l.df.dcast[[paste0(names(group.regex)[i], ".sd")]] <- regex.sub.df.fct(sd)
      }
      

  
      # delete first length(l) columns
      l.df.dcast <- l.df.dcast[, -(1:length(l))]
  
      # melt data.frame to make it ready for ggplot
      indices.mean <- 2 * (1:length(group.regex)) - 1
      # handle edge case only one group
      if (length(indices.mean) == 1)
          l.df.melt.mean <- melt(matrix(l.df.dcast[, indices.mean], dimnames = list(rownames(l.df.dcast), colnames(l.df.dcast)[indices.mean])))
      else l.df.melt.mean <- melt(as.matrix(l.df.dcast[, indices.mean]))
  
      indices.sd <- 2 * (1:length(group.regex))
      # handle edge case only one group
      if (length(indices.sd) == 1)
          l.df.melt.sd <- melt(matrix(l.df.dcast[, indices.sd], dimnames = list(rownames(l.df.dcast), colnames(l.df.dcast)[indices.sd])))
      else l.df.melt.sd <- melt(as.matrix(l.df.dcast[, indices.sd]))
  
  
      l.df.melt.mean$sd <- l.df.melt.sd$value
      colnames(l.df.melt.mean) <- c("Genes", "Groups", "value", "sd")

      p <- ggplot(l.df.melt.mean, aes(x = Genes, y = value, fill = Groups)) + geom_bar(position = "dodge",
          stat = "identity") + labs(x = "V-Genes", y = ylabel) + theme(axis.text.x = element_text(angle = 90,
          hjust = 1)) + ggtitle(title) + theme(text = element_text(size = font.size),
          axis.text.x = element_text(angle = angle.x.label, vjust = x.label.vjust))
  
      if (plot.sd.error.bars)
          # Width of the error bars
      p <- p + geom_errorbar(aes(ymin = value - sd, ymax = value + sd), width = 0.25,
          position = position_dodge(0.9))
  
      return(p)
    }
  }
