dir.create("r_data_script", showWarnings = F)
setwd("r_data_script")

#save workspace
save.image("workspace.RData")

#save list exclusive as 'l'
l<-tmpList
save(file="list.RData",l)
rm(l)

#save processed tables as csv files
dir.create("tables", showWarnings = F)
setwd("tables")
for(n in names(tmpList)) {
	write.table(file=paste0(n,".csv"), tmpList[[n]],quote=4,row.names=F,col.names=T,dec=".",sep=";")
}
setwd("..")

#save r-code
file.copy(scriptPath, "rscript.R", overwrite = T)
setwd("..")