package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import app.GlobalVars;

public class ScriptParser {
	private static final Logger log = Logger.getLogger(ScriptParser.class);

	private final static String paramPattern = "@param";
	private final static String descriptionPattern = "@description";
	private final static String defaultNoDescriptionAvailibleText = "No description availible";

	public static class Script {
		private HashMap<String, Parameter> mapParameter;
		private DefaultTableModel defaultTableModel;
		private String description = defaultNoDescriptionAvailibleText;
		private String name;
		private String absolutePathToScript;

		public Script(String name, HashMap<String, Parameter> mapParameter, String description, String absolutePath) {
			this.name = name;
			this.mapParameter = mapParameter;
			this.description = description;
			this.absolutePathToScript = GlobalVars.scriptParentDirAbsolutePath + File.separator + absolutePath;
		}

		public void tryToUpdateParameterValue(String parameterName, String parameterValue) {
			// update value only if the value exists
			// this avoid old invalid parameters
			if (getMapParameter().containsKey(parameterName)) {
				// update value in hashmap
				getMapParameter().get(parameterName).setDefaultValue(parameterValue);
				// update value in table
				for (int i = 0; i < getDefaultTableModel().getRowCount(); i++) {
					if (getDefaultTableModel().getValueAt(i, 0).equals(parameterName)) {
						getDefaultTableModel().setValueAt(parameterValue, i, 1);
					}
				}
			}
		}

		public String tryToGetValue(String parameterName) {
			for (int i = 0; i < getDefaultTableModel().getRowCount(); i++) {
				if (getDefaultTableModel().getValueAt(i, 0).equals(parameterName)) {
					return (String) getDefaultTableModel().getValueAt(i, 1);
				}
			}
			return null;
		}

		public String getParameterLine() {
			if (defaultTableModel == null || defaultTableModel.getRowCount() == 0)
				return null;
			String parameterLine = ", ";
			for (int i = 0; i < defaultTableModel.getRowCount(); i++) {
				String paraName = (String) defaultTableModel.getValueAt(i, 0);
				String paraValue = (String) defaultTableModel.getValueAt(i, 1);
				parameterLine += paraName + " = " + paraValue + ", ";
			}
			return parameterLine.substring(0, parameterLine.lastIndexOf(","));
		}

		public void setDefaultTableModel(DefaultTableModel defaultTableModel) {
			this.defaultTableModel = defaultTableModel;
		}

		public DefaultTableModel getDefaultTableModel() {
			return defaultTableModel;
		}

		public String getDescription() {
			return description;
		}

		public String getName() {
			return name;
		}

		public HashMap<String, Parameter> getMapParameter() {
			return mapParameter;
		}

		public String getAbsolutePathToScript() {
			return absolutePathToScript;
		}
	}

	public static class Parameter {
		private String name = "";
		String type = null;
		private String description = defaultNoDescriptionAvailibleText;
		private String defaultValue = "";
		List<String> allowedValues = new ArrayList<String>();
		private String regex = null;
		private String leftIntervalBound = null;
		private String rightIntervalBound = null;
		private boolean intervalBounded = false;

		private final String typeBool = "bool";

		public Parameter(String name, String defaultValue) {
			this.setName(name);
			this.setDefaultValue(defaultValue);
		}

		public Parameter(String name, String description, String type, List<String> allowedValues) {
			this.setName(name);
			this.type = type;
			this.setDescription(description);
			this.allowedValues = allowedValues;
		}

		public Parameter(String name, String description, String type, String regex) {
			this.setName(name);
			this.type = type;
			this.setDescription(description);
			this.regex = regex;
		}

		public Parameter(String name, String description, String type, String leftIntervalBound,
				String rightIntervalBound) {
			this.setName(name);
			this.type = type;
			this.setDescription(description);
			this.leftIntervalBound = leftIntervalBound;
			this.rightIntervalBound = rightIntervalBound;
			intervalBounded = true;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			if (description != null)
				this.description = description;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDefaultValue() {
			return defaultValue;
		}

		public void setDefaultValue(String defaultValue) {
			this.defaultValue = defaultValue;
		}

		public List<String> getAllowedValues() {
			return allowedValues;
		}

		public String getType() {
			return type;
		}

		public boolean isBoolean() {
			if (type != null && type.equals(typeBool))
				return true;
			return false;
		}

		/**
		 * Return the regex for the parameter validation
		 * 
		 * @return regex as String or null if no regex exists
		 */
		public String getRegex() {
			return regex;
		}

		/**
		 * Return the left interval bound for the parameter validation e.g. "[3"
		 * or "]1"
		 * 
		 * @return left interval bound as String or null if no bound exists
		 */
		public String getLeftIntervalBound() {
			return leftIntervalBound;
		}

		/**
		 * Return the right interval bound for the parameter validation e.g.
		 * "[3" or "]1"
		 * 
		 * @return right interval bound as String or null if no bound exists
		 */
		public String getRightIntervalBound() {
			return rightIntervalBound;
		}

		/**
		 * Is the parameter bound by an interval? This function exists only to
		 * improve the performance.
		 * 
		 * @return true if bound, false if not
		 */
		public boolean isIntervalBounded() {
			return intervalBounded;
		}

		/**
		 * Return the interval as a String e.g. "[0-1]"
		 * 
		 * @return interval string
		 */
		public String getIntervalAsText() {
			return leftIntervalBound + "-" + rightIntervalBound;
		}
	}

	public static Script parseScriptFull(String scriptPath) {
		log.debug(scriptPath);
		HashMap<String, Parameter> parameterMap = new HashMap<String, Parameter>();
		String scriptDescriptionText = "";
		String scriptName = "";

		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(scriptPath), "UTF-8"))) {
			String line = br.readLine();
			String functionHeader = "";
			// List<ScriptParser.Parameter> parameterList = new
			// ArrayList<ScriptParser.Parameter>();

			boolean headerBegin = false;
			boolean opened = false;
			boolean description = false;
			// boolean param = false;
			int open = 0;

			while (line != null) {
				// parse meta info
				if (line.contains("#'")) {
					String content = line.substring(2);
					if (content.contains(descriptionPattern)) {
						// cut the @description tag
						content = content.substring(content.indexOf(descriptionPattern) + descriptionPattern.length());
						description = true;
					}
					if (description) {
						if (content.matches("^\\s$"))
							description = false;
						else
							scriptDescriptionText += content;
					}
					// @param marginSide string("hallo","peter") Set the margin
					// on the side.
					// TODO param can use multiple lines
					if (content.contains(paramPattern)) {
						Parameter parameter = parseParamLine(content);
						parameterMap.put(parameter.getName(), parameter);
					}
				}
				// check for start of function definition
				if (line.contains("<-") && line.contains("function")) {
					headerBegin = true;
					scriptName = line.substring(0, line.indexOf("<-")).replaceAll("\\s", "");
				}
				if (headerBegin)
					functionHeader += line;
				if (headerBegin) {
					int i;
					for (i = 0; i < line.length(); i++) {
						open += bracketParser(line.charAt(i));
						if (open == 1)
							opened = true;
						else if (opened && open == 0)
							break;
						if (open == -1) {
							try {
								throw new Exception("closed bracket before an open one!");
							} catch (Exception e) {
								log.error(e, e);
							}
						}
					}
					// headerBegin = false;
					if (open == 0) {
						for (String parameter : parseHeader(
								functionHeader.substring(functionHeader.indexOf('(') + 1, i) + ",")){
							String paraParts[] = parseParameter(parameter);
							// set value
							if (paraParts != null) {
								// an description exists
								if (parameterMap.containsKey(paraParts[0]))
									parameterMap.get(paraParts[0]).setDefaultValue(paraParts[1]);
								// if there is not a description to the
								// parameter
								else
									parameterMap.put(paraParts[0], new Parameter(paraParts[0], paraParts[1]));
							}
						}
						break;
					}
				}
				line = br.readLine();
			}
		} catch (FileNotFoundException e) {
			log.error(e, e);
		} catch (IOException e) {
			log.error(e, e);
		}
		if (scriptDescriptionText.equals(""))
			scriptDescriptionText = defaultNoDescriptionAvailibleText;
		return new Script(scriptName, parameterMap, scriptDescriptionText, scriptPath);
	}

	private static int bracketParser(char c) {
		if (c == '(') {
			return 1;
		} else if ((c == ')')) {
			return -1;
		}
		return 0;
	}

	public static List<String> parseHeader(String header) {
		// int start = header.indexOf("(") + 1;
		// int end = header.indexOf(")");
		// delete all whitespaces
		// String rawParameter = header.substring(start, end).replaceAll("\\s+",
		// "");
		// String rawParameter = header.substring(start, end);
		List<String> parameters = new ArrayList<String>();
		int open = 0;
		int lastindex = 0;
		for (int i = 0; i < header.length(); i++) {
			if (header.charAt(i) == ',' && open == 0) {
				parameters.add(header.substring(lastindex, i));
				lastindex = i + 1;
			} else {
				open += bracketParser(header.charAt(i));
			}
		}
		if (parameters.size() == 0)
			parameters = null;

		// String[] parameters = rawParameter.split(",");
		// for (String parameter : parameters) {
		// String[] parameterParts = parameter.split("=");
		// }
		// first element is always the dataframe list
		// return Arrays.copyOfRange(parameters, 1, parameters.length);
		return parameters;
	}

	public static void parseParameterInsertModel(String parameter, DefaultTableModel paraTableModel) {
		int splitIndex = parameter.indexOf("=");
		if (splitIndex == -1)
			return;

		String[] parameterParts = new String[] { parameter.substring(0, splitIndex),
				parameter.substring(splitIndex + 1) };

		if (parameterParts.length != 2)
			return;

		String parameterValue = parameterParts[1];
		if (!parameterValue.contains("(")) {
			if (parameterValue.contains("\""))
				parameterValue = parameterValue.substring(parameterValue.indexOf("\""),
						parameterValue.lastIndexOf("\"") + 1);
			else
				parameterValue = parameterValue.replaceAll("\\s+", "");
		}
		paraTableModel.addRow(new String[] { parameterParts[0].replaceAll("\\s+", ""), parameterValue });
	}

	public static void addParameterToTableModel(HashMap<String, Parameter> map, DefaultTableModel paraTableModel) {
		for (Parameter parameter : map.values()) {
			log.debug("Set " + parameter.getName() + " to " + parameter.getDefaultValue());
			paraTableModel.addRow(new String[] { parameter.getName(), parameter.getDefaultValue() });
		}
	}

	public static String[] parseParameter(String parameter) {
		int splitIndex = parameter.indexOf("=");
		if (splitIndex == -1)
			return null;

		String[] parameterParts = new String[] { parameter.substring(0, splitIndex).replaceAll("\\s+", ""),
				parameter.substring(splitIndex + 1) };

		if (parameterParts.length != 2)
			return null;

		// ignore parameter preview
		if (parameterParts[0].equals("preview"))
			return null;

		String parameterValue = parameterParts[1];
		// delete whitespace chars at begin/end
		Pattern pattern = Pattern.compile("(\\S.*\\S|\\S)");
		Matcher matcher = pattern.matcher(parameterValue);
		if (matcher.find()) {
			parameterValue = matcher.group();
		}
		return new String[] { parameterParts[0], parameterValue };
	}

	public static Parameter parseParamLine(String content) {

		String paramLine = content.substring(content.indexOf(paramPattern) + paramPattern.length());
		String paramName = "";
		String paramDescription = "";
		String paramType = "";
		List<String> paramAllowedValuesList = new ArrayList<String>();

		// assume that the first whitespace separated word is
		// the parameter name
		Pattern pattern = Pattern.compile("^\\s[\\S]+\\s");
		Matcher matcher = pattern.matcher(paramLine);
		if (matcher.find()) {
			paramName = matcher.group().replaceAll("\\s", "");
			// string("hallo","peter") Set the margin on the
			// side.
			paramDescription = paramLine.substring(matcher.end());

			// string
			int indexSep = paramDescription.indexOf("(");
			if (indexSep > 0) {
				paramType = paramDescription.substring(0, indexSep);
				// ("hallo","peter") Set the margin on the
				// side.
				paramDescription = paramDescription.substring(indexSep);

				int posBrackedClose = parseStringBrakets(paramDescription);

				if (posBrackedClose == -1)
					log.error(content + " || brackets are incorrect.");
				else {
					// "hallo","peter")
					String values = paramDescription.substring(1, posBrackedClose + 1);

					// Set the margin on the side.
					if (paramDescription.length() > posBrackedClose + 1) {
						paramDescription = paramDescription.substring(posBrackedClose + 2);

						// del empty space
						pattern = Pattern.compile("\\S.*$");
						matcher = pattern.matcher(paramDescription);
						if (matcher.find())
							paramDescription = matcher.group();
					} else
						paramDescription = null;

					// "hallo" and "peter"
					pattern = Pattern.compile("[^,]+(,|\\)$)");
					matcher = pattern.matcher(values);
					while (matcher.find()) {
						paramAllowedValuesList.add(values.substring(matcher.start(), matcher.end() - 1));
					}
				}
			}
		}

		if (paramType.equals("float") || paramType.equals("integer")) {
			if (paramAllowedValuesList.size() == 1
					&& (paramAllowedValuesList.get(0).contains("[") || paramAllowedValuesList.get(0).contains("]"))) {
				// parse interval bound e.g. "[0-3]" to "[0" and "3]"
				String[] intervalBounds = paramAllowedValuesList.get(0).split("-");
				if (intervalBounds.length == 2)
					return new Parameter(paramName, paramDescription, paramType, intervalBounds[0], intervalBounds[1]);
				else
					log.error("Parameter parse interval error(" + paramName + "): " + paramAllowedValuesList.get(0));
			}

		} else if (paramType.equals("regex")) {
			// Handle regex with ,
			String regex = String.join(",", paramAllowedValuesList);
			return new Parameter(paramName, paramDescription, paramType, regex);
		}

		// create new Parameter structure/class
		return new Parameter(paramName, paramDescription, paramType, paramAllowedValuesList);
	}

	public static String parseFileName(String name) {
		// id_WT2_CD4_i10_CGTGAT_final.csv
		Pattern pattern = Pattern.compile("id_.*_i[0-9]+_[ACGT]+.*");
		Matcher matcher = pattern.matcher(name);
		String ret = name;
		if (matcher.find()) {
			ret = matcher.group().substring("id_".length());
			ret = ret.replaceFirst("_i[0-9]+_[ACGT]+.*", "");
		} else {
			// KO1_CD4_i1_GTGTCA.xls
			pattern = Pattern.compile(".*_i[0-9]+_[ACGT]+.*");
			matcher = pattern.matcher(name);
			if (matcher.find()) {
				ret = matcher.group().replaceFirst("_i[0-9]+_[ACGT]+.*", "");
			}
		}
		return ret;
	}

	public static String validateValue(Parameter parameter, String value) {
		String type = parameter.getType();
		// type is not set
		if (type == null)
			return null;

		if (type.equals("string")) {
			if (!value.startsWith("\"") || !value.endsWith("\""))
				return "Only strings are allowed (e.g. \"text\" do not forget the quotes)";
		} else if (type.equals("integer")) {
			if (!isInteger(value, 10))
				return "Only integers are allowed!";
			else if (parameter.isIntervalBounded() && !validateInterval(parameter, value))
				return "Integer is out of bound! Allowed interval: " + parameter.getIntervalAsText();
		} else if (type.equals("float")) {
			if (!isFloat(value))
				return "Only floating point numbers are allowed!";
			else if (parameter.isIntervalBounded() && !validateInterval(parameter, value))
				return "Float is out of bound! Allowed interval: " + parameter.getIntervalAsText();
		} else if (type.equals("regex")) {
			String pattern = parameter.getRegex();

			System.out.println(pattern);
			Pattern r = Pattern.compile(pattern);
			Matcher m = r.matcher(value);
			if (!m.find())
				return "Regular expression does not match! \"" + pattern + "\" (without quotes)";
		}
		return null;
	}

	public static boolean isInteger(String s, int radix) {
		if (s.isEmpty())
			return false;
		for (int i = 0; i < s.length(); i++) {
			if (i == 0 && s.charAt(i) == '-') {
				if (s.length() == 1)
					return false;
				else
					continue;
			}
			if (Character.digit(s.charAt(i), radix) < 0)
				return false;
		}
		return true;
	}

	public static boolean isFloat(String s) {
		String pattern = "^[-+]?[0-9]*\\.?[0-9]+$";

		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(s);
		if (m.find()) {
			return true;
		}
		return false;
	}

	public static boolean validateInterval(Parameter parameter, String value) {
		String leftIntervalBound = parameter.getLeftIntervalBound();
		String rightIntervalBound = parameter.getRightIntervalBound();
		if (leftIntervalBound == null || rightIntervalBound == null)
			return true;

		double valueNumeric = Double.parseDouble(value);

		// check left bound
		double leftBoundNumeric = Double.parseDouble(leftIntervalBound.substring(1));
		if (leftIntervalBound.startsWith("[")) {
			if (valueNumeric < leftBoundNumeric)
				return false;
		} else if (leftIntervalBound.startsWith("]")) {
			if (valueNumeric <= leftBoundNumeric)
				return false;
		}

		// check right bound
		double rightBoundNumeric = Double.parseDouble(rightIntervalBound.substring(0, rightIntervalBound.length() - 1));
		if (rightIntervalBound.endsWith("[")) {
			if (valueNumeric >= rightBoundNumeric)
				return false;
		} else if (rightIntervalBound.endsWith("]")) {
			if (valueNumeric > rightBoundNumeric)
				return false;
		}

		return true;
	}

	/**
	 * Parse a string with brackets. e.g. ((ab()a))a(s) => 8
	 * 
	 * @return Index of closing bracket
	 */
	public static int parseStringBrakets(String str) {
		int counter = 0;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == '(')
				counter++;
			else if (str.charAt(i) == ')') {
				counter--;
				if (counter == 0)
					return i;
			}
		}
		return -1;
	}
}
