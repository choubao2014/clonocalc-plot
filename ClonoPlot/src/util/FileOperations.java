package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;

import app.GlobalVars;

public class FileOperations {
	private static final Logger log = Logger.getLogger(FileOperations.class);

	public static void copyFileUsingStream(File source, File dest, boolean append) {
		if (source == null) {
			log.debug("source File is null!");
			return;
		} else if (dest == null) {
			log.debug("dest File is null!");
			return;
		}

		InputStream is = null;
		OutputStream os = null;
		try {
			is = new FileInputStream(source);
			os = new FileOutputStream(dest, append);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
		} catch (FileNotFoundException e) {
			log.error(e, e);
		} catch (IOException e) {
			log.error(e, e);
		} finally {
			try {
				is.close();
				os.close();
			} catch (IOException e) {
				log.error(e, e);
			}

		}
	}

	public static void writeListToFile(String filename, List<String> list) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(filename, GlobalVars.charsetName);
			for (String entry : list) {
				writer.println(entry);
			}
			writer.close();
		} catch (FileNotFoundException e) {
			log.error(e, e);
		} catch (UnsupportedEncodingException e) {
			log.error(e, e);
		}
	}

	public static void writeStringToFile(String filename, String content) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(filename, GlobalVars.charsetName);
			writer.println(content);
			writer.close();
		} catch (FileNotFoundException e) {
			log.error(e, e);
		} catch (UnsupportedEncodingException e) {
			log.error(e, e);
		}
	}

	public static String readFirstLineInString(String filePath) {
		String content = null;
		Scanner sc;
		try {
			sc = new Scanner(new File(filePath), GlobalVars.charsetName);
			content = sc.nextLine();
			sc.close();
			log.debug("read password: " + content);
		} catch (Exception e) {
			log.error(e, e);
		}
		return content;
	}

	public static String getFileExtension(String fileName) {
		int pos = fileName.lastIndexOf('.');
		if (pos > 0) {
			return fileName.substring(pos + 1);
		}
		return "";
	}

	public static String getNamewoExtension(String fileName) {
		int pos = fileName.lastIndexOf(".");
		if (pos > 0) {
			return fileName.substring(0, pos);
		}
		return fileName;
	}

	public static String readFile(String path) {
		Charset encoding = Charset.forName(GlobalVars.charsetName);
		byte[] encoded = null;
		try {
			encoded = Files.readAllBytes(Paths.get(path));
		} catch (IOException e) {
			log.error(e, e);
		}
		return new String(encoded, encoding);
	}
}
